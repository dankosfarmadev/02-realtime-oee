<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Worker_model extends CI_Model
{
    public function getReceive()
    {
        // $query = "SELECT `user`.*, `departement`.`departement`
        //           FROM `user` JOIN `departement`
        //           ON `user`.`departement_id` = `departement`.`id`
        //           WHERE `user`.`role_id` != 1
        //         ";
        // return $this->db->query($query)->result_array();

        $this->db->select('receive_delivery.*, master_status_form_rd.status, user.name, user.image');
        $this->db->from('receive_delivery');
        $this->db->join('master_status_form_rd', 'receive_delivery.status_form_rd_id = master_status_form_rd.id');
        $this->db->join('user', 'receive_delivery.create_by = user.id');
        $this->db->where('receive_delivery.jenis_form', 'receive');
        $this->db->order_by('receive_delivery.id', 'DESC');
        // $this->db->where('receive_delivery.create_by', $this->session->userdata('id'));
        return $query = $this->db->get()->result_array();
    }

    public function getDelivery()
    {
        // $query = "SELECT `user`.*, `departement`.`departement`
        //           FROM `user` JOIN `departement`
        //           ON `user`.`departement_id` = `departement`.`id`
        //           WHERE `user`.`role_id` != 1
        //         ";
        // return $this->db->query($query)->result_array();

        $this->db->select('receive_delivery.*, master_status_form_rd.status, user.name, user.image');
        $this->db->from('receive_delivery');
        $this->db->join('master_status_form_rd', 'receive_delivery.status_form_rd_id = master_status_form_rd.id');
        $this->db->join('user', 'receive_delivery.create_by = user.id');
        $this->db->where('receive_delivery.jenis_form', 'delivery');
        $this->db->order_by('receive_delivery.id', 'DESC');
        // $this->db->where('receive_delivery.create_by', $this->session->userdata('id'));
        return $query = $this->db->get()->result_array();
    }

    public function getDeliveryExport()
    {
        // $query = "SELECT `user`.*, `departement`.`departement`
        //           FROM `user` JOIN `departement`
        //           ON `user`.`departement_id` = `departement`.`id`
        //           WHERE `user`.`role_id` != 1
        //         ";
        // return $this->db->query($query)->result_array();

        $this->db->select('receive_delivery.*, master_status_form_rd.status, user.name, user.image');
        $this->db->from('receive_delivery');
        $this->db->join('master_status_form_rd', 'receive_delivery.status_form_rd_id = master_status_form_rd.id');
        $this->db->join('user', 'receive_delivery.create_by = user.id');
        $this->db->where('receive_delivery.jenis_form', 'delivery_export');
        $this->db->order_by('receive_delivery.id', 'DESC');
        // $this->db->where('receive_delivery.create_by', $this->session->userdata('id'));
        return $query = $this->db->get()->result_array();
    }

    public function getSpecificReceive($id)
    {
        $this->db->select('receive_delivery.*, user.name, user.image, user2.name names');
        $this->db->from('receive_delivery');
        $this->db->join('user', 'receive_delivery.create_by = user.id');
        $this->db->join('user user2', 'receive_delivery.update_by = user2.id');
        $this->db->where('receive_delivery.id', $id); // Produces: WHERE name = 'Joe'
        return $query = $this->db->get()->row_array();
    }

    public function getEveryBatchData($id)
    {
        $this->db->select('data_rd.*');
        $this->db->from('data_rd');
        $this->db->where('rd_id', $id); // Produces: WHERE name = 'Joe'
        return $query = $this->db->get()->result_array();
    }

    public function getSpecificDelivery($id)
    {
        $this->db->select('receive_delivery.*, user.name, user.image, user2.name names');
        $this->db->from('receive_delivery');
        $this->db->join('user', 'receive_delivery.create_by = user.id');
        $this->db->join('user user2', 'receive_delivery.update_by = user2.id');
        $this->db->where('receive_delivery.id', $id); // Produces: WHERE name = 'Joe'
        return $query = $this->db->get()->row_array();
    }

    public function getSpecificDeliveryExport($id)
    {
        $this->db->select('receive_delivery.*, user.name, user.image, user2.name names');
        $this->db->from('receive_delivery');
        $this->db->join('user', 'receive_delivery.create_by = user.id');
        $this->db->join('user user2', 'receive_delivery.update_by = user2.id');
        $this->db->where('receive_delivery.id', $id); // Produces: WHERE name = 'Joe'
        return $query = $this->db->get()->row_array();
    }

    public function getInspection()
    {
        // $query = "SELECT `user`.*, `departement`.`departement`
        //           FROM `user` JOIN `departement`
        //           ON `user`.`departement_id` = `departement`.`id`
        //           WHERE `user`.`role_id` != 1
        //         ";
        // return $this->db->query($query)->result_array();

        $this->db->select('inspection_checklist.*, master_status_form_inspection.status, user.name, user.image');
        $this->db->from('inspection_checklist');
        $this->db->join('master_status_form_inspection', 'inspection_checklist.status_form_inspection_id = master_status_form_inspection.id');
        $this->db->join('user', 'inspection_checklist.create_by = user.id');
        $this->db->order_by('inspection_checklist.id', 'DESC');
        // $this->db->where('receive_delivery.create_by', $this->session->userdata('id'));
        return $query = $this->db->get()->result_array();
    }

    public function getSpecificInspection($id)
    {
        $this->db->select('inspection_checklist.*');
        $this->db->from('inspection_checklist');
        $this->db->where('id', $id); // Produces: WHERE name = 'Joe'
        return $query = $this->db->get()->row_array();
    }

    public function getSpecificInspectionExport($id)
    {
        $this->db->select('inspection_checklist.*');
        $this->db->from('inspection_checklist');
        $this->db->where('rd_id', $id); // Produces: WHERE name = 'Joe'
        return $query = $this->db->get()->row_array();
    }


    public function zz($id)
    {
        $this->db->select('user.*, departement.departement');
        $this->db->from('user');
        $this->db->join('departement', 'departement.id = user.departement_id');
        $this->db->where('user.id', $id); // Produces: WHERE name = 'Joe'
        return $query = $this->db->get()->result_array();
    }
}
