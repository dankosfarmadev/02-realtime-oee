<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Oee_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('cookie');
        $this->load->model('Admin_model');
        $this->load->model('Mailer_model');
        $this->load->model('Feature_model');
        $this->tblName = 'oee';
    }

    public function tableau($nama)
    {
        // $this->db->select('*');
        // $this->db->from('oee');
        // $query = $this->db->get();
        // return $query->result_array();

        //         $query = "SELECT user_app.*, departement.departement
        //         FROM user_app JOIN departement
        //         ON user_app.departement_id = departement.id
        //         WHERE user_app.role_id != 1
        //       ";
        // return $this->db->query($query)->result_array();

        $query = "SELECT * FROM dashboard WHERE dashboard.nama = '$nama'";
        return $this->db->query($query)->result_array();
    }

    public function workerUpdate($id, $data)
    {
        $this->db->where('id', $id);
        // $this->db->update('user', $data);
        $this->db->update('user_app', $data);
    }

    public function insert_batch($data)
    {
        $this->db->insert_batch('oee', $data);
        if ($this->db->affected_rows() > 0) {
            return 1;
        } else {
            return 0;
        }
    }
    public function oee_table($line)
    {
        // $this->db->select('*');
        // $this->db->from('oee');
        // $query = $this->db->get();
        // return $query->result_array();

        $query = "SELECT * FROM oee WHERE line = $line ORDER BY id DESC LIMIT 5000
         ";
        return $this->db->query($query)->result_array();
    }







    function getRows($params = array())
    {
        $this->db->select('*');
        $this->db->from($this->tblName);

        //fetch data by conditions
        if (array_key_exists("where", $params)) {
            foreach ($params['where'] as $key => $value) {
                $this->db->where($key, $value);
            }
        }

        if (array_key_exists("order_by", $params)) {
            $this->db->order_by($params['order_by']);
        }

        if (array_key_exists("id", $params)) {
            $this->db->where('id', $params['id']);
            $query = $this->db->get();
            $result = $query->row_array();
        } else {
            //set start and limit
            if (array_key_exists("start", $params) && array_key_exists("limit", $params)) {
                $this->db->limit($params['limit'], $params['start']);
            } elseif (!array_key_exists("start", $params) && array_key_exists("limit", $params)) {
                $this->db->limit($params['limit']);
            }

            if (array_key_exists("returnType", $params) && $params['returnType'] == 'count') {
                $result = $this->db->count_all_results();
            } else {
                $query = $this->db->get();
                $result = ($query->num_rows() > 0) ? $query->result_array() : FALSE;
            }
        }

        //return fetched data
        return $result;
    }

    /*
     * Delete data from the database
     * @param id array/int
     */
    public function delete($id)
    {
        if (is_array($id)) {
            $this->db->where_in('id', $id);
        } else {
            $this->db->where('id', $id);
        }
        $delete = $this->db->delete($this->tblName);
        return $delete ? true : false;
    }
}
