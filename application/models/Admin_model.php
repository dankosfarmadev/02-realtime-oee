<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_model extends CI_Model
{
    public function getWorker()
    {
        $query = "SELECT user_app.*, departement.departement
                  FROM user_app JOIN departement
                  ON user_app.departement_id = departement.id
                  WHERE user_app.role_id != 1
                ";
        return $this->db->query($query)->result_array();
    }

    public function getSpecificWorker($id)
    {
        // $query = "SELECT `user_app`.*, `departement`.`departement`
        //           FROM `user_app` JOIN `departement`
        //           ON `user_app`.`departement_id` = `departement`.`id`
        //           WHERE `user_app`.`id` = $id
        //         ";
        // return $this->db->query($query)->result_array();


        $this->db->select('user_app.*, departement.departement');
        $this->db->from('user_app');
        $this->db->join('departement', 'departement.id = user_app.departement_id');
        $this->db->where('user_app.id', $id); // Produces: WHERE name = 'Joe'
        return $query = $this->db->get()->result_array();
    }

    public function getAllDept()
    {
        return $this->db->get('departement')->result_array();
    }

    public function getAllRole()
    {
        $this->db->select('*');
        $this->db->from('user_role');
        $this->db->where('user_role.id !=', 1); // Produces: WHERE name = 'Joe'
        return $this->db->get()->result_array();
    }

    public function workerUpdate($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('user_app', $data);
    }

    public function insert_batch($data)
    {
        $this->db->insert_batch('oee', $data);
        if ($this->db->affected_rows() > 0) {
            return 1;
        } else {
            return 0;
        }
    }
    public function oee_table()
    {
        // $this->db->select('*');
        // $this->db->from('oee');
        // $query = $this->db->get();
        // return $query->result_array();

        $query = "SELECT * FROM oee ORDER BY id DESC LIMIT 1000
         ";
        return $this->db->query($query)->result_array();
    }
}
