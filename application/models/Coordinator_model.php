<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Coordinator_model extends CI_Model
{
    public function getAllWhere()
    {
        $this->db->select('receive_delivery.*, master_status_form_rd.status, user.name');
        $this->db->from('receive_delivery');
        $this->db->join('master_status_form_rd', 'receive_delivery.status_form_rd_id = master_status_form_rd.id');
        $this->db->join('user', 'receive_delivery.create_by = user.id');
        $this->db->where('receive_delivery.status_form_rd_id', 5);
        $this->db->where('receive_delivery.jenis_form !=', 'delivery_export');
        $this->db->order_by('receive_delivery.id', 'DESC');
        // $this->db->where('receive_delivery.create_by', $this->session->userdata('id'));
        return $query = $this->db->get()->result_array();
    }

    public function getAllWhereExport()
    {
        $this->db->select('receive_delivery.*, master_status_form_rd.status, user.name');
        $this->db->from('receive_delivery');
        $this->db->join('master_status_form_rd', 'receive_delivery.status_form_rd_id = master_status_form_rd.id');
        $this->db->join('user', 'receive_delivery.create_by = user.id');
        $this->db->where('receive_delivery.status_form_rd_id', 5);
        $this->db->where('receive_delivery.jenis_form =', 'delivery_export');
        $this->db->order_by('receive_delivery.id', 'DESC');
        // $this->db->where('receive_delivery.create_by', $this->session->userdata('id'));
        return $query = $this->db->get()->result_array();
    }
}
