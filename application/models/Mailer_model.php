<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

require_once __DIR__ . '/../../vendor/autoload.php';

class Mailer_model extends CI_Model
{

    public function getWorker()
    {
        $query = "SELECT `user`.*, `departement`.`departement`
                  FROM `user` JOIN `departement`
                  ON `user`.`departement_id` = `departement`.`id`
                  WHERE `user`.`role_id` != 1
                ";
        return $this->db->query($query)->result_array();
    }

    public function sendMail($name, $to, $cc, $type, $subject, $message, $redirect)
    {
        // var_dump('wlwl');
        // die;
        $type = $type;
        $config = [
            'protocol'  => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_user' => 'dankosfarmadev@gmail.com',
            'smtp_pass' => 'Dankosfarma1',
            'smtp_port' => 465,
            'mailtype'  => 'html',
            'charset'   => 'utf-8',
            'newline'   => "\r\n"
        ];

        $this->email->initialize($config);

        $this->email->from('dankosfarmadev@gmail.com', $name);
        $this->email->to($to);
        $this->email->cc($cc);
        // $this->email->to($this->input->post('email'));

        if ($type == 'verify') {
            $this->email->subject($subject);
            // $this->email->message('Click this link to verify you account : <a href="' . base_url() . 'auth/verify?email=' . $this->input->post('email') . '&token=' . urlencode($token) . '">Activate</a>');
            $this->email->message($message);
        } else if ($type == 'forgot') {
            $this->email->subject($subject);
            $this->email->message($message);
            // $this->email->message('Click this link to reset your password : <a href="' . base_url() . 'auth/resetpassword?email=' . $this->input->post('email') . '&token=' . urlencode($token) . '">Reset Password</a>');
        }

        if ($this->email->send()) {
            $this->session->set_flashdata('message_email', '<div class="alert alert-success" role="alert">Email success to send!</div>');
        } else {
            $this->session->set_flashdata('message_email', '<div class="alert alert-danger" role="alert">Email failed to send!</div>');
        }

        // redirect($redirect);
        if ($redirect == '') {
        }
    }

    public function curlRequest($url){
        $c = curl_init();
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($c);
        curl_close($c);
        return $data;
    }

    public function test($url){
        $filename = $url;
        $handle = fopen($filename, "r");
        $contents = fread($handle, filesize($filename));
        fclose($handle);
        return $contents;
    }

    public function exportPDF($id)
    {
        $this->load->model('Worker_model');


        $data['user'] = $this->db->get_where('user', ['nik' => $this->session->userdata('nik')])->row_array();
        $data['receive_data'] = $this->Worker_model->getSpecificReceive($id);
        $data['rd_data'] = $this->Worker_model->getEveryBatchData($id);

        $mpdf = new \Mpdf\Mpdf([
            'format' => 'a4'
        ]);
        // $stylesheet = file_get_contents('C:\xampp\htdocs\template-ci-v3\application\views\test\style.css');
        $stylesheet = file_get_contents('application\views\test\style.css');
        // $stylesheet = file_get_contents()
        // $stylesheet = $this->curlRequest('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js');
        // $stylesheet = file_get_contents("https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js");
        // $stylesheet = $this->test('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js');
        $data = $this->load->view('test/testInvoicev2', $data, true);



        // $url_path = "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js";
        // $res = $this->curl->simple_get($url_path);
        // $stylesheet = json_decode($res, true);



        $mpdf->WriteHTML($stylesheet, 1);
        // $mpdf->WriteHTML($stylesheet2, 1);
        $mpdf->WriteHTML($data, 2);
        $mpdf->Output();

        // $mpdf = new \Mpdf\Mpdf([
        //     'margin_left' => 20,
        //     'margin_right' => 15,
        //     'margin_top' => 48,
        //     'margin_bottom' => 25,
        //     'margin_header' => 10,
        //     'margin_footer' => 10
        // ]);

        // $mpdf->SetProtection(array('print'));
        // $mpdf->SetTitle("Acme Trading Co. - Invoice");
        // $mpdf->SetAuthor("Acme Trading Co.");
        // $mpdf->SetWatermarkText("Paid");
        // $mpdf->showWatermarkText = true;
        // $mpdf->watermark_font = 'DejaVuSansCondensed';
        // $mpdf->watermarkTextAlpha = 0.1;
        // $mpdf->SetDisplayMode('fullpage');
        // $data = $this->load->view('test/testInvoicev2', '', true);
        // $mpdf->WriteHTML($$data);

        // $mpdf->Output();
    }

    public function exportPDFInspection($id)
    {
        $this->load->model('Worker_model');


        $data['user'] = $this->db->get_where('user', ['nik' => $this->session->userdata('nik')])->row_array();
        $data['receive_data'] = $this->Worker_model->getSpecificReceive($id);
        $data['rd_data'] = $this->Worker_model->getEveryBatchData($id);
        $data['ins'] = $this->Worker_model->getSpecificInspectionExport($id);

        $mpdf = new \Mpdf\Mpdf([
            'format' => 'a4'
        ]);
        $stylesheet = file_get_contents('C:\xampp\htdocs\template-ci-v3\application\views\test\style.css');
        // $stylesheet2 = file_get_contents("https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js");
        $data = $this->load->view('test/testInvoicev2Inspection', $data, true);
        $mpdf->WriteHTML($stylesheet, 1);
        // $mpdf->WriteHTML($stylesheet2, 1);
        $mpdf->WriteHTML($data, 2);
        $mpdf->Output();
    }
}
