<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Menu_model extends CI_Model
{
    public function getSubMenu()
    {
        $query = "SELECT user_sub_menu.*, user_menu.menu
                  FROM user_sub_menu JOIN user_menu
                  ON user_sub_menu.menu_id = user_menu.id
                ";
        return $this->db->query($query)->result_array();
    }

    public function getMenu()
    {
        // $query = "SELECT `user_sub_menu`.*, `user_menu`.`menu`
        //           FROM `user_sub_menu` JOIN `user_menu`
        //           ON `user_sub_menu`.`menu_id` = `user_menu`.`id`
        //         ";
        return $query = $this->db->get('user_menu')->result_array();  // Produces: SELECT * FROM mytable
        // return $this->db->query($query)->result_array();
    }

    public function getSubMenuXlsx()
    {
        $query = "SELECT user_sub_menu.*, user_menu.menu
                  FROM user_sub_menu JOIN user_menu
                  ON user_sub_menu.menu_id = user_menu.id
                ";
        return $this->db->query($query)->result();
        // return $query = $this->db->get('user_menu')->result_array();  // Produces: SELECT * FROM mytable
    }

    public function menuView($id)
    {
        $query = "SELECT user_menu.*
                  FROM user_menu
                  WHERE user_menu.id = $id
                ";
        return $this->db->query($query)->result_array();
    }

    public function submenuView($id)
    {
        $query = "SELECT user_sub_menu.*
                  FROM user_sub_menu
                  WHERE user_sub_menu.id = $id
                ";
        return $this->db->query($query)->result_array();
    }
}
