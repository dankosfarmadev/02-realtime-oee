<html>

<head>

    <link rel="stylesheet" href="styles.css">
    <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous"> -->
    <!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
    </link>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js">
    </link>
    <link href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    </link> -->
</head>

<body>
    <div class="container mt-5 mb-3">
        <div class="row d-flex justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="d-flex flex-row p-2">
                        <img src="<?= base_url('assets/'); ?>img/dankos_logo.jpg" width="75">
                        <div class="d-flex flex-column text-center">7 Inspection Checklist</div>
                        <div class="d-flex flex-column text-center font-weight-bold">Checklist Pemeriksaan Kendaraan Ekspor</div>
                    </div>
                    <hr>
                    <div class="d-flex flex-row p-2">
                        <div class="d-flex flex-column text-left" style="font-weight: bold; font-size: 12;">1. Bagian luar (Outside/Undercarriage)</div>
                        <table class="table table-borderless">
                            <tbody>
                                <tr class="content justify-content-start">
                                    <td class="text-left" style="font-size: 12;"><small>a. Pada bagian kontainer jika ada perbaikan, rusak, belubang, penyok </small></td>
                                    <td class="text-right" style="font-size: 12;"><small><?= $ins['1a'] == 'Y' ? 'Yes' : 'No' ?></small></td>
                                </tr>
                                <tr class="content justify-content-start">
                                    <td class="text-left" style="font-size: 12;"><small>b. Bagian rangka kontainer terlihat </small></td>
                                    <td class="text-right" style="font-size: 12;"><small><?= $ins['1b'] == 'Y' ? 'Yes' : 'No' ?></small></td>
                                </tr>
                                <tr class="content justify-content-start">
                                    <td class="text-left" style="font-size: 12;"><small>c. Pada bagian kontainer jika ada perbaikan, rusak, belubang, penyok </small></td>
                                    <td class="text-right" style="font-size: 12;"><small><?= $ins['1c'] == 'Y' ? 'Yes' : 'No' ?></small></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="d-flex flex-row p-2" style="margin-top: -20px;">
                        <div class="d-flex flex-column text-left" style="font-weight: bold; font-size: 12;">2. Bagian dalam dan luar pintu (Inside/Outside Doors)</div>
                        <table class="table table-borderless">
                            <tbody>
                                <tr class="content justify-content-start">
                                    <td class="text-left" style="font-size: 12;"><small>a. Pastikan kunci, dan mekanisme penguncian aman dan terjamin </small></td>
                                    <td class="text-right" style="font-size: 12;"><small><?= $ins['2a'] == 'Y' ? 'Yes' : 'No' ?></small></td>
                                </tr>
                                <tr class="content justify-content-start">
                                    <td class="text-left" style="font-size: 12;"><small>b. Cek jika ada baut yang hilang </small></td>
                                    <td class="text-right" style="font-size: 12;"><small><?= $ins['2b'] == 'Y' ? 'Yes' : 'No' ?></small></td>
                                </tr>
                                <tr class="content justify-content-start">
                                    <td class="text-left" style="font-size: 12;"><small>c. Pastikan engsel pintu aman dan terjamin </small></td>
                                    <td class="text-right" style="font-size: 12;"><small><?= $ins['2c'] == 'Y' ? 'Yes' : 'No' ?></small></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="d-flex flex-row p-2" style="margin-top: -20px;">
                        <div class="d-flex flex-column text-left" style="font-weight: bold; font-size: 12;">3. Dinding kanan (Right Side)</div>
                        <table class="table table-borderless">
                            <tbody>
                                <tr class="content justify-content-start">
                                    <td class="text-left" style="font-size: 12;"><small>a. Perhatikan perbaikan yang mencurigakan pada bagian kontainer </small></td>
                                    <td class="text-right" style="font-size: 12;"><small><?= $ins['3a'] == 'Y' ? 'Yes' : 'No' ?></small></td>
                                </tr>
                                <tr class="content justify-content-start">
                                    <td class="text-left" style="font-size: 12;"><small>b. Perbaikan pada dinding kontainer harus terlihat dikedua sisi dalam dan diluar </small></td>
                                    <td class="text-right" style="font-size: 12;"><small><?= $ins['3b'] == 'Y' ? 'Yes' : 'No' ?></small></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="d-flex flex-row p-2" style="margin-top: -20px;">
                        <div class="d-flex flex-column text-left" style="font-weight: bold; font-size: 12;">4. Dinding kiri (Left Side)</div>
                        <table class="table table-borderless">
                            <tbody>
                                <tr class="content justify-content-start">
                                    <td class="text-left" style="font-size: 12;"><small>a. Perhatikan perbaikan yang mencurigakan pada bagian kontainer </small></td>
                                    <td class="text-right" style="font-size: 12;"><small><?= $ins['4a'] == 'Y' ? 'Yes' : 'No' ?></small></td>
                                </tr>
                                <tr class="content justify-content-start">
                                    <td class="text-left" style="font-size: 12;"><small>b. Perbaikan pada dinding kontainer harus terlihat dikedua sisi dalam dan diluar </small></td>
                                    <td class="text-right" style="font-size: 12;"><small><?= $ins['4b'] == 'Y' ? 'Yes' : 'No' ?></small></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="d-flex flex-row p-2" style="margin-top: -20px;">
                        <div class="d-flex flex-column text-left" style="font-weight: bold; font-size: 12;">5. Dinding depan (Front Wall)</div>
                        <table class="table table-borderless">
                            <tbody>
                                <tr class="content justify-content-start">
                                    <td class="text-left" style="font-size: 12;"><small>a. Bagian dinding terbuat dari bahan yang bergelombang </small></td>
                                    <td class="text-right" style="font-size: 12;"><small><?= $ins['5a'] == 'Y' ? 'Yes' : 'No' ?></small></td>
                                </tr>
                                <tr class="content justify-content-start">
                                    <td class="text-left" style="font-size: 12;"><small>b. Tulangan pada sudut bagian kiri dan kanan atas harus terlihat, jika hilang atau salah adalah tidak normal </small></td>
                                    <td class="text-right" style="font-size: 12;"><small><?= $ins['5b'] == 'Y' ? 'Yes' : 'No' ?></small></td>
                                </tr>
                                <tr class="content justify-content-start">
                                    <td class="text-left" style="font-size: 12;"><small>c. Pastikan ventilasi terlihat </small></td>
                                    <td class="text-right" style="font-size: 12;"><small><?= $ins['5c'] == 'Y' ? 'Yes' : 'No' ?></small></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="d-flex flex-row p-2" style="margin-top: -20px;">
                        <div class="d-flex flex-column text-left" style="font-weight: bold; font-size: 12;">6. Dinding atap (Celling/Roof)</div>
                        <table class="table table-borderless">
                            <tbody>
                                <tr class="content justify-content-start">
                                    <td class="text-left" style="font-size: 12;"><small>a. Pastikan rangka kontainer terlihat </small></td>
                                    <td class="text-right" style="font-size: 12;"><small><?= $ins['6a'] == 'Y' ? 'Yes' : 'No' ?></small></td>
                                </tr>
                                <tr class="content justify-content-start">
                                    <td class="text-left" style="font-size: 12;"><small>b. Pastikan ventilasi terlihat, jika ada maka tidak boleh ditutup atau dihilangkan</small></td>
                                    <td class="text-right" style="font-size: 12;"><small><?= $ins['6b'] == 'Y' ? 'Yes' : 'No' ?></small></td>
                                </tr>
                                <tr class="content justify-content-start">
                                    <td class="text-left" style="font-size: 12;"><small>c. Pastikan tidak ada benda lain yang menempel pada kontainer </small></td>
                                    <td class="text-right" style="font-size: 12;"><small><?= $ins['6c'] == 'Y' ? 'Yes' : 'No' ?></small></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="d-flex flex-row p-2" style="margin-top: -20px;">
                        <div class="d-flex flex-column text-left" style="font-weight: bold; font-size: 12;">7. Bagian lantai (Floor)</div>
                        <table class="table table-borderless">
                            <tbody>
                                <tr class="content justify-content-start">
                                    <td class="text-left" style="font-size: 12;"><small>a. Pastikan lantai kontainer dalam keadaan rata </small></td>
                                    <td class="text-right" style="font-size: 12;"><small><?= $ins['7a'] == 'Y' ? 'Yes' : 'No' ?></small></td>
                                </tr>
                                <tr class="content justify-content-start">
                                    <td class="text-left" style="font-size: 12;"><small>b. Pastikan lantai dalam keadaan seragam</small></td>
                                    <td class="text-right" style="font-size: 12;"><small><?= $ins['7b'] == 'Y' ? 'Yes' : 'No' ?></small></td>
                                </tr>
                                <tr class="content justify-content-start">
                                    <td class="text-left" style="font-size: 12;"><small>c. Lihat jika ada perbaikan tidak biasa pada lantai </small></td>
                                    <td class="text-right" style="font-size: 12;"><small><?= $ins['7c'] == 'Y' ? 'Yes' : 'No' ?></small></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>







                    <hr>

                    <div class="d-flex flex-column text-center">
                        <small class="text-center" style="font-size: 10; color:red; text-align: 'center';">
                            **Saya telah melakukan pemeriksaan secara visual kondisi kontainer/box yang disebutkan di atas.
                            Saya yang bertanda tangan mengkonfirmasi bahwa kontainer memiliki struktur yang kuat, kedap cuaca,
                            tidak memiliki kompartemen palsu, dan mekanisme penguncian berada dalam keadaan baik dan terjamin dan tidak menunjukkan tanda-tanda
                            yang dapat dirusak**
                        </small>
                    </div>











                    <hr>
                    <div class="table-responsive p-2">
                        <table class="table table-borderless" style="margin-top: -15px; margin-bottom: -15px;">
                            <tbody>
                                <tr class="content">
                                    <td class="">
                                        <small style="font-size: 12;">Keterangan: </small>
                                        <br>
                                        <small style="font-size: 12;"><?= $ins['catatan'] ?></small>
                                    </td>
                                    <td class="">
                                        <table class="table table-borderless" style="margin-right: 75px;">
                                            <tbody>
                                                <tr>
                                                    <td class="text-right" style="font-size: 12;">
                                                        Dikerjakan oleh:
                                                    </td>
                                                </tr>
                                                <tr class="content justify-content-end">
                                                    <td class="text-right">
                                                        <img style="width: 100px; height: 100px;" src="<?= base_url('assets/img/profile/') . $receive_data['image'] ?>" class="img-thumbnail">
                                                    </td>
                                                </tr>
                                                <br>
                                                <tr class="">
                                                    <td class="text-right" style="font-size: 10px;"><?= $receive_data['name'] ?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </div>

</html>