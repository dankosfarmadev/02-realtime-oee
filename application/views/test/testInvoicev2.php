<html>

<head>

    <!-- <link rel="stylesheet" href="styles.css"> -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/'); ?>bootstrap-3.3.0/dist/css/bootstrap.min.css">
    <!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
    </link>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js">
    </link>
    <link href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    </link> -->
</head>

<body>
    <div class="container mt-5 mb-3">
        <div class="row d-flex justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="d-flex flex-row p-2">
                        <!-- <img src="<?= base_url('assets/'); ?>app-assets/images/flags/de.png" width="48"> -->
                        <img src="<?= base_url('assets/'); ?>img/dankos_logo.jpg" width="75">
                        <!-- <div class="d-flex flex-column text-center">Formulir
                            <?= $receive_data['jenis_form'] == 'receive' ? 'Penerimaan<s>/Pengiriman</s>' : '<s>Penerimaan/</s>Pengiriman' ?>
                            Obat Jadi</div> -->
                        <div class="d-flex flex-column text-center">Formulir Penerimaan/Pengiriman Obat Jadi</div>
                    </div>
                    <hr>
                    <div class="table-responsive p-2">
                        <table class="table table-borderless" style="margin-top: -15px; margin-bottom: -15px;">
                            <tbody>
                                <tr class="content">
                                    <!-- <div class="justify-left"> -->
                                    <td class="">
                                        <small>
                                            Tanggal : <?= date('d-m-Y', strtotime($receive_data['tanggal'])); ?>
                                            <br>
                                            Dist/Cust : <?= $receive_data['distributor_customer'] ?>
                                            <br>
                                            Ekspedisi : <?= $receive_data['ekspedisi'] ?>
                                            <br>
                                            No polisi : <?= $receive_data['no_polisi'] ?>
                                        </small>
                                    </td>
                                    <td class="">
                                        <small>
                                            Suhu awal : <?= $receive_data['suhu_awal'] ?>
                                            <br>
                                            Suhu setelah : <?= $receive_data['suhu_setelah'] ?>
                                            <br>
                                            Jam awal : <?= $receive_data['jam_awal'] ?>
                                            <br>
                                            Jam setelah : <?= $receive_data['jam_setelah'] ?>
                                        </small>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <hr>
                    <div class="d-flex flex-column text-center text-bold-600">
                        <?= $receive_data['jenis_form'] == 'receive' ? 'Penerimaan<s>/Pengiriman</s>' : '<s>Penerimaan/</s>Pengiriman' ?>
                        * </div>
                    <hr>

                    <div class="products p-2">
                        <table class="table table-borderless" style="margin-top: -10px; margin-bottom: -15px;">
                            <tbody>
                                <tr class="content">
                                    <td style="font-size: 13;" class="text-center"><small>|No| </small></td>
                                    <td style="font-size: 13;" class="text-center"><small>|Batch Number| </small></td>
                                    <td style="font-size: 13;" class="text-center"><small>|LPN Number| </small></td>
                                    <td style="font-size: 13;" class="text-center"><small>|Qty MB| </small></td>
                                    <td style="font-size: 13;" class="text-center"><small>|Qty / MB| </small></td>
                                    <td style="font-size: 13;" class="text-center"><small>|Qty Pick| </small></td>
                                    <td style="font-size: 13;" class="text-center"><small>|Product Check Out| </small></td>
                                    <td style="font-size: 13;" class="text-center"><small>|Qty EPM Pallet| </small></td>
                                    <!-- <td class="text-center">Total</td> -->
                                </tr>
                                <?php $i = 1;
                                $total = 0 ?>
                                <?php foreach ($rd_data as $r) : ?>
                                    <tr class="">
                                        <td style="font-size: 13;" class="text-center"><small><?= $i ?></small></td>
                                        <td style="font-size: 13;" class="text-center"><small><?= $r['batch_number']; ?></small></td>
                                        <td style="font-size: 13;" class="text-center"><small><?= $r['lpn_number']; ?></small></td>
                                        <td style="font-size: 13;" class="text-center"><small><?= $r['qty_mb']; ?></small></td>
                                        <td style="font-size: 13;" class="text-center"><small><?= $r['qty_per_mb']; ?></small></td>
                                        <td style="font-size: 13;" class="text-center"><small><?= $r['qty_pick']; ?></small></td>
                                        <td style="font-size: 13;" class="text-center"><small><?= $r['product_checklist_out']; ?></small></td>
                                        <td style="font-size: 13;" class="text-center"><small><?= $r['qty_epm_pallet']; ?></small></td>
                                    </tr>
                                    <?php $i++;
                                    $total = $total + $r['qty_epm_pallet']; ?>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <hr>
                    <div class="d-flex flex-column text-right" style="margin-top: -10px; margin-bottom: -10px; margin-right: 45px;"> <small> Total : <?= $total; ?> </small></div>
                    <hr>
                    <div class="d-flex flex-column text-left" style="margin-top: -10px; margin-bottom: -10px; font-size: 12;">
                        Catatan : <br><small style="font-size: 8; color:red;">(catat pada kolom ini jika terdapat produk kondisi basah/rusak dalam bentuk apapun atau terjadi penyimpangan):</small>
                        <br>
                        <?= $receive_data['catatan'] ?>
                        <br><br>
                        <small style="font-size: 10;">
                            No seal/segel:
                            <!-- <br> -->
                            <?= $receive_data['segel'] ?>
                        </small>
                    </div>
                    <hr>
                    <div class="d-flex flex-column text-center text-bold-600" style="margin-top: -10px; margin-bottom: -10px;"><small>Kondisi Kendaraan</small></div>
                    <hr>
                    <div class="products p-2">
                        <table class="table table-borderless" style="margin-top: -10px; margin-bottom: -15px;">
                            <tbody>
                                <tr class="content justify-content-start">
                                    <td style="font-size: 13;" class="text-center"><small>|Kebersihan| </small></td>
                                    <td style="font-size: 13;" class="text-center"><small>|Bunyi Mesin| </small></td>
                                    <td style="font-size: 13;" class="text-center"><small>|Asap Kendaraan| </small></td>
                                    <td style="font-size: 13;" class="text-center"><small>|Bau| </small></td>
                                    <td style="font-size: 13;" class="text-center"><small>|Kebocoran| </small></td>
                                    <td style="font-size: 13;" class="text-center"><small>|Uji Emisi/KIR| </small></td>
                                    <td style="font-size: 13;" class="text-center"><small>|Tetesan Oli| </small></td>
                                </tr>
                                <br>
                                <tr class="">
                                    <td style="font-size: 13;" class="text-center"><small><?= $receive_data['kebersihan_kendaraan'] ?></small></td>
                                    <td style="font-size: 13;" class="text-center"><small><?= $receive_data['bunyi_mesin_kendaraan'] ?></small></td>
                                    <td style="font-size: 13;" class="text-center"><small><?= $receive_data['asap_kendaraan'] ?></small></td>
                                    <td style="font-size: 13;" class="text-center"><small><?= $receive_data['bau_kendaraan'] ?></small></td>
                                    <td style="font-size: 13;" class="text-center"><small><?= $receive_data['kebocoran_kendaraan'] ?></small></td>
                                    <td style="font-size: 13;" class="text-center"><small><?= $receive_data['emisi_kendaraan'] ?></small></td>
                                    <td style="font-size: 13;" class="text-center"><small><?= $receive_data['oli_kendaraan'] ?></small></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <hr>
                    <div class="table-responsive p-2">
                        <table class="table table-borderless" style="margin-top: -15px; margin-bottom: -15px;">
                            <tbody>
                                <tr class="content">
                                    <td class="">
                                        <small>Keterangan</small>
                                        <ul>
                                            <li>
                                                <small>
                                                    Pencatatan dilakukan untuk produk dengan <br>
                                                    temperatur khusus (2-8ºC atau &lt;25ºC)
                                                </small>
                                            </li>
                                            <li>
                                                <small>
                                                    C atau ȼ adalah koli
                                                </small>
                                            </li>
                                            <li>
                                                <small>
                                                    Jumlah palet EPM hanya untuk pengiriman obat jadi
                                                </small>
                                                <ul>
                                                    <li><small>coret yang tidak sesuai *</small></li>
                                                    <li><small>catat nomor seal/segel pada kolom CATATAN **</small></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
                                    <td class="">
                                        <p style="font-size: 12px;">Dikerjakan oleh</p> <br>
                                        <table class="table table-borderless">
                                            <tbody>
                                                <tr class="content justify-content-start">
                                                    <td class="text-center">
                                                        <img style="width: 100px; height: 100px;" src="<?= base_url('assets/img/profile/') . $receive_data['image'] ?>" class="img-thumbnail">
                                                    </td>
                                                    <td class="text-center">
                                                        <img style="width: 100px; height: 100px;" src="<?= base_url('assets/img/foto_ekspedisi/') . $receive_data['foto_ekspedisi']; ?>" class="img-thumbnail">
                                                    </td>
                                                </tr>
                                                <br>
                                                <br>
                                                <tr class="">
                                                    <td class="text-center" style="font-size: 10px;"><?= $receive_data['name'] ?></td>
                                                    <td class="text-center" style="font-size: 10px;"><?= $receive_data['nama_ekspedisi'] ?></td>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <br>
                    <div class="d-flex flex-column text-center">
                        <small class="text-center" style="font-size: 10; color:red; text-align: 'center';">
                            Dokumen ini sebagai bukti valid produk/barang yang FG PT Dankos Farma terima atau shipping secara jumlah, nomor batch, kondisi
                            dan keterangan lainnya yang tertera dan ditandatangani oleh pihak ekspedisi.
                        </small>
                    </div>


                    <!-- <div class="products p-2">
                        <table class="table table-borderless">
                            <tbody>
                                <tr class="add">
                                    <td></td>
                                    <td>Subtotal</td>
                                    <td>GST(10%)</td>
                                    <td class="text-center">Total</td>
                                </tr>
                                <tr class="content">
                                    <td></td>
                                    <td>$40,000</td>
                                    <td>2,500</td>
                                    <td class="text-center">$42,500</td>
                                </tr>
                            </tbody>
                        </table>
                    </div> -->
                    <!-- <hr>
                    <div class="address p-2">
                        <table class="table table-borderless">
                            <tbody>
                                <tr class="add">
                                    <td>Bank Details</td>
                                </tr>
                                <tr class="content">
                                    <td> Bank Name : ADS BANK <br> Swift Code : ADS1234Q <br> Account Holder : Jelly Pepper <br> Account Number : 5454542WQR <br> </td>
                                </tr>
                            </tbody>
                        </table>
                    </div> -->
                </div>
            </div>
        </div>
    </div>

</html>