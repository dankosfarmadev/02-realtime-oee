<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">

            <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

            <?= form_open_multipart('user/edit'); ?>
            <div class="form-group row">
                <label for="email" class="col-sm-2 col-form-label">NIK</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="nik" name="nik" value="<?= $user['nik']; ?>" readonly>
                </div>
            </div>
            <div class="form-group row">
                <label for="email" class="col-sm-2 col-form-label">Email</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="email" name="email" value="<?= $user['email']; ?>" readonly>
                </div>
            </div>
            <div class="form-group row">
                <label for="departement_id" class="col-sm-2 col-form-label">Departement</label>
                <div class="col-sm-10">
                    <select name="departement_id" id="departement_id" class="form-control" readonly disabled>
                        <option value="">Select Departement</option>
                        <?php foreach ($list_departemen as $m) : ?>
                            <option value="<?= $m['id']; ?>" <?= $m['id'] == $user['departement_id'] ? 'selected' : ''; ?>><?= $m['departement']; ?></option>
                            <!-- <option value="<?= $value->id ?>" <?= (in_array($value->id, $selected)) ? "selected = 'selected'" : ""; ?> ><?= $value->title; ?></option> -->
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <!-- <div class="form-group row">
                <label for="email" class="col-sm-2 col-form-label">Departement</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="nik" name="nik" value="<?= $user['nik']; ?>" readonly>
                </div>
            </div> -->
            <div class="form-group row">
                <label for="name" class="col-sm-2 col-form-label">Full name</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="name" name="name" value="<?= $user['name']; ?>" readonly>
                    <?= form_error('name', '<small class="text-danger pl-3">', '</small>'); ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-2">Picture</div>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-3">
                            <img src="<?= base_url('assets/img/profile/') . $user['image']; ?>" class="img-thumbnail">
                        </div>
                        <div class="col-sm-9">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="image" name="image">
                                <label class="custom-file-label" for="image">Choose file</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group row justify-content-end">
                <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Edit</button>
                </div>
            </div>


            </form>


        </div>
    </div>



</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->