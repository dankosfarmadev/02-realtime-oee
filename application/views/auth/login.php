   <?php
    // if (get_cookie('remember_me_email')!=null) {
    //     $email = get_cookie('remember_me_email')
    // }
    // if (get_cookie('remember_me_pw')!=null) {
    //     $pw = get_cookie('remember_me_pw')
    // }
    ?>

   <!-- BEGIN: Content-->
   <div class="app-content content">
       <div class="content-overlay"></div>
       <div class="content-wrapper">
           <div class="content-header row">
           </div>
           <div class="content-body">
               <!-- login page start -->
               <section id="auth-login" class="row flexbox-container">
                   <div class="col-xl-8 col-11">
                       <div class="card bg-authentication mb-0">
                           <div class="row m-0">
                               <!-- left section-login -->
                               <div class="col-md-6 col-12 px-0">
                                   <div class="card disable-rounded-right mb-0 p-2 h-100 d-flex justify-content-center">
                                       <div class="card-header pb-1">
                                           <div class="card-title">
                                               <h4 class="text-center mb-2"><?= NAME_APP ?> <small style="font-size: 14px;"><?= VERSION_APP ?></small></h4>
                                           </div>
                                       </div>
                                       <div class="card-body">
                                           <!-- <div class="d-flex flex-md-row flex-column justify-content-around">
                                                <a href="javascript:void(0);" class="btn btn-social btn-google btn-block font-small-3 mr-md-1 mb-md-0 mb-1">
                                                    <i class="bx bxl-google font-medium-3"></i><span class="pl-50 d-block text-center">Google</span></a>
                                                <a href="javascript:void(0);" class="btn btn-social btn-block mt-0 btn-facebook font-small-3">
                                                    <i class="bx bxl-facebook-square font-medium-3"></i><span class="pl-50 d-block text-center">Facebook</span></a>
                                            </div> -->
                                           <!-- <div class="divider">
                                                <div class="divider-text text-uppercase text-muted"><small>or login with
                                                        email</small>
                                                </div>
                                            </div> -->

                                           <!-- <div class="input-group-append">
                                                        <span class="input-group-text" onclick="password_show_hide();">
                                                        <i class="fas fa-eye" id="show_eye"></i>
                                                        <i class="fas fa-eye-slash d-none" id="hide_eye"></i>
                                                        </span>
                                                    </div> -->
                                           <?= $this->session->flashdata('message'); ?>

                                           <form class="user" method="post" action="<?= base_url('auth'); ?>">
                                               <!-- <div class="form-group mb-50">
                                                   <label class="text-bold-600" for="exampleInputEmail1">Email address</label>
                                                   <input type="email" class="form-control" id="email" name="email" placeholder="Email address" value="<?= set_value('email'); ?>">
                                                   <?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>
                                               </div> -->
                                               <div class="form-group mb-50">
                                                   <label class="text-bold-600" for="exampleInputEmail1">NIK</label>
                                                   <input type="text" class="form-control" id="email" name="nik" placeholder="NIK" value="<?= set_value('nik'); ?>">
                                                   <?= form_error('nik', '<small class="text-danger pl-3">', '</small>'); ?>
                                               </div>
                                               <div class="form-group">
                                                   <label class="text-bold-600" for="exampleInputPassword1">Password</label>
                                                   <!-- <div class="input-group" id="show_hide_password"> -->
                                                   <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                                                   <!-- <div class="input-group-addon">
                                                            <a href=""><i class="bx bx-right-arrow-alt" aria-hidden="true"></i></a>
                                                        </div> -->
                                                   <!-- </div> -->
                                                   <?= form_error('password', '<small class="text-danger pl-3">', '</small>'); ?>
                                               </div>
                                               <div class="form-group d-flex flex-md-row flex-column justify-content-between align-items-center">
                                                   <div class="text-left">
                                                       <div class="checkbox checkbox-sm">
                                                           <input type="checkbox" class="form-check-input" id="remember" name="remember[]" value="1">
                                                           <label class="checkboxsmall" for="remember"><small>Remember me
                                                               </small></label>
                                                       </div>
                                                   </div>
                                                   <!-- <div class="text-right"><a href="<?= base_url('auth/forgotpassword'); ?>" class="card-link"><small>Forgot Password?</small></a></div> -->
                                               </div>
                                               <button type="submit" class="btn btn-primary glow w-100 position-relative">Login<i id="icon-arrow" class="bx bx-right-arrow-alt"></i></button>
                                           </form>
                                           <hr>
                                           <!-- <div class="text-center"><small class="mr-25">Don't have an account?</small><a href="<?= base_url('auth/registration'); ?>"><small>Sign up</small></a></div> -->
                                       </div>
                                   </div>
                               </div>
                               <!-- right section image -->
                               <div class="col-md-6 d-md-block d-none text-center align-self-center p-3">
                                   <img class="img-fluid" src="<?= base_url('assets/'); ?>app-assets/images/pages/eng_2.svg" alt="branding logo">
                               </div>
                           </div>
                       </div>
                   </div>
               </section>
               <!-- login page ends -->

           </div>
       </div>
   </div>
   <!-- END: Content-->