<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <?= $this->session->flashdata('message_email'); ?>

            <!-- <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1> -->
            <!-- Dashboard Ecommerce Starts -->
            <section id="dashboard-ecommerce">
                <div class="row">
                    <!-- Greetings Content Starts -->
                    <div class="col-xl-4 col-md-6 col-12 dashboard-greetings">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="greeting-text"><?= $title; ?> : Congratulations John!</h3>
                                <p class="mb-0">Best seller of the month</p>
                            </div>
                            <div class="card-body pt-0">
                                <div class="d-flex justify-content-between align-items-end">
                                    <div class="dashboard-content-left">
                                        <h1 class="text-primary font-large-2 text-bold-500">$89k</h1>
                                        <p>You have done 57.6% more sales today.</p>
                                        <a href="<?= base_url('admin/test'); ?>" type="button" class="btn btn-primary glow">Test send email</a>
                                        <a href="<?= base_url('test/export'); ?>" type="button" class="btn btn-danger glow" target="_blank">Test PDF</a>
                                        <!-- <a href="<?= base_url('admin/testingEmail'); ?>" type="button" class="btn btn-primary glow">Test send email</a> -->
                                        <!-- <button type="button" class="btn btn-primary glow">View Sales</button> -->
                                    </div>
                                    <div class="dashboard-content-right">
                                        <img src="<?= base_url('assets/') ?>app-assets/images/icon/cup.png" height="220" width="220" class="img-fluid" alt="Dashboard Ecommerce" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Multi Radial Chart Starts -->
                    <div class="col-xl-4 col-md-6 col-12 dashboard-visit">
                        <div class="card">
                            <div class="card-header d-flex justify-content-between align-items-center">
                                <h4 class="card-title">Visits of 2020</h4>
                                <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
                            </div>
                            <div class="card-body">
                                <div id="multi-radial-chart"></div>
                                <ul class="list-inline text-center mt-1 mb-0">
                                    <li class="mr-2"><span class="bullet bullet-xs bullet-primary mr-50"></span>Target</li>
                                    <li class="mr-2"><span class="bullet bullet-xs bullet-danger mr-50"></span>Mart</li>
                                    <li><span class="bullet bullet-xs bullet-warning mr-50"></span>Ebay</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-12 dashboard-users">
                        <div class="row  ">
                            <!-- Statistics Cards Starts -->
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-sm-6 col-12 dashboard-users-success">
                                        <div class="card text-center">
                                            <div class="card-body py-1">
                                                <div class="badge-circle badge-circle-lg badge-circle-light-success mx-auto mb-50">
                                                    <i class="bx bx-briefcase-alt font-medium-5"></i>
                                                </div>
                                                <div class="text-muted line-ellipsis">New Products</div>
                                                <h3 class="mb-0">1.2k</h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-12 dashboard-users-danger">
                                        <div class="card text-center">
                                            <div class="card-body py-1">
                                                <div class="badge-circle badge-circle-lg badge-circle-light-danger mx-auto mb-50">
                                                    <i class="bx bx-user font-medium-5"></i>
                                                </div>
                                                <div class="text-muted line-ellipsis">New Users</div>
                                                <h3 class="mb-0">45.6k</h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-12 col-lg-6 col-12 dashboard-revenue-growth">
                                        <div class="card">
                                            <div class="card-header d-flex justify-content-between align-items-center pb-0">
                                                <h4 class="card-title">Revenue Growth</h4>
                                                <div class="d-flex align-items-end justify-content-end">
                                                    <span class="mr-25">$25,980</span>
                                                    <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
                                                </div>
                                            </div>
                                            <div class="card-body pb-0">
                                                <div id="revenue-growth-chart"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Revenue Growth Chart Starts -->
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-8 col-12 dashboard-order-summary">
                        <div class="card">
                            <div class="row">
                                <!-- Order Summary Starts -->
                                <div class="col-md-8 col-12 order-summary border-right pr-md-0">
                                    <div class="card mb-0">
                                        <div class="card-header d-flex justify-content-between align-items-center">
                                            <h4 class="card-title">Order Summary</h4>
                                            <div class="d-flex">
                                                <button type="button" class="btn btn-sm btn-light-primary mr-1">Week</button>
                                                <button type="button" class="btn btn-sm btn-primary glow">Month</button>
                                            </div>
                                        </div>
                                        <div class="card-body p-0">
                                            <div id="order-summary-chart"></div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Sales History Starts -->
                                <div class="col-md-4 col-12 pl-md-0">
                                    <div class="card mb-0">
                                        <div class="card-header pb-50">
                                            <h4 class="card-title">Best Sellers</h4>
                                        </div>
                                        <div class="card-body py-1">
                                            <div class="d-flex justify-content-between align-items-center mb-2">
                                                <div class="sales-item-name">
                                                    <p class="mb-0">iPhone</p>
                                                    <small class="text-muted">Smartphone</small>
                                                </div>
                                                <h6 class='mb-0'>794</h6>
                                            </div>
                                            <div class="d-flex justify-content-between align-items-center mb-2">
                                                <div class="sales-item-name">
                                                    <p class="mb-0">Airpods</p>
                                                    <small class="text-muted">Earphone</small>
                                                </div>
                                                <h6 class='mb-0'>550</h6>
                                            </div>
                                            <div class="d-flex justify-content-between align-items-center">
                                                <div class="sales-item-name">
                                                    <p class="mb-0">MacBook</p>
                                                    <small class="text-muted">Laptop</small>
                                                </div>
                                                <h6 class='mb-0'>271</h6>
                                            </div>
                                        </div>
                                        <div class="card-footer border-top pb-md-0">
                                            <h5>Total Sales</h5>
                                            <span class="text-primary text-bold-500">$82,950.96</span>
                                            <div class="progress progress-bar-primary progress-sm mt-50 mb-md-50">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="78" style="width:78%"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Latest Update Starts -->
                    <div class="col-xl-4 col-md-6 col-12 dashboard-latest-update">
                        <div class="card">
                            <div class="card-header d-flex justify-content-between align-items-center pb-50">
                                <h4 class="card-title">Latest Update</h4>
                                <div class="dropdown">
                                    <button class="btn btn-sm btn-outline-secondary dropdown-toggle" type="button" id="dropdownMenuButtonSec" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        2020
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButtonSec">
                                        <a class="dropdown-item" href="javascript:;">2020</a>
                                        <a class="dropdown-item" href="javascript:;">2019</a>
                                        <a class="dropdown-item" href="javascript:;">2018</a>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body p-0 pb-1">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item list-group-item-action border-0 d-flex align-items-center justify-content-between">
                                        <div class="list-left d-flex">
                                            <div class="list-icon mr-1">
                                                <div class="avatar bg-rgba-primary m-0">
                                                    <div class="avatar-content">
                                                        <i class="bx bxs-zap text-primary font-size-base"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="list-content">
                                                <span class="list-title">Total Products</span>
                                                <small class="text-muted d-block">2k New Products</small>
                                            </div>
                                        </div>
                                        <span>10k</span>
                                    </li>
                                    <li class="list-group-item list-group-item-action border-0 d-flex align-items-center justify-content-between">
                                        <div class="list-left d-flex">
                                            <div class="list-icon mr-1">
                                                <div class="avatar bg-rgba-info m-0">
                                                    <div class="avatar-content">
                                                        <i class="bx bx-stats text-info font-size-base"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="list-content">
                                                <span class="list-title">Total Sales</span>
                                                <small class="text-muted d-block">39k New Sales</small>
                                            </div>
                                        </div>
                                        <span>26M</span>
                                    </li>
                                    <li class="list-group-item list-group-item-action border-0 d-flex align-items-center justify-content-between">
                                        <div class="list-left d-flex">
                                            <div class="list-icon mr-1">
                                                <div class="avatar bg-rgba-danger m-0">
                                                    <div class="avatar-content">
                                                        <i class="bx bx-credit-card text-danger font-size-base"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="list-content">
                                                <span class="list-title">Total Revenue</span>
                                                <small class="text-muted d-block">43k New Revenue</small>
                                            </div>
                                        </div>
                                        <span>15M</span>
                                    </li>
                                    <li class="list-group-item list-group-item-action border-0 d-flex align-items-center justify-content-between">
                                        <div class="list-left d-flex">
                                            <div class="list-icon mr-1">
                                                <div class="avatar bg-rgba-success m-0">
                                                    <div class="avatar-content">
                                                        <i class="bx bx-dollar text-success font-size-base"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="list-content">
                                                <span class="list-title">Total Cost</span>
                                                <small class="text-muted d-block">Total Expenses</small>
                                            </div>
                                        </div>
                                        <span>2B</span>
                                    </li>
                                    <li class="list-group-item list-group-item-action border-0 d-flex align-items-center justify-content-between">
                                        <div class="list-left d-flex">
                                            <div class="list-icon mr-1">
                                                <div class="avatar bg-rgba-primary m-0">
                                                    <div class="avatar-content">
                                                        <i class="bx bx-user text-primary font-size-base"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="list-content">
                                                <span class="list-title">Total Users</span>
                                                <small class="text-muted d-block">New Users</small>
                                            </div>
                                        </div>
                                        <span>2k</span>
                                    </li>
                                    <li class="list-group-item list-group-item-action border-0 d-flex align-items-center justify-content-between">
                                        <div class="list-left d-flex">
                                            <div class="list-icon mr-1">
                                                <div class="avatar bg-rgba-danger m-0">
                                                    <div class="avatar-content">
                                                        <i class="bx bx-edit-alt text-danger font-size-base"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="list-content">
                                                <span class="list-title">Total Visits</span>
                                                <small class="text-muted d-block">New Visits</small>
                                            </div>
                                        </div>
                                        <span>46k</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- Earning Swiper Starts -->
                    <div class="col-xl-4 col-md-6 col-12 dashboard-earning-swiper" id="widget-earnings">
                        <div class="card">
                            <div class="card-header border-bottom d-flex justify-content-between align-items-center">
                                <h5 class="card-title"><i class="bx bx-dollar font-medium-5 align-middle"></i> <span class="align-middle">Earnings</span></h5>
                                <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
                            </div>
                            <div class="card-body py-1 px-0">
                                <!-- earnings swiper starts -->
                                <div class="widget-earnings-swiper swiper-container p-1">
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide rounded swiper-shadow py-50 px-2 d-flex align-items-center" id="repo-design">
                                            <i class="bx bx-pyramid mr-1 font-weight-normal font-medium-4"></i>
                                            <div class="swiper-text">
                                                <div class="swiper-heading">Repo Design</div>
                                                <small class="d-block">Gitlab</small>
                                            </div>
                                        </div>
                                        <div class="swiper-slide rounded swiper-shadow py-50 px-2 d-flex align-items-center" id="laravel-temp">
                                            <i class="bx bx-sitemap mr-50 font-large-1"></i>
                                            <div class="swiper-text">
                                                <div class="swiper-heading">Designer</div>
                                                <small class="d-block">Women Clothes</small>
                                            </div>
                                        </div>
                                        <div class="swiper-slide rounded swiper-shadow py-50 px-2 d-flex align-items-center" id="admin-theme">
                                            <i class="bx bx-check-shield mr-50 font-large-1"></i>
                                            <div class="swiper-text">
                                                <div class="swiper-heading">Best Sellers</div>
                                                <small class="d-block">Clothing</small>
                                            </div>
                                        </div>
                                        <div class="swiper-slide rounded swiper-shadow py-50 px-2 d-flex align-items-center" id="ux-developer">
                                            <i class="bx bx-devices mr-50 font-large-1"></i>
                                            <div class="swiper-text">
                                                <div class="swiper-heading">Admin Template</div>
                                                <small class="d-block">Global Network</small>
                                            </div>
                                        </div>
                                        <div class="swiper-slide rounded swiper-shadow py-50 px-2 d-flex align-items-center" id="marketing-guide">
                                            <i class="bx bx-book-bookmark mr-50 font-large-1"></i>
                                            <div class="swiper-text">
                                                <div class="swiper-heading">Marketing Guide</div>
                                                <small class="d-block">Books</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- earnings swiper ends -->
                            </div>
                            <div class="main-wrapper-content">
                                <div class="wrapper-content" data-earnings="repo-design">
                                    <div class="widget-earnings-scroll table-responsive">
                                        <table class="table table-borderless widget-earnings-width mb-0">
                                            <tbody>
                                                <tr>
                                                    <td class="pr-75">
                                                        <div class="media align-items-center">
                                                            <a class="media-left mr-50" href="javascript:;">
                                                                <img src="../../../app-assets/images/portrait/small/avatar-s-10.jpg" alt="avatar" class="rounded-circle" height="30" width="30">
                                                            </a>
                                                            <div class="media-body">
                                                                <h6 class="media-heading mb-0">Jerry Lter</h6>
                                                                <span class="font-small-2">Designer</span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="px-0 w-25">
                                                        <div class="progress progress-bar-info progress-sm mb-0">
                                                            <div class="progress-bar" role="progressbar" aria-valuenow="33" aria-valuemin="80" aria-valuemax="100" style="width:33%;"></div>
                                                        </div>
                                                    </td>
                                                    <td class="text-center"><span class="badge badge-light-warning">- $280</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="pr-75">
                                                        <div class="media align-items-center">
                                                            <a class="media-left mr-50" href="javascript:;">
                                                                <img src="../../../app-assets/images/portrait/small/avatar-s-11.jpg" alt="avatar" class="rounded-circle" height="30" width="30">
                                                            </a>
                                                            <div class="media-body">
                                                                <h6 class="media-heading mb-0">Pauly uez</h6>
                                                                <span class="font-small-2">Devloper</span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="px-0 w-25">
                                                        <div class="progress progress-bar-success progress-sm mb-0">
                                                            <div class="progress-bar" role="progressbar" aria-valuenow="10" aria-valuemin="80" aria-valuemax="100" style="width:10%;"></div>
                                                        </div>
                                                    </td>
                                                    <td class="text-center"><span class="badge badge-light-success">+ $853</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="pr-75">
                                                        <div class="media align-items-center">
                                                            <a class="media-left mr-50" href="javascript:;">
                                                                <img src="../../../app-assets/images/portrait/small/avatar-s-11.jpg" alt="avatar" class="rounded-circle" height="30" width="30">
                                                            </a>
                                                            <div class="media-body">
                                                                <h6 class="media-heading mb-0">Lary Masey</h6>
                                                                <span class="font-small-2">Marketing</span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="px-0 w-25">
                                                        <div class="progress progress-bar-primary progress-sm mb-0">
                                                            <div class="progress-bar" role="progressbar" aria-valuenow="15" aria-valuemin="80" aria-valuemax="100" style="width:15%;"></div>
                                                        </div>
                                                    </td>
                                                    <td class="text-center"><span class="badge badge-light-primary">+ $125</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="pr-75">
                                                        <div class="media align-items-center">
                                                            <a class="media-left mr-50" href="javascript:;">
                                                                <img src="../../../app-assets/images/portrait/small/avatar-s-12.jpg" alt="avatar" class="rounded-circle" height="30" width="30">
                                                            </a>
                                                            <div class="media-body">
                                                                <h6 class="media-heading mb-0">Lula Taylor</h6>
                                                                <span class="font-small-2">Degigner</span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="px-0 w-25">
                                                        <div class="progress progress-bar-danger progress-sm mb-0">
                                                            <div class="progress-bar" role="progressbar" aria-valuenow="35" aria-valuemin="80" aria-valuemax="100" style="width:35%;"></div>
                                                        </div>
                                                    </td>
                                                    <td class="text-center"><span class="badge badge-light-danger">- $310</span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="wrapper-content" data-earnings="laravel-temp">
                                    <div class="widget-earnings-scroll table-responsive">
                                        <table class="table table-borderless widget-earnings-width mb-0">
                                            <tbody>
                                                <tr>
                                                    <td class="pr-75">
                                                        <div class="media align-items-center">
                                                            <a class="media-left mr-50" href="javascript:;">
                                                                <img src="../../../app-assets/images/portrait/small/avatar-s-9.jpg" alt="avatar" class="rounded-circle" height="30" width="30">
                                                            </a>
                                                            <div class="media-body">
                                                                <h6 class="media-heading mb-0">Jesus Lter</h6>
                                                                <span class="font-small-2">Designer</span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="px-0 w-25">
                                                        <div class="progress progress-bar-info progress-sm mb-0">
                                                            <div class="progress-bar" role="progressbar" aria-valuenow="28" aria-valuemin="80" aria-valuemax="100" style="width:28%;"></div>
                                                        </div>
                                                    </td>
                                                    <td class="text-center"><span class="badge badge-light-info">- $280</span></td>
                                                </tr>
                                                <tr>
                                                    <td class="pr-75">
                                                        <div class="media align-items-center">
                                                            <a class="media-left mr-50" href="javascript:;">
                                                                <img src="../../../app-assets/images/portrait/small/avatar-s-10.jpg" alt="avatar" class="rounded-circle" height="30" width="30">
                                                            </a>
                                                            <div class="media-body">
                                                                <h6 class="media-heading mb-0">Pauly Dez</h6>
                                                                <span class="font-small-2">Devloper</span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="px-0 w-25">
                                                        <div class="progress progress-bar-success progress-sm mb-0">
                                                            <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="80" aria-valuemax="100" style="width:90%;"></div>
                                                        </div>
                                                    </td>
                                                    <td class="text-center"><span class="badge badge-light-success">+ $83</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="pr-75">
                                                        <div class="media align-items-center">
                                                            <a class="media-left mr-50" href="javascript:;">
                                                                <img src="../../../app-assets/images/portrait/small/avatar-s-11.jpg" alt="avatar" class="rounded-circle" height="30" width="30">
                                                            </a>
                                                            <div class="media-body">
                                                                <h6 class="media-heading mb-0">Lary Masey</h6>
                                                                <span class="font-small-2">Marketing</span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="px-0 w-25">
                                                        <div class="progress progress-bar-primary progress-sm mb-0">
                                                            <div class="progress-bar" role="progressbar" aria-valuenow="15" aria-valuemin="80" aria-valuemax="100" style="width:15%;"></div>
                                                        </div>
                                                    </td>
                                                    <td class="text-center"><span class="badge badge-light-primary">+ $125</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="pr-75">
                                                        <div class="media align-items-center">
                                                            <a class="media-left mr-50" href="javascript:;">
                                                                <img src="../../../app-assets/images/portrait/small/avatar-s-12.jpg" alt="avatar" class="rounded-circle" height="30" width="30">
                                                            </a>
                                                            <div class="media-body">
                                                                <h6 class="media-heading mb-0">Lula Taylor</h6>
                                                                <span class="font-small-2">Devloper</span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="px-0 w-25">
                                                        <div class="progress progress-bar-danger progress-sm mb-0">
                                                            <div class="progress-bar" role="progressbar" aria-valuenow="35" aria-valuemin="80" aria-valuemax="100" style="width:35%;"></div>
                                                        </div>
                                                    </td>
                                                    <td class="text-center"><span class="badge badge-light-danger">- $310</span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="wrapper-content" data-earnings="admin-theme">
                                    <div class="widget-earnings-scroll table-responsive">
                                        <table class="table table-borderless widget-earnings-width mb-0">
                                            <tbody>
                                                <tr>
                                                    <td class="pr-75">
                                                        <div class="media align-items-center">
                                                            <a class="media-left mr-50" href="javascript:;">
                                                                <img src="../../../app-assets/images/portrait/small/avatar-s-25.jpg" alt="avatar" class="rounded-circle" height="30" width="30">
                                                            </a>
                                                            <div class="media-body">
                                                                <h6 class="media-heading mb-0">Mera Lter</h6>
                                                                <span class="font-small-2">Designer</span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="px-0 w-25">
                                                        <div class="progress progress-bar-info progress-sm mb-0">
                                                            <div class="progress-bar" role="progressbar" aria-valuenow="52" aria-valuemin="80" aria-valuemax="100" style="width:52%;"></div>
                                                        </div>
                                                    </td>
                                                    <td class="text-center"><span class="badge badge-light-info">- $180</span></td>
                                                </tr>
                                                <tr>
                                                    <td class="pr-75">
                                                        <div class="media align-items-center">
                                                            <a class="media-left mr-50" href="javascript:;">
                                                                <img src="../../../app-assets/images/portrait/small/avatar-s-15.jpg" alt="avatar" class="rounded-circle" height="30" width="30">
                                                            </a>
                                                            <div class="media-body">
                                                                <h6 class="media-heading mb-0">Pauly Dez</h6>
                                                                <span class="font-small-2">Devloper</span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="px-0 w-25">
                                                        <div class="progress progress-bar-success progress-sm mb-0">
                                                            <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="80" aria-valuemax="100" style="width:90%;"></div>
                                                        </div>
                                                    </td>
                                                    <td class="text-center"><span class="badge badge-light-success">+ $553</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="pr-75">
                                                        <div class="media align-items-center">
                                                            <a class="media-left mr-50" href="javascript:;">
                                                                <img src="../../../app-assets/images/portrait/small/avatar-s-11.jpg" alt="avatar" class="rounded-circle" height="30" width="30">
                                                            </a>
                                                            <div class="media-body">
                                                                <h6 class="media-heading mb-0">jini mara</h6>
                                                                <span class="font-small-2">Marketing</span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="px-0 w-25">
                                                        <div class="progress progress-bar-primary progress-sm mb-0">
                                                            <div class="progress-bar" role="progressbar" aria-valuenow="15" aria-valuemin="80" aria-valuemax="100" style="width:15%;"></div>
                                                        </div>
                                                    </td>
                                                    <td class="text-center"><span class="badge badge-light-primary">+ $125</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="pr-75">
                                                        <div class="media align-items-center">
                                                            <a class="media-left mr-50" href="javascript:;">
                                                                <img src="../../../app-assets/images/portrait/small/avatar-s-12.jpg" alt="avatar" class="rounded-circle" height="30" width="30">
                                                            </a>
                                                            <div class="media-body">
                                                                <h6 class="media-heading mb-0">Lula Taylor</h6>
                                                                <span class="font-small-2">UX</span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="px-0 w-25">
                                                        <div class="progress progress-bar-danger progress-sm mb-0">
                                                            <div class="progress-bar" role="progressbar" aria-valuenow="35" aria-valuemin="80" aria-valuemax="100" style="width:35%;"></div>
                                                        </div>
                                                    </td>
                                                    <td class="text-center"><span class="badge badge-light-danger">- $150</span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="wrapper-content" data-earnings="ux-developer">
                                    <div class="widget-earnings-scroll table-responsive">
                                        <table class="table table-borderless widget-earnings-width mb-0">
                                            <tbody>
                                                <tr>
                                                    <td class="pr-75">
                                                        <div class="media align-items-center">
                                                            <a class="media-left mr-50" href="javascript:;">
                                                                <img src="../../../app-assets/images/portrait/small/avatar-s-16.jpg" alt="avatar" class="rounded-circle" height="30" width="30">
                                                            </a>
                                                            <div class="media-body">
                                                                <h6 class="media-heading mb-0">Drako Lter</h6>
                                                                <span class="font-small-2">Designer</span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="px-0 w-25">
                                                        <div class="progress progress-bar-info progress-sm mb-0">
                                                            <div class="progress-bar" role="progressbar" aria-valuenow="38" aria-valuemin="80" aria-valuemax="100" style="width:38%;"></div>
                                                        </div>
                                                    </td>
                                                    <td class="text-center"><span class="badge badge-light-danger">- $280</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="pr-75">
                                                        <div class="media align-items-center">
                                                            <a class="media-left mr-50" href="javascript:;">
                                                                <img src="../../../app-assets/images/portrait/small/avatar-s-1.jpg" alt="avatar" class="rounded-circle" height="30" width="30">
                                                            </a>
                                                            <div class="media-body">
                                                                <h6 class="media-heading mb-0">Pauly Dez</h6>
                                                                <span class="font-small-2">Devloper</span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="px-0 w-25">
                                                        <div class="progress progress-bar-success progress-sm mb-0">
                                                            <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="80" aria-valuemax="100" style="width:90%;"></div>
                                                        </div>
                                                    </td>
                                                    <td class="text-center"><span class="badge badge-light-success">+ $853</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="pr-75">
                                                        <div class="media align-items-center">
                                                            <a class="media-left mr-50" href="javascript:;">
                                                                <img src="../../../app-assets/images/portrait/small/avatar-s-11.jpg" alt="avatar" class="rounded-circle" height="30" width="30">
                                                            </a>
                                                            <div class="media-body">
                                                                <h6 class="media-heading mb-0">Lary Masey</h6>
                                                                <span class="font-small-2">Marketing</span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="px-0 w-25">
                                                        <div class="progress progress-bar-primary progress-sm mb-0">
                                                            <div class="progress-bar" role="progressbar" aria-valuenow="15" aria-valuemin="80" aria-valuemax="100" style="width:15%;"></div>
                                                        </div>
                                                    </td>
                                                    <td class="text-center"><span class="badge badge-light-primary">+ $125</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="pr-75">
                                                        <div class="media align-items-center">
                                                            <a class="media-left mr-50" href="javascript:;">
                                                                <img src="../../../app-assets/images/portrait/small/avatar-s-2.jpg" alt="avatar" class="rounded-circle" height="30" width="30">
                                                            </a>
                                                            <div class="media-body">
                                                                <h6 class="media-heading mb-0">Lvia Taylor</h6>
                                                                <span class="font-small-2">Devloper</span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="px-0 w-25">
                                                        <div class="progress progress-bar-danger progress-sm mb-0">
                                                            <div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="80" aria-valuemax="100" style="width:75%;"></div>
                                                        </div>
                                                    </td>
                                                    <td class="text-center"><span class="badge badge-light-danger">- $360</span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="wrapper-content" data-earnings="marketing-guide">
                                    <div class="widget-earnings-scroll table-responsive">
                                        <table class="table table-borderless widget-earnings-width mb-0">
                                            <tbody>
                                                <tr>
                                                    <td class="pr-75">
                                                        <div class="media align-items-center">
                                                            <a class="media-left mr-50" href="javascript:;">
                                                                <img src="../../../app-assets/images/portrait/small/avatar-s-19.jpg" alt="avatar" class="rounded-circle" height="30" width="30">
                                                            </a>
                                                            <div class="media-body">
                                                                <h6 class="media-heading mb-0">yono Lter</h6>
                                                                <span class="font-small-2">Designer</span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="px-0 w-25">
                                                        <div class="progress progress-bar-info progress-sm mb-0">
                                                            <div class="progress-bar" role="progressbar" aria-valuenow="28" aria-valuemin="80" aria-valuemax="100" style="width:28%;"></div>
                                                        </div>
                                                    </td>
                                                    <td class="text-center"><span class="badge badge-light-primary">- $270</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="pr-75">
                                                        <div class="media align-items-center">
                                                            <a class="media-left mr-50" href="javascript:;">
                                                                <img src="../../../app-assets/images/portrait/small/avatar-s-11.jpg" alt="avatar" class="rounded-circle" height="30" width="30">
                                                            </a>
                                                            <div class="media-body">
                                                                <h6 class="media-heading mb-0">Pauly Dez</h6>
                                                                <span class="font-small-2">Devloper</span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="px-0 w-25">
                                                        <div class="progress progress-bar-success progress-sm mb-0">
                                                            <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="80" aria-valuemax="100" style="width:90%;"></div>
                                                        </div>
                                                    </td>
                                                    <td class="text-center"><span class="badge badge-light-success">+ $853</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="pr-75">
                                                        <div class="media align-items-center">
                                                            <a class="media-left mr-50" href="javascript:;">
                                                                <img src="../../../app-assets/images/portrait/small/avatar-s-12.jpg" alt="avatar" class="rounded-circle" height="30" width="30">
                                                            </a>
                                                            <div class="media-body">
                                                                <h6 class="media-heading mb-0">Lary Masey</h6>
                                                                <span class="font-small-2">Marketing</span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="px-0 w-25">
                                                        <div class="progress progress-bar-primary progress-sm mb-0">
                                                            <div class="progress-bar" role="progressbar" aria-valuenow="15" aria-valuemin="80" aria-valuemax="100" style="width:15%;"></div>
                                                        </div>
                                                    </td>
                                                    <td class="text-center"><span class="badge badge-light-primary">+ $225</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="pr-75">
                                                        <div class="media align-items-center">
                                                            <a class="media-left mr-50" href="javascript:;">
                                                                <img src="../../../app-assets/images/portrait/small/avatar-s-25.jpg" alt="avatar" class="rounded-circle" height="30" width="30">
                                                            </a>
                                                            <div class="media-body">
                                                                <h6 class="media-heading mb-0">Lula Taylor</h6>
                                                                <span class="font-small-2">Devloper</span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="px-0 w-25">
                                                        <div class="progress progress-bar-danger progress-sm mb-0">
                                                            <div class="progress-bar" role="progressbar" aria-valuenow="35" aria-valuemin="80" aria-valuemax="100" style="width:35%;"></div>
                                                        </div>
                                                    </td>
                                                    <td class="text-center"><span class="badge badge-light-danger">- $350</span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Marketing Campaigns Starts -->
                    <div class="col-xl-8 col-12 dashboard-marketing-campaign">
                        <div class="card marketing-campaigns">
                            <div class="card-header d-flex justify-content-between align-items-center pb-1">
                                <h4 class="card-title">Marketing campaigns</h4>
                                <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
                            </div>
                            <div class="card-body pb-0">
                                <div class="row mb-1">
                                    <div class="col-md-9 col-12">
                                        <div class="d-inline-block">
                                            <!-- chart-1   -->
                                            <div class="d-flex market-statistics-1">
                                                <!-- chart-statistics-1 -->
                                                <div id="donut-success-chart" class="mx-1"></div>
                                                <!-- data -->
                                                <div class="statistics-data my-auto">
                                                    <div class="statistics">
                                                        <span class="font-medium-2 mr-50 text-bold-600">25,756</span><span class="text-success">(+16.2%)</span>
                                                    </div>
                                                    <div class="statistics-date">
                                                        <i class="bx bx-radio-circle font-small-1 text-success mr-25"></i>
                                                        <small class="text-muted">May 12, 2020</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="d-inline-block">
                                            <!-- chart-2 -->
                                            <div class="d-flex mb-75 market-statistics-2">
                                                <!-- chart statistics-2 -->
                                                <div id="donut-danger-chart" class="mx-1"></div>
                                                <!-- data-2 -->
                                                <div class="statistics-data my-auto">
                                                    <div class="statistics">
                                                        <span class="font-medium-2 mr-50 text-bold-600">5,352</span><span class="text-danger">(-4.9%)</span>
                                                    </div>
                                                    <div class="statistics-date">
                                                        <i class="bx bx-radio-circle font-small-1 text-success mr-25"></i>
                                                        <small class="text-muted">Jul 26, 2020</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-12 text-md-right">
                                        <button class="btn btn-sm btn-primary glow mt-md-2 mb-1">View Report</button>
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <!-- table start -->
                                <table id="table-marketing-campaigns" class="table table-borderless table-marketing-campaigns mb-0">
                                    <thead>
                                        <tr>
                                            <th>Campaign</th>
                                            <th>Growth</th>
                                            <th>Charges</th>
                                            <th>Status</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="py-1 line-ellipsis">
                                                <img class="rounded-circle mr-1" src="../../../app-assets/images/icon/fs.png" alt="card" height="24" width="24">Fastrack Watches
                                            </td>
                                            <td class="py-1">
                                                <i class="bx bx-trending-up text-success align-middle mr-50"></i><span>30%</span>
                                            </td>
                                            <td class="py-1">$5,536</td>
                                            <td class="text-success py-1">Active</td>
                                            <td class="text-center py-1">
                                                <div class="dropdown">
                                                    <span class="bx bx-dots-vertical-rounded font-medium-3 dropdown-toggle nav-hide-arrow cursor-pointer" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="menu"></span>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <a class="dropdown-item" href="javascript:;"><i class="bx bx-edit-alt mr-1"></i> edit</a>
                                                        <a class="dropdown-item" href="javascript:;"><i class="bx bx-trash mr-1"></i> delete</a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="py-1 line-ellipsis">
                                                <img class="rounded-circle mr-1" src="../../../app-assets/images/icon/puma.png" alt="card" height="24" width="24">Puma Shoes
                                            </td>
                                            <td class="py-1">
                                                <i class="bx bx-trending-down text-danger align-middle mr-50"></i><span>15.5%</span>
                                            </td>
                                            <td class="py-1">$1,569</td>
                                            <td class="text-success py-1">Active</td>
                                            <td class="text-center py-1">
                                                <div class="dropdown">
                                                    <span class="bx bx-dots-vertical-rounded font-medium-3 dropdown-toggle nav-hide-arrow cursor-pointer" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="menu">
                                                    </span>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <a class="dropdown-item" href="javascript:;"><i class="bx bx-edit-alt mr-1"></i> edit</a>
                                                        <a class="dropdown-item" href="javascript:;"><i class="bx bx-trash mr-1"></i> delete</a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="py-1 line-ellipsis">
                                                <img class="rounded-circle mr-1" src="../../../app-assets/images/icon/nike.png" alt="card" height="24" width="24">Nike Air Jordan
                                            </td>
                                            <td class="py-1">
                                                <i class="bx bx-trending-up text-success align-middle mr-50"></i><span>70.3%</span>
                                            </td>
                                            <td class="py-1">$23,859</td>
                                            <td class="text-danger py-1">Closed</td>
                                            <td class="text-center py-1">
                                                <div class="dropdown">
                                                    <span class="bx bx-dots-vertical-rounded font-medium-3 dropdown-toggle nav-hide-arrow cursor-pointer" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="menu">
                                                    </span>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <a class="dropdown-item" href="javascript:;"><i class="bx bx-edit-alt mr-1"></i> edit</a>
                                                        <a class="dropdown-item" href="javascript:;"><i class="bx bx-trash mr-1"></i> delete</a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="py-1 line-ellipsis">
                                                <img class="rounded-circle mr-1" src="../../../app-assets/images/icon/one-plus.png" alt="card" height="24" width="24">Oneplus 7 pro
                                            </td>
                                            <td class="py-1">
                                                <i class="bx bx-trending-up text-success align-middle mr-50"></i><span>10.4%</span>
                                            </td>
                                            <td class="py-1">$9,523</td>
                                            <td class="text-success py-1">Active</td>
                                            <td class="text-center py-1">
                                                <div class="dropdown">
                                                    <span class="bx bx-dots-vertical-rounded font-medium-3 dropdown-toggle nav-hide-arrow cursor-pointer" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="menu">
                                                    </span>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <a class="dropdown-item" href="javascript:;"><i class="bx bx-edit-alt mr-1"></i> edit</a>
                                                        <a class="dropdown-item" href="javascript:;"><i class="bx bx-trash mr-1"></i> delete</a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="py-1 line-ellipsis">
                                                <img class="rounded-circle mr-1" src="../../../app-assets/images/icon/google.png" alt="card" height="24" width="24">Google Pixel 4 xl
                                            </td>
                                            <td class="py-1"><i class="bx bx-trending-down text-danger align-middle mr-50"></i><span>-62.38%</span>
                                            </td>
                                            <td class="py-1">$12,897</td>
                                            <td class="text-danger py-1">Closed</td>
                                            <td class="text-center py-1">
                                                <div class="dropup">
                                                    <span class="bx bx-dots-vertical-rounded font-medium-3 dropdown-toggle nav-hide-arrow cursor-pointer" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="menu">
                                                    </span>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <a class="dropdown-item" href="javascript:;"><i class="bx bx-edit-alt mr-1"></i> edit</a>
                                                        <a class="dropdown-item" href="javascript:;"><i class="bx bx-trash mr-1"></i> delete</a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <!-- table ends -->
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Dashboard Ecommerce ends -->

            <!-- line chart section start -->
            <section id="chartjs-charts">
                <!-- Line Chart -->
                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Line Chart</h4>
                            </div>
                            <div class="card-body pl-0">
                                <div class="height-300">
                                    <canvas id="line-chart"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Bar Chart -->
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Bar Chart</h4>
                            </div>
                            <div class="card-body pl-0">
                                <div class="height-300">
                                    <canvas id="bar-chart"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <!-- MyChart Chart -->
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">MyChart</h4>
                            </div>
                            <div class="card-body pl-0">
                                <div class="height-350">
                                    <canvas id="myChart"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- MyChartx Chart -->
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">MyChartx</h4>
                            </div>
                            <div class="card-body pl-0">
                                <div class="height-350">
                                    <canvas id="myChartx"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- MyChartz Chart -->
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">MyChartz</h4>
                            </div>
                            <div class="card-body pl-1">
                                <div class="height-350">
                                    <div id="myChartz">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ChartArea Chart -->
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Chart Area</h4>
                            </div>
                            <div class="card-body pl-1">
                                <div class="height-350">
                                    <div id="chartArea">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Horizontal Chart -->
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">my Column Chart</h4>
                            </div>
                            <div class="card-body pl-1">
                                <div class="height-350">
                                    <div id="myColumnChart"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">my Stroked Chart</h4>
                            </div>
                            <div class="card-body pl-1">
                                <div class="height-350">
                                    <div id="strokedChart"></div>
                                    <!-- <div id="growthChart"></div> -->
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Horizontal Chart -->
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Horizontal Bar Chart</h4>
                            </div>
                            <div class="card-body pl-0">
                                <div class="height-300">
                                    <canvas id="horizontal-bar"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Pie Chart -->
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Pie Chart</h4>
                            </div>
                            <div class="card-body pl-0">
                                <div class="height-300">
                                    <canvas id="simple-pie-chart"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <!-- Doughnut Chart -->
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Doughnut Chart</h4>
                            </div>
                            <div class="card-body">
                                <div class="height-300">
                                    <canvas id="simple-doughnut-chart"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Radar Chart -->
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Radar Chart</h4>
                            </div>
                            <div class="card-body">
                                <div class="height-300">
                                    <canvas id="radar-chart"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Polar & Radar Chart -->
                <div class="row">
                    <!-- Polar Chart -->
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Polar Chart</h4>
                            </div>
                            <div class="card-body">
                                <div class="height-300">
                                    <canvas id="polar-chart"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>


                    <!-- Bubble Chart -->
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Bubble Chart</h4>
                            </div>
                            <div class="card-body">
                                <div class="height-300">
                                    <canvas id="bubble-chart" width="300"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Scatter logX Chart -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Scatter Chart</h4>
                            </div>
                            <div class="card-body">
                                <div class="height-300">
                                    <canvas id="scatter-chart"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- line chart section end -->

            <section id="dashboard-analytics">
                <div class="row">
                    <!-- Website Analytics Starts-->
                    <div class="col-md-6 col-sm-12">
                        <div class="card">
                            <div class="card-header d-flex justify-content-between align-items-center">
                                <h4 class="card-title">Website Analytics</h4>
                                <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
                            </div>
                            <div class="card-body pb-1">
                                <div class="d-flex justify-content-around align-items-center flex-wrap">
                                    <div class="user-analytics mr-2">
                                        <i class="bx bx-user mr-25 align-middle"></i>
                                        <span class="align-middle text-muted">Users</span>
                                        <div class="d-flex">
                                            <div id="radial-success-chart"></div>
                                            <h3 class="mt-1 ml-50">61K</h3>
                                        </div>
                                    </div>
                                    <div class="sessions-analytics mr-2">
                                        <i class="bx bx-trending-up align-middle mr-25"></i>
                                        <span class="align-middle text-muted">Sessions</span>
                                        <div class="d-flex">
                                            <div id="radial-warning-chart"></div>
                                            <h3 class="mt-1 ml-50">92K</h3>
                                        </div>
                                    </div>
                                    <div class="bounce-rate-analytics">
                                        <i class="bx bx-pie-chart-alt align-middle mr-25"></i>
                                        <span class="align-middle text-muted">Bounce Rate</span>
                                        <div class="d-flex">
                                            <div id="radial-danger-chart"></div>
                                            <h3 class="mt-1 ml-50">72.6%</h3>
                                        </div>
                                    </div>
                                </div>
                                <div id="analytics-bar-chart" class="my-75"></div>
                            </div>
                        </div>

                    </div>
                    <div class="col-xl-3 col-md-6 col-sm-12 dashboard-referral-impression">
                        <div class="row">
                            <!-- Referral Chart Starts-->
                            <div class="col-xl-12 col-12">
                                <div class="card">
                                    <div class="card-body text-center pb-0">
                                        <h2>$32,690</h2>
                                        <span class="text-muted">Referral 40%</span>
                                        <div id="success-line-chart"></div>
                                    </div>
                                </div>
                            </div>
                            <!-- Impression Radial Chart Starts-->
                            <div class="col-xl-12 col-12">
                                <div class="card">
                                    <div class="card-body donut-chart-wrapper">
                                        <div id="donut-chart" class="d-flex justify-content-center"></div>
                                        <ul class="list-inline d-flex justify-content-around mb-0">
                                            <li> <span class="bullet bullet-xs bullet-primary mr-50"></span>Social</li>
                                            <li> <span class="bullet bullet-xs bullet-info mr-50"></span>Email</li>
                                            <li> <span class="bullet bullet-xs bullet-warning mr-50"></span>Search</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-12 col-sm-12">
                        <div class="row">
                            <!-- Conversion Chart Starts-->
                            <div class="col-xl-12 col-md-6 col-12">
                                <div class="card">
                                    <div class="card-header d-flex justify-content-between pb-xl-0 pt-xl-1">
                                        <div class="conversion-title">
                                            <h4 class="card-title">Conversion</h4>
                                            <p>60%
                                                <i class="bx bx-trending-up text-success font-size-small align-middle mr-25"></i>
                                            </p>
                                        </div>
                                        <div class="conversion-rate">
                                            <h2>89k</h2>
                                        </div>
                                    </div>
                                    <div class="card-body text-center">
                                        <div id="bar-negative-chart" class="negative-bar-chart"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-12 col-md-6 col-12">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card">
                                            <div class="card-body d-flex align-items-center justify-content-between">
                                                <div class="d-flex align-items-center">
                                                    <div class="avatar bg-rgba-primary m-0 p-25 mr-75 mr-xl-2">
                                                        <div class="avatar-content">
                                                            <i class="bx bx-user text-primary font-medium-2"></i>
                                                        </div>
                                                    </div>
                                                    <div class="total-amount">
                                                        <h5 class="mb-0">$38,566</h5>
                                                        <small class="text-muted">Conversion</small>
                                                    </div>
                                                </div>
                                                <div id="primary-line-chart"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="card">
                                            <div class="card-body d-flex align-items-center justify-content-between">
                                                <div class="d-flex align-items-center">
                                                    <div class="avatar bg-rgba-warning m-0 p-25 mr-75 mr-xl-2">
                                                        <div class="avatar-content">
                                                            <i class="bx bx-dollar text-warning font-medium-2"></i>
                                                        </div>
                                                    </div>
                                                    <div class="total-amount">
                                                        <h5 class="mb-0">$53,659</h5>
                                                        <small class="text-muted">Income</small>
                                                    </div>
                                                </div>
                                                <div id="warning-line-chart"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


        </div>
    </div>
</div>
</div>


<!-- Zero configuration table -->
<!-- <section id="basic-datatable">
     <div class="row">
         <div class="col-12">
             <div class="card">
                 <div class="card-header">
                     <h4 class="card-title"><?= $title; ?></h4>
                 </div>
                 <div class="card-body card-dashboard">
                     <a href="" class="btn btn-primary mb-1" data-toggle="modal" data-target="#newRoleModal">Add New Role</a>
                     <div class="table-responsive">

                         <?= form_error('menu', '<div class="alert alert-danger" role="alert">', '</div>'); ?>

                         <?= $this->session->flashdata('message'); ?>

                         <table class="table zero-configuration">
                             <thead>
                                 <tr>
                                     <th>#</th>
                                     <th>Nama</th>
                                     <th>Email</th>
                                     <th>Image</th>
                                     <th>Date Created</th>
                                 </tr>
                             </thead>
                             <tbody>
                                 <?php $i = 1; ?>
                                 <?php foreach ($test as $r) : ?>
                                     <tr>
                                         <th scope="row"><?= $i; ?></th>
                                         <td><?= $r['name']; ?></td>
                                         <td><?= $r['email']; ?></td>
                                         <td><?= $r['image']; ?></td>
                                         <td><?= date("d/m/Y H:i:s", $r['date_created']); ?></td>
                                     </tr>
                                     <?php $i++; ?>
                                 <?php endforeach; ?>
                             </tbody>
                         </table>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </section> -->
<!--/ Zero configuration table -->

<script>
    var $primary = '#5A8DEE';
    var $success = '#39DA8A';
    var $danger = '#FF5B5C';
    var $warning = '#FDAC41';
    var $info = '#00CFDD';
    var $label_color = '#475f7b';
    var $primary_light = '#E2ECFF';
    var $danger_light = '#ffeed9';
    var $gray_light = '#828D99';
    var $sub_label_color = "#596778";
    var $radial_bg = "#e7edf3";


    var xValues = ["Italy", "France", "Spain", "USA", "Argentina"];
    var yValues = [55, 49, 44, 24, 15];
    var barColors = ["red", "green", "blue", "orange", "brown"];

    new Chart("myChart", {
        type: "bar",
        data: {
            labels: xValues,
            datasets: [{
                backgroundColor: barColors,
                data: yValues
            }]
        },
        options: {
            legend: {
                display: false
            },
            title: {
                display: true,
                text: "World Wine Production 2018"
            }
        }
    });

    const ctx = document.getElementById('myChartx');
    const myChartx = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
            datasets: [{
                label: '# of Votes',
                data: [120012021, 19219380, 312798123, 517236721, 27864534, 397324672],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });


    var options = {
        series: [{
            name: "Desktops",
            data: [10, 41, 35, 51, 49, 62, 69, 91, 148]
        }],
        chart: {
            height: 350,
            type: 'line',
            zoom: {
                enabled: false
            }
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            curve: 'straight'
        },
        title: {
            text: 'Product Trends by Month',
            align: 'left'
        },
        grid: {
            row: {
                colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                opacity: 0.5
            },
        },
        xaxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep'],
        }
    };

    var chart = new ApexCharts(document.querySelector("#myChartz"), options);
    chart.render();


    var options = {
        series: [{
            name: 'series1',
            data: [31, 40, 28, 51, 42, 109, 100]
        }, {
            name: 'series2',
            data: [11, 32, 45, 32, 34, 52, 41]
        }],
        chart: {
            height: 350,
            type: 'area'
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            curve: 'smooth'
        },
        xaxis: {
            type: 'datetime',
            categories: ["2018-09-19T00:00:00.000Z", "2018-09-19T01:30:00.000Z", "2018-09-19T02:30:00.000Z", "2018-09-19T03:30:00.000Z", "2018-09-19T04:30:00.000Z", "2018-09-19T05:30:00.000Z", "2018-09-19T06:30:00.000Z"]
        },
        tooltip: {
            x: {
                format: 'dd/MM/yy HH:mm'
            },
        },
    };

    var chart = new ApexCharts(document.querySelector("#chartArea"), options);
    chart.render();


    var options = {
        series: [{
            name: 'Net Profit',
            data: [44, 55, 57, 56, 61, 58, 63, 60, 66, 0, 40, 70]
        }, {
            name: 'Revenue',
            data: [76, 85, 101, 98, 87, 105, 91, 114, 94, 80, 0, 30]
        }, {
            name: 'Free Cash Flow',
            data: [35, 41, 36, 26, 45, 48, 52, 53, 41, 50, 89, 0]
        }],
        chart: {
            type: 'bar',
            height: 350
        },
        plotOptions: {
            bar: {
                horizontal: false,
                columnWidth: '55%',
                endingShape: 'rounded'
            },
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            show: true,
            width: 2,
            colors: ['transparent']
        },
        xaxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Des'],
        },
        yaxis: {
            title: {
                text: '$ (thousands)'
            }
        },
        fill: {
            opacity: 1
        },
        tooltip: {
            y: {
                formatter: function(val) {
                    return "$ " + val + " thousands"
                }
            }
        }
    };

    var chart = new ApexCharts(document.querySelector("#myColumnChart"), options);
    chart.render();



    var options = {
        series: [100],
        chart: {
            height: 350,
            type: 'radialBar',
            offsetY: -10
        },
        plotOptions: {
            radialBar: {
                startAngle: -135,
                endAngle: 135,
                dataLabels: {
                    name: {
                        fontSize: '16px',
                        color: undefined,
                        offsetY: 120
                    },
                    value: {
                        offsetY: 76,
                        fontSize: '22px',
                        color: undefined,
                        formatter: function(val) {
                            return val + "%";
                        }
                    }
                }
            }
        },
        // fill: {
        //     type: 'gradient',
        //     gradient: {
        //         shade: 'dark',
        //         shadeIntensity: 0.15,
        //         inverseColors: false,
        //         opacityFrom: 1,
        //         opacityTo: 1,
        //         stops: [0, 50, 65, 91]
        //     },
        // },
        fill: {
            type: 'gradient',
            gradient: {
                shade: 'dark',
                type: 'horizontal',
                shadeIntensity: 0.5,
                gradientToColors: [$success],
                inverseColors: false,
                opacityFrom: 1,
                opacityTo: 1,
                stops: [0, 100]
            },
        },
        stroke: {
            dashArray: 4
        },
        labels: ['Median Ratio'],
    };

    var chart = new ApexCharts(document.querySelector("#strokedChart"), options);
    chart.render();



    // Primary Line Chart
    // -----------------------------
    var primaryLineChartOption = {
        chart: {
            height: 40,
            // width: 180,
            type: 'line',
            toolbar: {
                show: false
            },
            sparkline: {
                enabled: true,
            },
        },
        grid: {
            show: false,
            padding: {
                bottom: 5,
                top: 5,
                left: 10,
                right: 0
            }
        },
        colors: [$primary],
        dataLabels: {
            enabled: false,
        },
        stroke: {
            width: 3,
            curve: 'smooth'
        },
        series: [{
            data: [50, 100, 0, 60, 20, 30]
        }],
        fill: {
            type: 'gradient',
            gradient: {
                shade: 'dark',
                type: "horizontal",
                gradientToColors: [$primary],
                opacityFrom: 0,
                opacityTo: 0.9,
                stops: [0, 30, 70, 100]
            }
        },
        xaxis: {
            show: false,
            labels: {
                show: false
            },
            axisBorder: {
                show: false
            }
        },
        yaxis: {
            show: false
        },
    }


    //  primary line chart
    var primaryLineChart = new ApexCharts(
        document.querySelector("#primary-line-chart"),
        primaryLineChartOption
    );
    primaryLineChart.render();


    // Stacked Bar Nagetive Chart
    // ----------------------------------
    var barNegativeChartoptions = {
        chart: {
            height: 110,
            stacked: true,
            type: 'bar',
            toolbar: {
                show: false
            },
            sparkline: {
                enabled: true,
            },
        },
        plotOptions: {
            bar: {
                columnWidth: '20%',
                endingShape: 'rounded',
            },
            distributed: true,
        },
        colors: [$primary, $warning],
        series: [{
            name: 'New Clients',
            data: [75, 150, 225, 200, 35, 50, 150, 180, 50, 150, 240, 140, 75, 35, 60, 120]
        }, {
            name: 'Retained Clients',
            data: [-100, -55, -40, -120, -70, -40, -60, -50, -70, -30, -60, -40, -50, -70, -40, -50],
        }],
        grid: {
            show: false,
        },
        legend: {
            show: false,
        },
        dataLabels: {
            enabled: false
        },
        tooltip: {
            x: {
                show: false
            }
        },
    }

    var barNegativeChart = new ApexCharts(
        document.querySelector("#bar-negative-chart"),
        barNegativeChartoptions
    );
</script>