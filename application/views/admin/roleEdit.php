<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">


            <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
            <!-- <form action="" method="post" action="<?= base_url('admin/roleEdit'); ?>"> -->
            <!-- <?= form_open_multipart('admin/roleEdit'); ?> -->
            <form action="" method="post">
                <div class="form-group row">
                    <label for="email" class="col-sm-2 col-form-label">Role</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="role" name="role" value="<?= $role['role']; ?>">
                        <input type="hidden" class="form-control" id="id" name="id" value="<?= $role['id']; ?>">
                        <p><?php echo $role['id'] ?></p>
                    </div>
                </div>

                <div class="form-group row justify-content-end">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Edit Role</button>
                    </div>
                </div>


            </form>


        </div>
    </div>
</div>
<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->