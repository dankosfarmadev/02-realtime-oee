<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">



            <!-- Zero configuration table -->
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title"><?= $title; ?></h4>
                            </div>
                            <div class="card-body card-dashboard">
                                <!-- <p class="card-text">
                                        DataTables has most features enabled by default, so all you need
                                        to do to use it with your own tables is to call the construction
                                        function: $().DataTable();.
                                    </p> -->
                                <!-- <a href="" class="btn btn-primary mb-1" data-toggle="modal" data-target="#newRoleModal">Add New Role</a> -->
                                <a href="<?= base_url('admin/workerAdd')  ?>" class="btn btn-primary mb-3">Add New Worker</a>
                                <div class="table-responsive">

                                    <?= form_error('menu', '<div class="alert alert-danger" role="alert">', '</div>'); ?>

                                    <?= $this->session->flashdata('message'); ?>

                                    <table class="table zero-configuration">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>NIK</th>
                                                <th>Active?</th>
                                                <th>Departement</th>
                                                <th>Update Date</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <!-- <tr>
                                                    <td>Tiger Nixon</td>
                                                    <td>System Architect</td>
                                                    <td>Edinburgh</td>
                                                </tr> -->
                                            <?php $i = 1; ?>
                                            <?php foreach ($worker_data as $r) : ?>
                                                <tr>
                                                    <th scope="row"><?= $i; ?></th>
                                                    <td><?= $r['name']; ?></td>
                                                    <td><?= $r['nik']; ?></td>
                                                    <td><?= $r['is_active'] == '1' ? 'Active' : 'Not Active' ?></td>
                                                    <td><?= $r['departement']; ?></td>
                                                    <td><?= date('d-m-Y H:i:s', strtotime($r['update_date'])); ?></td>
                                                    <td>
                                                        <!-- otomatis method get ? -->
                                                        <!-- <a href="<?= base_url('admin/worker/edit/') . $r['id']; ?>" class="badge badge-success">edit</a> -->
                                                        <a href="<?= base_url('admin/workerEdit/') . $r['id']; ?>" class="badge badge-success">edit</a>
                                                        <!-- <a href="" class="badge badge-danger">delete</a> -->
                                                    </td>
                                                </tr>
                                                <?php $i++; ?>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->


        </div>
    </div>
</div>
<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->