<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <!-- <div class="row">
                <a href="<?= base_url('worker/receive')  ?>" class="btn btn-icon btn-outline-primary ml-1 mb-2 mr-1"><i class="bx bx-arrow-back"></i></a>
                <h1 class="h3 mb-2 text-gray-800"><?= $title; ?></h1>
            </div> -->
            <!-- Columns section start -->
            <section id="columns">
                <div class="row">
                    <div class="col-12 mt-3 mb-1">
                        <h4 class="text-uppercase"><?= $title; ?></h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 mt-1">
                        <div class="card-columns">
                            <div class="card">
                                <img class="card-img-top img-fluid" src="<?= base_url('assets/'); ?>app-assets/images/slider/09.png" alt="Card image cap">
                                <div class="card-header">
                                    <h4 class="card-title">Setup application</h4>
                                </div>
                                <div class="card-body">
                                    <p class="card-text">
                                        1. Change Database & Config
                                        <br>
                                        2. Change Role
                                        <br>
                                        3. Change CC Email (Globa Var)
                                        <br>
                                        4. Change Version (Global Var)
                                    </p>
                                    <a href="javascript:void(0);" class="btn btn-outline-primary">Go somewhere</a>
                                </div>
                            </div>
                            <!-- <div class="card bg-primary text-center">
                                <div class="card-body">
                                    <img src="<?= base_url('assets/'); ?>app-assets/images/elements/iphone-x.png" alt="element 05" width="150" class="mb-1 img-fluid">
                                    <h4 class="card-title text-white">iPhone 11</h4>
                                    <p class="card-text text-white">945 items</p>
                                </div>
                            </div>
                            <div class="card position-static text-white bg-danger bg-lighten-1 text-center">
                                <div class="card-body">
                                    <img src="<?= base_url('assets/'); ?>app-assets/images/elements/ipad-pro.png" alt="element 02" width="120" class="mb-1 img-fluid">
                                    <h4 class="card-title text-white">iPad Mini</h4>
                                    <p class="card-text">456 items</p>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Bottom Image Cap</h4>
                                </div>
                                <div class="card-body">
                                    <p class="card-text">
                                        Jelly-o sesame snaps cheesecake topping. Cupcake fruitcake macaroon
                                        donut pastry gummies tiramisu chocolate bar muffin. Dessert bonbon caramels brownie
                                        chocolate bar chocolate tart dragée.
                                    </p>
                                    <p class="card-text">
                                        Cupcake fruitcake macaroon donut pastry gummies tiramisu chocolate bar
                                        muffin.
                                    </p>
                                    <small class="text-muted">Last updated 3 mins ago</small>
                                </div>
                                <img class="card-img-bottom img-fluid" src="<?= base_url('assets/'); ?>app-assets/images/slider/04.jpg" alt="Card image cap">
                            </div>
                            <div class="card text-white">
                                <img class="card-img img-fluid position-sticky" src="<?= base_url('assets/'); ?>app-assets/images/slider/03.jpg" alt="Card image">
                                <div class="card-img-overlay overlay-warning">
                                    <h4 class="card-title white mb-50">Overlay Card</h4>
                                    <p class="card-text text-ellipsis">
                                        Sugar plum tiramisu sweet. Cake jelly marshmallow cotton candy chupa
                                        chups carrot cake topping chupa chups.
                                    </p>
                                    <small>Last updated 3 mins ago</small>
                                </div>
                            </div>
                            <div class="card border-info text-center bg-transparent">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12 mb-50 d-flex justify-content-center">
                                            <img src="<?= base_url('assets/'); ?>app-assets/images/elements/macbook-pro.png" alt="element 04" width="150" class="float-left mt-1 img-fluid">
                                        </div>
                                        <div class="col-md-6 col-sm-12 d-flex justify-content-center flex-column">
                                            <h4>
                                                <span class="badge badge-light-info">New Arrival</span>
                                            </h4>
                                            <p class="card-text">Mac Book.</p>
                                        </div>
                                    </div>
                                    <button class="btn btn-info mt-50">Buy Now</button>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </section>

        </div>
    </div>
</div>