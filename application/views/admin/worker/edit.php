<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <div class="row">
                <a href="<?= base_url('admin/worker')  ?>" class="btn btn-icon btn-outline-primary ml-1 mb-2 mr-1"><i class="bx bx-arrow-back"></i></a>
                <h1 class="h3 mb-2 text-gray-800"><?= $title; ?></h1>
            </div>
            <section id="basic-horizontal-layouts">
                <div class="row match-height">
                    <div class="col-md-12 col-12">
                        <div class="card">
                            <div class="card-header">
                                <!-- <h4 class="card-title"><?= $title; ?></h4> -->
                            </div>
                            <div class="card-body">

                                <form action="<?= base_url('admin/workerEdit/' . $worker_data_specific[0]['id']); ?>" method="post">
                                    <div class="form-body">
                                        <!-- <?php if (validation_errors()) : ?>
                                            <div class="alert alert-danger" role="alert">
                                                <?= validation_errors(); ?>
                                            </div>
                                        <?php endif; ?> -->
                                        <?= $this->session->flashdata('message'); ?>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Nama</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" id="name" class="form-control" name="name" placeholder="Name" value="<?= $worker_data_specific[0]['name']; ?>">
                                                <?= form_error('name', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <div class="col-md-4">
                                                <label>NIK</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" id="nik" class="form-control" name="nik" placeholder="NIK" value="<?= $worker_data_specific[0]['nik']; ?>">
                                                <?= form_error('nik', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Email</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" id="email" class="form-control" name="email" placeholder="Email" value="<?= $worker_data_specific[0]['email']; ?>">
                                                <?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Departement</label>
                                                <?= form_error('Departement', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <select name="departement_id" id="departement_id" class="form-control">
                                                    <option value="">Select Departement</option>
                                                    <?php foreach ($list_departemen as $m) : ?>
                                                        <option value="<?= $m['id']; ?>" <?= $m['id'] == $worker_data_specific[0]['departement_id'] ? 'selected' : ''; ?>><?= $m['departement']; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                                <?= form_error('icon', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Role</label>
                                                <?= form_error('Departement', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <select name="role_id" id="role_id" class="form-control">
                                                    <option value="">Select Role</option>
                                                    <?php foreach ($list_role as $m) : ?>
                                                        <option value="<?= $m['id']; ?>" <?= $m['id'] == $worker_data_specific[0]['role_id'] ? 'selected' : ''; ?>><?= $m['role']; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                                <?= form_error('icon', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Active</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <!-- <label for="active" class="col-sm-2 col-form-label custom-control-label">Active</label> -->
                                                <div class="custom-control custom-switch custom-switch-glow custom-control-inline mb-1">
                                                    <!-- <input type="checkbox" class="custom-control-input" id="active" name="active[]"> -->
                                                    <input type="checkbox" class="custom-control-input" id="active" name="active[]" <?= $worker_data_specific[0]['is_active'] == 1 ? 'checked' : '' ?>>
                                                    <label class="custom-control-label" for="active">
                                                    </label>
                                                </div>
                                            </div>
                                            <!-- <div class="col-md-4">
                                                <label>Password & Confirm Password</label>
                                            </div>
                                            <div class="form-group row col-md-8">
                                                <div class="col-sm-6 mb-3 mb-sm-0">
                                                    <input type="password" class="form-control form-control-user" id="password1" name="password1" placeholder="Password">
                                                    <?= form_error('password1', '<small class="text-danger pl-3">', '</small>'); ?>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="password" class="form-control form-control-user" id="password2" name="password2" placeholder="Repeat Password">
                                                </div>
                                            </div> -->
                                            <input type="hidden" class="form-control" id="id" name="id" value="<?= $worker_data_specific[0]['id']; ?>">
                                            <p><?php echo $worker_data_specific[0]['id'] ?></p>

                                            <div class="col-sm-12 d-flex justify-content-end">
                                                <a href="<?= base_url('admin/workerResetPass/' . $worker_data_specific[0]['id']); ?>" class="btn btn-danger mr-1">Reset Password</a>
                                                <button type="reset" class="btn btn-light-secondary mr-1">Reset</button>
                                                <button type="submit" class="btn btn-primary ">Edit Worker</button>

                                            </div>
                                            <!-- <div class="col-sm-12 d-flex justify-content-start">

                                                <a href="<?= base_url('admin')  ?>" class="btn btn-danger ml-1">Reset Password</a>
                                            </div> -->
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Basic Horizontal form layout section end -->
        </div>
    </div>
</div>