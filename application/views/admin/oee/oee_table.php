<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">


            <!-- <script type='text/javascript' src='http://10.100.4.100/javascripts/api/viz_v1.js'></script>
            <div class='tableauPlaceholder' style='width: 1536px; height: 850px;'><object class='tableauViz' width='1536' height='850' style='display:none;'>
                    <param name='host_url' value='http%3A%2F%2F10.100.4.100%2F' />
                    <param name='embed_code_version' value='3' />
                    <param name='site_root' value='&#47;t&#47;Manufacture' />
                    <param name='name' value='DANKOSFARMA-OEE-Realtime-PROD&#47;DANKOS1OEEReport' />
                    <param name='tabs' value='yes' />
                    <param name='toolbar' value='yes' />
                    <param name='showAppBanner' value='false' />
                </object>
            </div> -->

            <!-- http://10.100.4.100/t/Manufacture/views/DANKOSFARMA-OEE-Realtime-PROD/DANKOS1OEEReport?:showAppBanner=false&:display_count=n&:showVizHome=n&:origin=viz_share_link -->
            <!-- <iframe width="500px" height="500px" src="http://10.100.4.100/t/Manufacture/views/DANKOSFARMA-OEE-Realtime-PROD/DANKOS1OEEReport?:showAppBanner=false&:display_count=n&:showVizHome=n&:origin=viz_share_link">
            </iframe> -->

            <!-- <iframe width="500px" height="500px" src="http://10.100.4.100/t/Manufacture/views/DANKOSFARMA-OEE-Realtime-PROD/DANKOS1OEEReport?:showAppBanner=false&:display_count=n&:showVizHome=n&:origin=viz_share_link">
            </iframe> -->

            <!-- <iframe src="http://10.100.4.100/t/Manufacture/views/DANKOSFARMA-OEE-Realtime-PROD/DANKOS1OEEReport?:showVizHome=no&:embed=true" width="645" height="955"></iframe> -->



            <!-- Zero configuration table -->
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title"><?= $title; ?></h4>
                            </div>
                            <div class="card-body card-dashboard">
                                <!-- <p class="card-text">
                                        DataTables has most features enabled by default, so all you need
                                        to do to use it with your own tables is to call the construction
                                        function: $().DataTable();.
                                    </p> -->
                                <?= $this->session->flashdata('message'); ?>

                                <form method="post" action="<?php echo base_url('admin/spreadsheet_import'); ?>" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <input type="file" name="upload_file" class="form-control" placeholder="Enter Name" id="upload_file" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" name="submit" class="btn btn-primary">
                                    </div>
                                </form>




                                <!-- <a href="" class="btn btn-primary mb-1" data-toggle="modal" data-target="#newRoleModal">Add New Role</a> -->
                                <div class="table-responsive">

                                    <!-- <?= form_error('menu', '<div class="alert alert-danger" role="alert">', '</div>'); ?> -->

                                    <!-- <?= $this->session->flashdata('message'); ?> -->

                                    <table id="wkwk_table" class="table zero-configuration">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Jenis Mesin</th>
                                                <th>Shift</th>
                                                <th>Tanggal</th>
                                                <th>Type</th>
                                                <th>Description</th>
                                                <th>Value</th>
                                                <th>Operator</th>
                                                <th>Create Date</th>
                                                <th>Line</th>
                                                <th><input type="checkbox" id="select_all" value="" /></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i = 1; ?>
                                            <?php foreach ($role as $r) : ?>
                                                <tr>
                                                    <th scope="row"><?= $i; ?></th>
                                                    <td><?= $r['jenis_mesin']; ?></td>
                                                    <td><?= $r['shift']; ?></td>
                                                    <td><?= $r['tanggal']; ?></td>
                                                    <td><?= $r['type']; ?></td>
                                                    <td><?= $r['description']; ?></td>
                                                    <td><?= $r['value']; ?></td>
                                                    <td><?= $r['operator']; ?></td>
                                                    <td><?= $r['create_date']; ?></td>
                                                    <td><?= $r['line']; ?></td>
                                                    <td><input type="checkbox" name="checked_id[]" class="checkbox" value="<?php echo $r['id']; ?>" /></td>
                                                </tr>
                                                <?php $i++; ?>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>



                            </div>
                        </div>
                    </div>
                </div>
            </section>


        </div>
    </div>
</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        $('#wkwk_table').dataTable({
            "aLengthMenu": [
                [100, 1000, 2500, -1],
                [100, 1000, 2500, "All (max: 5000)"]
            ],
            "iDisplayLength": 1000
        });
    });
</script>