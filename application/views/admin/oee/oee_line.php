<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">





            <!-- Zero configuration table -->
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title text-center"><?= $title; ?></h4>
                            </div>
                            <div class="card-body card-dashboard">
                                <!-- <p class="card-text">
                                        DataTables has most features enabled by default, so all you need
                                        to do to use it with your own tables is to call the construction
                                        function: $().DataTable();.
                                    </p> -->
                                <?= $this->session->flashdata('message'); ?>

                                <a href="<?= base_url('oee/table/' . $title) ?>" class="btn btn-secondary mb-3 btn-block">Lihat table</a>





                                <h4 class="card-title text-center">Upload OEE - <?= $title; ?></h4>
                                <form method="post" action="<?php echo base_url('oee/spreadsheet_import'); ?>" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <input type="file" name="upload_file" class="form-control" placeholder="Enter Name" id="upload_file" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" name="submit" class="btn btn-primary text-center btn-block">
                                    </div>
                                </form>

                                <a href="<?= base_url('oee/format_oee') ?>" class="btn btn-light btn-block">Download format OEE</a>
                                <!-- <a href="application/views/file/OEE_Format.xlsx" class="btn btn-secondary btn-block" download="">Download format OEE v2</a> -->


                                <!-- <a href="" class="btn btn-primary mb-1" data-toggle="modal" data-target="#newRoleModal">Add New Role</a> -->
                                <!-- <div class="table-responsive">
                                    <table class="table zero-configuration">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Jenis Mesin</th>
                                                <th>Shift</th>
                                                <th>Tanggal</th>
                                                <th>Type</th>
                                                <th>Description</th>
                                                <th>Value</th>
                                                <th>Operator</th>
                                                <th>Create Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i = 1; ?>
                                            <?php foreach ($role as $r) : ?>
                                                <tr>
                                                    <th scope="row"><?= $i; ?></th>
                                                    <td><?= $r['jenis_mesin']; ?></td>
                                                    <td><?= $r['shift']; ?></td>
                                                    <td><?= $r['tanggal']; ?></td>
                                                    <td><?= $r['type']; ?></td>
                                                    <td><?= $r['description']; ?></td>
                                                    <td><?= $r['value']; ?></td>
                                                    <td><?= $r['operator']; ?></td>
                                                    <td><?= $r['create_date']; ?></td>
                                                </tr>
                                                <?php $i++; ?>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </section>


        </div>
    </div>
</div>
</div>