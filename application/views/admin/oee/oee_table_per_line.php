<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">


            <!-- <form method="post" action="" enctype="multipart/form-data"> -->
            <form name="bulk_action_form" action="<?= base_url('oee/bulk_delete'); ?>" method="post" onSubmit="return delete_confirm();">





                <!-- Zero configuration table -->
                <section id="basic-datatable">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title text-center"><?= $title; ?></h4>
                                    <!-- <input type="submit" name="submit" class="btn btn-danger" value="Delete"> -->
                                    <input type="submit" class="btn btn-danger" name="bulk_delete_submit" value="DELETE" />
                                </div>
                                <div class="card-body card-dashboard">
                                    <?= $this->session->flashdata('message'); ?>
                                    <div class="table-responsive">
                                        <table id="wkwk_table" class="table zero-configuration">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Jenis Mesin</th>
                                                    <th>Shift</th>
                                                    <th>Tanggal</th>
                                                    <th>Type</th>
                                                    <th>Description</th>
                                                    <th>Value</th>
                                                    <th>Operator</th>
                                                    <th>Create Date</th>
                                                    <th><input type="checkbox" id="select_all" value="" /></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $i = 1; ?>
                                                <?php foreach ($role as $r) : ?>
                                                    <tr>
                                                        <th scope="row"><?= $i; ?></th>
                                                        <td><?= $r['jenis_mesin']; ?></td>
                                                        <td><?= $r['shift']; ?></td>
                                                        <td><?= $r['tanggal']; ?></td>
                                                        <td><?= $r['type']; ?></td>
                                                        <td><?= $r['description']; ?></td>
                                                        <td><?= $r['value']; ?></td>
                                                        <td><?= $r['operator']; ?></td>
                                                        <td><?= $r['create_date']; ?></td>
                                                        <td><input type="checkbox" name="checked_id[]" class="checkbox" value="<?php echo $r['id']; ?>" /></td>
                                                    </tr>
                                                    <?php $i++; ?>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </form>



        </div>
    </div>
</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    function delete_confirm() {
        if ($('.checkbox:checked').length > 0) {
            var result = confirm("Are you sure to delete selected?");
            if (result) {
                return true;
            } else {
                return false;
            }
        } else {
            alert('Select at least 1 record to delete.');
            return false;
        }
    }

    $(document).ready(function() {
        $('#select_all').on('click', function() {
            if (this.checked) {
                $('.checkbox').each(function() {
                    this.checked = true;
                });
            } else {
                $('.checkbox').each(function() {
                    this.checked = false;
                });
            }
        });

        $('.checkbox').on('click', function() {
            if ($('.checkbox:checked').length == $('.checkbox').length) {
                $('#select_all').prop('checked', true);
            } else {
                $('#select_all').prop('checked', false);
            }
        });
    });

    $(document).ready(function() {
        $('#wkwk_table').dataTable({
            "aLengthMenu": [
                [100, 1000, 2500, -1],
                [100, 1000, 2500, "All (max: 5000)"]
            ],
            "iDisplayLength": 1000
        });
    });
</script>