<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">



            <!-- Zero configuration table -->
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title"><?= $title; ?></h4>
                            </div>
                            <div class="card-body card-dashboard">
                                <?php if (($this->session->userdata('role_id') == 5) || ($this->session->userdata('role_id') == 4) || ($this->session->userdata('role_id') == 2) || ($this->session->userdata('role_id') == 1)) {
                                    null;
                                } else { ?>
                                    <a href="<?= base_url('worker/deliveryAdd_export')  ?>" class="btn btn-primary mb-2">Add Delivery Export</a>

                                <?php } ?>


                                <div class="btn-group">
                                    <div class="dropdown">
                                        <button class="btn btn-primary dropdown-toggle mb-2" type="button" id="dropdownMenuButtonIcon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="bx bx-error-circle mr-50"></i> Export File
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButtonIcon">
                                            <a class="dropdown-item" href="<?= base_url('feature/deliveryExportXlsx'); ?>"><i class="bx bx-file mr-50"></i> xlsx </a>
                                        </div>
                                    </div>
                                </div>

                                <!-- <a href="<?= base_url('worker/deliveryAdd_export')  ?>" class="btn btn-primary mb-3">Add Delivery</a> -->
                                <div class="table-responsive">

                                    <?= form_error('menu', '<div class="alert alert-danger" role="alert">', '</div>'); ?>

                                    <?= $this->session->flashdata('message'); ?>

                                    <table class="table zero-configuration">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Tanggal</th>
                                                <th>Dist/Cust</th>
                                                <th>Ekspedisi</th>
                                                <th>No polisi</th>
                                                <th>Jam awal</th>
                                                <th>Jam setelah</th>
                                                <th>Create by</th>
                                                <th>Status</th>
                                                <!-- <th>Update Date</th> -->
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i = 1; ?>
                                            <?php foreach ($receive as $r) : ?>
                                                <tr>
                                                    <td scope="row">
                                                        <p class="font-small-3"><?= $i; ?></p>
                                                    </td>
                                                    <td>
                                                        <p class="font-small-3"><?= date('d-m-Y', strtotime($r['tanggal'])); ?></p>
                                                    </td>
                                                    <td>
                                                        <p class="font-small-3"><?= $r['distributor_customer']; ?></p>
                                                    </td>
                                                    <td>
                                                        <p class="font-small-3"><?= $r['ekspedisi'] ?></p>
                                                    </td>
                                                    <td>
                                                        <p class="font-small-3"><?= $r['no_polisi']; ?></p>
                                                    </td>
                                                    <td>
                                                        <p class="font-small-3"><?= $r['jam_awal']; ?></p>
                                                    </td>
                                                    <td>
                                                        <p class="font-small-3"><?= $r['jam_setelah']; ?></p>
                                                    </td>
                                                    <td>
                                                        <p class="font-small-3"><?= $r['name']; ?></p>
                                                    </td>
                                                    <td>
                                                        <p class="font-small-3"><?= $r['status']; ?></p>
                                                    </td>
                                                    <!-- <td><?= date('d-m-Y H:i:s', strtotime($r['update_date'])); ?></td> -->
                                                    <td>
                                                        <a href="<?= base_url('worker/deliveryDetail_export/') . $r['id']; ?>" class="badge badge-primary">Detail</a>
                                                    </td>
                                                </tr>
                                                <?php $i++; ?>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->


        </div>
    </div>
</div>
<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->