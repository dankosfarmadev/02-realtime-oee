<!-- Baru -->
<?php if (($this->session->userdata('role_id') == 1)) {
?>


    <!-- Newest -->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <div class="row">
                    <a href="<?= base_url('worker/delivery')  ?>" class="btn btn-icon btn-outline-primary ml-1 mb-2 mr-1"><i class="bx bx-arrow-back"></i></a>
                    <h1 class="h3 mb-2 text-gray-800"><?= $title; ?></h1>
                </div>
                <?php if ($receive_data['status_form_rd_id'] != 6) {
                    null;
                } else { ?>
                    <button type="button" class="btn btn-success glow mr-1 mb-1"><i class="bx bxs-download"></i>
                        <a href="<?= base_url('test/export/' . $receive_data['id']) ?>" target="_blank">
                            <span class="align-middle ml-25" style="color: white;">Export PDF</span>
                        </a>
                    </button>
                <?php } ?>
                <section id="basic-horizontal-layouts">
                    <div class="row match-height">
                        <div class="col-md-12 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <!-- <h4 class="card-title"><?= $title; ?></h4> -->
                                </div>
                                <div class="card-body">
                                    <!-- <form class="form form-horizontal" action="<?= base_url('worker/deliveryAdd'); ?>" method="post"> -->
                                    <?= form_open_multipart('coordinator/approvalDocDetailDone2/' . $receive_data['id']); ?>

                                    <div class="form-body">
                                        <!-- <?php if (validation_errors()) : ?>
                                            <div class="alert alert-danger" role="alert">
                                                <?= validation_errors(); ?>
                                            </div>
                                        <?php endif; ?> -->
                                        <?= $this->session->flashdata('message'); ?>
                                        <div class="row">
                                            <?php if ($receive_data['status_form_rd_id'] == 2) { ?>
                                                <div class="col-md-12">
                                                    <div class="alert alert-warning" role="alert">Return oleh <?= $receive_data['names'] ?>: <?= $receive_data['alasan_return'] ?> </div>
                                                </div>
                                            <?php } elseif ($receive_data['status_form_rd_id'] == 3) {
                                            ?>
                                                <div class="col-md-12">
                                                    <div class="alert alert-danger" role="alert">Reject oleh <?= $receive_data['names'] ?>: <?= $receive_data['alasan_reject'] ?> </div>
                                                </div>
                                            <?php
                                            } ?>
                                            <div class="col-md-4">
                                                <label>Tanggal</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <fieldset class="form-group position-relative has-icon-left">
                                                    <!-- <input type="date" name="dataPicker" class="form-control pickadate" placeholder="Select Date" value="<?= set_value('dataPicker'); ?>"> -->
                                                    <input type="date" name="dataPicker" class="form-control" placeholder="Select Date" value="<?= $receive_data['tanggal']; ?>">
                                                    <div class="form-control-position">
                                                        <i class='bx bx-calendar'></i>
                                                    </div>
                                                </fieldset>
                                                <?= form_error('dataPicker', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Distributor/Customer</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" id="dist_cust" class="form-control" name="dist_cust" placeholder="Distributor or customer" value="<?= $receive_data['distributor_customer']; ?>">
                                                <?= form_error('dist_cust', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Ekspedisi</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" id="ekspedisi" class="form-control" name="ekspedisi" placeholder="Ekspedisi" value="<?= $receive_data['ekspedisi']; ?>">
                                                <?= form_error('ekspedisi', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <div class="col-md-4">
                                                <label>No Polisi</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" id="no_polisi" class="form-control" name="no_polisi" placeholder="No Plat Polisi" value="<?= $receive_data['no_polisi']; ?>">
                                                <?= form_error('no_polisi', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Suhu awal muat (Celcius)</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" id="suhu_awal" class="form-control" name="suhu_awal" placeholder="Suhu awal" value="<?= $receive_data['suhu_awal']; ?>">
                                                <?= form_error('suhu_awal', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <!-- <div class="col-md-4">
                                                <label>Suhu setelah muat (Celcius)</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" id="suhu_setelah" class="form-control" name="suhu_setelah" placeholder="Suhu setelah" value="<?= $receive_data['suhu_setelah']; ?>">
                                                <?= form_error('suhu_setelah', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div> -->
                                            <div class="col-md-4">
                                                <label>Jam awal muat</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="time" id="jam_awal" class="form-control" name="jam_awal" placeholder="Jam awal" value="<?= $receive_data['jam_awal']; ?>">
                                                <?= form_error('jam_awal', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <!-- <div class="col-md-4">
                                                <label>Jam setelah muat</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="time" id="jam_setelah" class="form-control" name="jam_setelah" placeholder="Jam setelah" value="<?= $receive_data['jam_setelah']; ?>">
                                                <?= form_error('jam_setelah', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div> -->






                                            <div class="col-md-12 mt-2 mb-2">
                                                <h4 class="card-title">Kondisi kendaraan <small class="text-muted">*</small></h4>
                                            </div>



                                            <div class="col-md-4">
                                                <label>Kebersihan</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <ul class="list-unstyled mb-0">
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="kebersihan" id="kebersihan1" value="Bersih" <?= strtolower($receive_data['kebersihan_kendaraan']) == 'bersih' ? 'checked' : ''; ?>>
                                                                <label for="kebersihan1">Bersih</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="kebersihan" id="kebersihan2" value="Tidak bersih" <?= strtolower($receive_data['kebersihan_kendaraan']) == 'tidak bersih' ? 'checked' : ''; ?>>
                                                                <label for="kebersihan2">Tidak bersih</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                </ul>
                                            </div>
                                            <?= form_error('kebersihan', '<small class="text-danger pl-3">', '</small>'); ?>


                                            <div class="col-md-4">
                                                <label>Bunyi</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <ul class="list-unstyled mb-0">
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="bunyi" id="bunyi1" value="Berisik" <?= strtolower($receive_data['bunyi_mesin_kendaraan']) == 'berisik' ? 'checked' : ''; ?>>
                                                                <label for="bunyi1">Berisik</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="bunyi" id="bunyi2" value="Tidak berisik" <?= strtolower($receive_data['bunyi_mesin_kendaraan']) == 'tidak berisik' ? 'checked' : ''; ?>>
                                                                <label for="bunyi2">Tidak berisik</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                </ul>
                                            </div>
                                            <?= form_error('bunyi', '<small class="text-danger pl-3">', '</small>'); ?>


                                            <div class="col-md-4">
                                                <label>Asap Kendaraan</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <ul class="list-unstyled mb-0">
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="asap" id="asap1" value="Hitam" <?= strtolower($receive_data['asap_kendaraan']) == 'hitam' ? 'checked' : ''; ?>>
                                                                <label for="asap1">Hitam</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="asap" id="asap2" value="Tidak hitam" <?= strtolower($receive_data['asap_kendaraan']) == 'tidak hitam' ? 'checked' : ''; ?>>
                                                                <label for="asap2">Tidak hitam</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                </ul>
                                            </div>
                                            <?= form_error('asap', '<small class="text-danger pl-3">', '</small>'); ?>


                                            <div class="col-md-4">
                                                <label>Bau</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <ul class="list-unstyled mb-0">
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="bau" id="bau1" value="Bau" <?= strtolower($receive_data['bau_kendaraan']) == 'bau' ? 'checked' : ''; ?>>
                                                                <label for="bau1">Bau</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="bau" id="bau2" value="Tidak bau" <?= strtolower($receive_data['bau_kendaraan']) == 'tidak bau' ? 'checked' : ''; ?>>
                                                                <label for="bau2">Tidak bau</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                </ul>
                                            </div>
                                            <?= form_error('bau', '<small class="text-danger pl-3">', '</small>'); ?>


                                            <div class="col-md-4">
                                                <label>Kebocoran</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <ul class="list-unstyled mb-0">
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="kebocoran" id="kebocoran1" value="Bocor" <?= strtolower($receive_data['kebocoran_kendaraan']) == 'bocor' ? 'checked' : ''; ?>>
                                                                <label for="kebocoran1">Bocor</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="kebocoran" id="kebocoran2" value="Tidak bocor" <?= strtolower($receive_data['kebocoran_kendaraan']) == 'tidak bocor' ? 'checked' : ''; ?>>
                                                                <label for="kebocoran2">Tidak bocor</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                </ul>
                                            </div>
                                            <?= form_error('kebocoran', '<small class="text-danger pl-3">', '</small>'); ?>

                                            <div class="col-md-4">
                                                <label>Uji Emisi/KIR</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <ul class="list-unstyled mb-0">
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="emisi" id="emisi1" value="Ya" <?= strtolower($receive_data['emisi_kendaraan']) == 'ya' ? 'checked' : ''; ?>>
                                                                <label for="emisi1">Ya</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="emisi" id="emisi2" value="Tidak" <?= strtolower($receive_data['emisi_kendaraan']) == 'tidak' ? 'checked' : ''; ?>>
                                                                <label for="emisi2">Tidak</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                </ul>
                                            </div>
                                            <?= form_error('emisi', '<small class="text-danger pl-3">', '</small>'); ?>


                                            <div class="col-md-4">
                                                <label>Tetesan Oli</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <ul class="list-unstyled mb-0">
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="oli" id="oli1" value="Ya" <?= strtolower($receive_data['oli_kendaraan']) == 'ya' ? 'checked' : ''; ?>>
                                                                <label for="oli1">Ya</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="oli" id="oli2" value="Tidak" <?= strtolower($receive_data['oli_kendaraan']) == 'tidak' ? 'checked' : ''; ?>>
                                                                <label for="oli2">Tidak</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                </ul>
                                            </div>
                                            <?= form_error('oli', '<small class="text-danger pl-3">', '</small>'); ?>




                                            <div class="col-md-12 mt-2 mb-2">
                                                <h4 class="card-title">Pengirim <small class="text-muted">*</small></h4>
                                            </div>


                                            <div class="col-md-4">
                                                <label>Nama Ekspedisi (Nama Pengirim)</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" id="nama_ekspedisi" class="form-control" name="nama_ekspedisi" placeholder="Nama" value="<?= $receive_data['nama_ekspedisi']; ?>" disabled>
                                                <?= form_error('nama_ekspedisi', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Foto Pengirim</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <div class="row">
                                                    <div class="col-sm-9">
                                                        <div class="custom-file">
                                                            <input type="file" class="custom-file-input" id="image" name="image" value="<?= $receive_data['foto_ekspedisi']; ?>" disabled>
                                                            <label class="custom-file-label" for="image">Choose file</label>
                                                            <?= form_error('image', '<small class="text-danger pl-3">', '</small>'); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                            </div>
                                            <div class="col-sm-3">
                                                <img src="<?= base_url('assets/img/foto_ekspedisi/') . $receive_data['foto_ekspedisi']; ?>" class="img-thumbnail">
                                            </div>

                                            <hr>


                                            <hr>
                                            <div class="col-md-12 mt-2 mb-2">
                                                <h4 class="card-title">Catatan <small class="text-muted"></small></h4>
                                                <small class="text-danger">(catat pada kolom ini jika terdapat produk kondisi basah/rusak dalam bentuk apapun atau terjadi
                                                    penyimpangan):*</small>
                                            </div>


                                            <div class="col-md-4">
                                                <label>Catatan</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" id="catatan" class="form-control" name="catatan" placeholder="Catatan" value="<?= $receive_data['catatan']; ?>">
                                                <?= form_error('catatan', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>

                                            <div class="col-md-4">
                                                <label>No seal/segel</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" id="segel" class="form-control" name="segel" placeholder="No seal/segel" value="<?= $receive_data['segel']; ?>">
                                                <?= form_error('segel', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>


                                            <hr>

                                            <div class="col-md-12 mt-2 mb-2">
                                                <h4 class="card-title">Data <small class="text-muted">*</small></h4>
                                            </div>


                                            <!-- Step 2 -->
                                            <h6>
                                                <i class="step-icon"></i>
                                            </h6>
                                            <!-- Step 2 end-->
                                            <!-- body content of step 2 -->
                                            <div class="col-md-12" style="margin: 10px;">
                                                <div class="box box-solid">
                                                    <div class="box-body">
                                                        <br>
                                                        <table class="table table-bordered" id="tableLoop">
                                                            <thead>
                                                                <tr>
                                                                    <th class="text-center col-md-1 col-sm-1">#</th>
                                                                    <th>Data delivery</th>
                                                                    <!-- <th class="col-md-2"><button class="btn btn-success btn-block" id="BarisBaru"><i class="fa fa-plus"></i> Baris Baru</button></th> -->
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php $i = 1; ?>
                                                                <?php foreach ($rd_data as $r) : ?>
                                                                    <tr>
                                                                        <td class="text-center"> <?= $i ?>
                                                                        <td>
                                                                            <small><cite title="Source Title">Batch number</cite></small>
                                                                            <input type="text" name="bn[]" class="form-control bn" placeholder="Batch number" required="" value="<?= $r['batch_number']; ?>">
                                                                            <br />
                                                                            <small><cite title="Source Title">LPN number</cite></small>
                                                                            <input type="text" name="lpn[]" class="form-control lpn" placeholder="LPN number" required="" value="<?= $r['lpn_number']; ?>">
                                                                            <br />
                                                                            <small><cite title="Source Title">Quantity MB</cite></small>
                                                                            <input type="text" name="qty_mb[]" class="form-control qty_mb" placeholder="Quantity MB" required="" value="<?= $r['qty_mb']; ?>">
                                                                            <br />
                                                                            <small><cite title="Source Title">Quantity per MB</cite></small>
                                                                            <input type="text" name="qty_per_mb[]" class="form-control qty_per_mb" placeholder="Quantity per MB" required="" value="<?= $r['qty_per_mb']; ?>">
                                                                            <br />
                                                                            <small><cite title="Source Title">Quantity Pick</cite></small>
                                                                            <input type="text" name="qty_pick[]" class="form-control qty_pick" placeholder="Quantity Pick" required="" value="<?= $r['qty_pick']; ?>">
                                                                            <br />
                                                                            <small><cite title="Source Title">Product Checklist Out</cite></small>
                                                                            <select name="pco[]" id="pco" class="form-control">
                                                                                <!-- <option value="Y">OK</option>
                                                                                <option value="N">NOK</option> -->
                                                                                <option value="Y" <?= strtolower($r['product_checklist_out']) == 'y' ? 'selected' : ''; ?>>OK</option>
                                                                                <option value="N" <?= strtolower($r['product_checklist_out']) == 'n' ? 'selected' : ''; ?>>NOK</option>
                                                                            </select>
                                                                            <br />
                                                                            <small><cite title="Source Title">Quantity EPM Pallet</cite></small>
                                                                            <input type="text" name="qep[]" class="form-control qep" placeholder="Quantity EPM Pallet (number)" required="" value="<?= $r['qty_epm_pallet']; ?>">
                                                                            <br />
                                                                            <input type="hidden" id="data_rd_id" class="form-control" name="data_rd_id[]" placeholder="id" value="<?= $r['id']; ?>">
                                                                        </td>
                                                                        <!-- <td class="text-center">
                                                                            <a class="btn btn-sm btn-danger" data-toggle="tooltip" title="Hapus Baris" id="HapusBaris"><i class="bx bxs-x-square"></i></a>
                                                                        </td> -->

                                                                        <!-- </tr> -->
                                                                    </tr>
                                                                    <?php $i++; ?>
                                                                <?php endforeach; ?>
                                                            </tbody>

                                                        </table>
                                                    </div>
                                                </div>
                                            </div>


                                            <input type="hidden" id="id" class="form-control" name="id" placeholder="id" value="<?= $receive_data['id']; ?>">


                                            <hr>
                                            <!-- <div class="row"> -->
                                            <div class="col-md-4">
                                                <label>Suhu setelah muat (Celcius)</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" id="suhu_setelah" class="form-control" name="suhu_setelah" placeholder="Suhu setelah" value="<?= $receive_data['suhu_setelah']; ?>">
                                                <?= form_error('suhu_setelah', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Jam setelah muat</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="time" id="jam_setelah" class="form-control" name="jam_setelah" placeholder="Jam setelah" value="<?= $receive_data['jam_setelah']; ?>">
                                                <?= form_error('jam_setelah', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>

                                            <!-- </div> -->

                                            <div class="col-sm-12 d-flex justify-content-end mt-2">
                                                <button type="submit" class="btn btn-success mr-1">Approve/Update</button>
                                                <button type="reset" class="btn btn-light-secondary">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- Basic Horizontal form layout section end -->
            </div>
        </div>
    </div>






    <!-- ======================================================================= RETURN =============================================================================== -->

<?php }
// ini yg di return
elseif (($receive_data['create_by'] == $this->session->userdata('id')) && $receive_data['status_form_rd_id'] == 2) {
?>

    <!-- Newest -->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <div class="row">
                    <a href="<?= base_url('worker/receive')  ?>" class="btn btn-icon btn-outline-primary ml-1 mb-2 mr-1"><i class="bx bx-arrow-back"></i></a>
                    <h1 class="h3 mb-2 text-gray-800"><?= $title; ?></h1>
                </div>
                <?php if ($receive_data['status_form_rd_id'] != 6) {
                    null;
                } else { ?>
                    <button type="button" class="btn btn-success glow mr-1 mb-1"><i class="bx bxs-download"></i>
                        <a href="<?= base_url('test/export/' . $receive_data['id']) ?>" target="_blank">
                            <span class="align-middle ml-25" style="color: white;">Export PDF</span>
                        </a>
                    </button>
                <?php } ?>
                <section id="basic-horizontal-layouts">
                    <div class="row match-height">
                        <div class="col-md-12 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <!-- <h4 class="card-title"><?= $title; ?></h4> -->
                                </div>
                                <div class="card-body">
                                    <!-- <?= form_open_multipart('coordinator/approvalDocDetailDone/' . $receive_data['id']); ?> -->
                                    <?= form_open_multipart('worker/kirimUlangDelivery/' . $receive_data['id']); ?>


                                    <div class="form-body">
                                        <!-- <?php if (validation_errors()) : ?>
                                            <div class="alert alert-danger" role="alert">
                                                <?= validation_errors(); ?>
                                            </div>
                                        <?php endif; ?> -->
                                        <?= $this->session->flashdata('message'); ?>
                                        <div class="row">
                                            <?php if ($receive_data['status_form_rd_id'] == 2) { ?>
                                                <div class="col-md-12">
                                                    <div class="alert alert-warning" role="alert">Return oleh <?= $receive_data['names'] ?>: <?= $receive_data['alasan_return'] ?> </div>
                                                </div>
                                            <?php } elseif ($receive_data['status_form_rd_id'] == 3) {
                                            ?>
                                                <div class="col-md-12">
                                                    <div class="alert alert-danger" role="alert">Reject oleh <?= $receive_data['names'] ?>: <?= $receive_data['alasan_reject'] ?> </div>
                                                </div>
                                            <?php
                                            } ?>
                                            <div class="col-md-4">
                                                <label>Tanggal</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <fieldset class="form-group position-relative has-icon-left">
                                                    <!-- <input type="date" name="dataPicker" class="form-control pickadate" placeholder="Select Date" value="<?= set_value('dataPicker'); ?>"> -->
                                                    <input type="date" name="dataPicker" class="form-control" placeholder="Select Date" value="<?= $receive_data['tanggal']; ?>">
                                                    <div class="form-control-position">
                                                        <i class='bx bx-calendar'></i>
                                                    </div>
                                                </fieldset>
                                                <?= form_error('dataPicker', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Distributor/Customer</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" id="dist_cust" class="form-control" name="dist_cust" placeholder="Distributor or customer" value="<?= $receive_data['distributor_customer']; ?>">
                                                <?= form_error('dist_cust', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Ekspedisi</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" id="ekspedisi" class="form-control" name="ekspedisi" placeholder="Ekspedisi" value="<?= $receive_data['ekspedisi']; ?>">
                                                <?= form_error('ekspedisi', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <div class="col-md-4">
                                                <label>No Polisi</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" id="no_polisi" class="form-control" name="no_polisi" placeholder="No Plat Polisi" value="<?= $receive_data['no_polisi']; ?>">
                                                <?= form_error('no_polisi', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Suhu awal muat (Celcius)</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" id="suhu_awal" class="form-control" name="suhu_awal" placeholder="Suhu awal" value="<?= $receive_data['suhu_awal']; ?>">
                                                <?= form_error('suhu_awal', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <!-- <div class="col-md-4">
                                                <label>Suhu setelah muat (Celcius)</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" id="suhu_setelah" class="form-control" name="suhu_setelah" placeholder="Suhu setelah" value="<?= $receive_data['suhu_setelah']; ?>">
                                                <?= form_error('suhu_setelah', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div> -->
                                            <div class="col-md-4">
                                                <label>Jam awal muat</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="time" id="jam_awal" class="form-control" name="jam_awal" placeholder="Jam awal" value="<?= $receive_data['jam_awal']; ?>">
                                                <?= form_error('jam_awal', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <!-- <div class="col-md-4">
                                                <label>Jam setelah muat</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="time" id="jam_setelah" class="form-control" name="jam_setelah" placeholder="Jam setelah" value="<?= $receive_data['jam_setelah']; ?>">
                                                <?= form_error('jam_setelah', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div> -->






                                            <div class="col-md-12 mt-2 mb-2">
                                                <h4 class="card-title">Kondisi kendaraan <small class="text-muted">*</small></h4>
                                            </div>



                                            <div class="col-md-4">
                                                <label>Kebersihan</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <ul class="list-unstyled mb-0">
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="kebersihan" id="kebersihan1" value="Bersih" <?= strtolower($receive_data['kebersihan_kendaraan']) == 'bersih' ? 'checked' : ''; ?>>
                                                                <label for="kebersihan1">Bersih</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="kebersihan" id="kebersihan2" value="Tidak bersih" <?= strtolower($receive_data['kebersihan_kendaraan']) == 'tidak bersih' ? 'checked' : ''; ?>>
                                                                <label for="kebersihan2">Tidak bersih</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                </ul>
                                            </div>
                                            <?= form_error('kebersihan', '<small class="text-danger pl-3">', '</small>'); ?>


                                            <div class="col-md-4">
                                                <label>Bunyi</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <ul class="list-unstyled mb-0">
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="bunyi" id="bunyi1" value="Berisik" <?= strtolower($receive_data['bunyi_mesin_kendaraan']) == 'berisik' ? 'checked' : ''; ?>>
                                                                <label for="bunyi1">Berisik</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="bunyi" id="bunyi2" value="Tidak berisik" <?= strtolower($receive_data['bunyi_mesin_kendaraan']) == 'tidak berisik' ? 'checked' : ''; ?>>
                                                                <label for="bunyi2">Tidak berisik</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                </ul>
                                            </div>
                                            <?= form_error('bunyi', '<small class="text-danger pl-3">', '</small>'); ?>


                                            <div class="col-md-4">
                                                <label>Asap Kendaraan</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <ul class="list-unstyled mb-0">
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="asap" id="asap1" value="Hitam" <?= strtolower($receive_data['asap_kendaraan']) == 'hitam' ? 'checked' : ''; ?>>
                                                                <label for="asap1">Hitam</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="asap" id="asap2" value="Tidak hitam" <?= strtolower($receive_data['asap_kendaraan']) == 'tidak hitam' ? 'checked' : ''; ?>>
                                                                <label for="asap2">Tidak hitam</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                </ul>
                                            </div>
                                            <?= form_error('asap', '<small class="text-danger pl-3">', '</small>'); ?>


                                            <div class="col-md-4">
                                                <label>Bau</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <ul class="list-unstyled mb-0">
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="bau" id="bau1" value="Bau" <?= strtolower($receive_data['bau_kendaraan']) == 'bau' ? 'checked' : ''; ?>>
                                                                <label for="bau1">Bau</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="bau" id="bau2" value="Tidak bau" <?= strtolower($receive_data['bau_kendaraan']) == 'tidak bau' ? 'checked' : ''; ?>>
                                                                <label for="bau2">Tidak bau</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                </ul>
                                            </div>
                                            <?= form_error('bau', '<small class="text-danger pl-3">', '</small>'); ?>


                                            <div class="col-md-4">
                                                <label>Kebocoran</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <ul class="list-unstyled mb-0">
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="kebocoran" id="kebocoran1" value="Bocor" <?= strtolower($receive_data['kebocoran_kendaraan']) == 'bocor' ? 'checked' : ''; ?>>
                                                                <label for="kebocoran1">Bocor</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="kebocoran" id="kebocoran2" value="Tidak bocor" <?= strtolower($receive_data['kebocoran_kendaraan']) == 'tidak bocor' ? 'checked' : ''; ?>>
                                                                <label for="kebocoran2">Tidak bocor</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                </ul>
                                            </div>
                                            <?= form_error('kebocoran', '<small class="text-danger pl-3">', '</small>'); ?>

                                            <div class="col-md-4">
                                                <label>Uji Emisi/KIR</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <ul class="list-unstyled mb-0">
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="emisi" id="emisi1" value="Ya" <?= strtolower($receive_data['emisi_kendaraan']) == 'ya' ? 'checked' : ''; ?>>
                                                                <label for="emisi1">Ya</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="emisi" id="emisi2" value="Tidak" <?= strtolower($receive_data['emisi_kendaraan']) == 'tidak' ? 'checked' : ''; ?>>
                                                                <label for="emisi2">Tidak</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                </ul>
                                            </div>
                                            <?= form_error('emisi', '<small class="text-danger pl-3">', '</small>'); ?>


                                            <div class="col-md-4">
                                                <label>Tetesan Oli</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <ul class="list-unstyled mb-0">
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="oli" id="oli1" value="Ya" <?= strtolower($receive_data['oli_kendaraan']) == 'ya' ? 'checked' : ''; ?>>
                                                                <label for="oli1">Ya</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="oli" id="oli2" value="Tidak" <?= strtolower($receive_data['oli_kendaraan']) == 'tidak' ? 'checked' : ''; ?>>
                                                                <label for="oli2">Tidak</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                </ul>
                                            </div>
                                            <?= form_error('oli', '<small class="text-danger pl-3">', '</small>'); ?>




                                            <div class="col-md-12 mt-2 mb-2">
                                                <h4 class="card-title">Pengirim <small class="text-muted">*</small></h4>
                                            </div>


                                            <div class="col-md-4">
                                                <label>Nama Ekspedisi (Nama Pengirim)</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" id="nama_ekspedisi" class="form-control" name="nama_ekspedisi" placeholder="Nama" value="<?= $receive_data['nama_ekspedisi']; ?>" disabled>
                                                <?= form_error('nama_ekspedisi', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Foto Pengirim</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <div class="row">
                                                    <div class="col-sm-9">
                                                        <div class="custom-file">
                                                            <input type="file" class="custom-file-input" id="image" name="image" value="<?= $receive_data['foto_ekspedisi']; ?>" disabled>
                                                            <label class="custom-file-label" for="image">Choose file</label>
                                                            <?= form_error('image', '<small class="text-danger pl-3">', '</small>'); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                            </div>
                                            <div class="col-sm-3">
                                                <img src="<?= base_url('assets/img/foto_ekspedisi/') . $receive_data['foto_ekspedisi']; ?>" class="img-thumbnail">
                                            </div>

                                            <hr>


                                            <hr>
                                            <div class="col-md-12 mt-2 mb-2">
                                                <h4 class="card-title">Catatan <small class="text-muted"></small></h4>
                                                <small class="text-danger">(catat pada kolom ini jika terdapat produk kondisi basah/rusak dalam bentuk apapun atau terjadi
                                                    penyimpangan):*</small>
                                            </div>


                                            <div class="col-md-4">
                                                <label>Catatan</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" id="catatan" class="form-control" name="catatan" placeholder="Catatan" value="<?= $receive_data['catatan']; ?>">
                                                <?= form_error('catatan', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>

                                            <div class="col-md-4">
                                                <label>No seal/segel</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" id="segel" class="form-control" name="segel" placeholder="No seal/segel" value="<?= $receive_data['segel']; ?>">
                                                <?= form_error('segel', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>


                                            <hr>

                                            <div class="col-md-12 mt-2 mb-2">
                                                <h4 class="card-title">Data <small class="text-muted">*</small></h4>
                                            </div>


                                            <!-- Step 2 -->
                                            <h6>
                                                <i class="step-icon"></i>
                                            </h6>
                                            <!-- Step 2 end-->
                                            <!-- body content of step 2 -->
                                            <div class="col-md-12" style="margin: 10px;">
                                                <div class="box box-solid">
                                                    <div class="box-body">
                                                        <br>
                                                        <table class="table table-bordered" id="tableLoop">
                                                            <thead>
                                                                <tr>
                                                                    <th class="text-center col-md-1 col-sm-1">#</th>
                                                                    <th>Data receive</th>
                                                                    <!-- <th class="col-md-2"><button class="btn btn-success btn-block" id="BarisBaru"><i class="fa fa-plus"></i> Baris Baru</button></th> -->
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php $i = 1; ?>
                                                                <?php foreach ($rd_data as $r) : ?>
                                                                    <tr>
                                                                        <td class="text-center"> <?= $i ?>
                                                                        <td>
                                                                            <small><cite title="Source Title">Batch number</cite></small>
                                                                            <input type="text" name="bn[]" class="form-control bn" placeholder="Batch number" required="" value="<?= $r['batch_number']; ?>">
                                                                            <br />
                                                                            <small><cite title="Source Title">LPN number</cite></small>
                                                                            <input type="text" name="lpn[]" class="form-control lpn" placeholder="LPN number" required="" value="<?= $r['lpn_number']; ?>">
                                                                            <br />
                                                                            <small><cite title="Source Title">Quantity MB</cite></small>
                                                                            <input type="text" name="qty_mb[]" class="form-control qty_mb" placeholder="Quantity MB" required="" value="<?= $r['qty_mb']; ?>">
                                                                            <br />
                                                                            <small><cite title="Source Title">Quantity per MB</cite></small>
                                                                            <input type="text" name="qty_per_mb[]" class="form-control qty_per_mb" placeholder="Quantity per MB" required="" value="<?= $r['qty_per_mb']; ?>">
                                                                            <br />
                                                                            <small><cite title="Source Title">Quantity Pick</cite></small>
                                                                            <input type="text" name="qty_pick[]" class="form-control qty_pick" placeholder="Quantity Pick" required="" value="<?= $r['qty_pick']; ?>">
                                                                            <br />
                                                                            <small><cite title="Source Title">Product Checklist Out</cite></small>
                                                                            <select name="pco[]" id="pco" class="form-control">
                                                                                <!-- <option value="Y">OK</option>
                                                                                <option value="N">NOK</option> -->
                                                                                <option value="Y" <?= strtolower($r['product_checklist_out']) == 'y' ? 'selected' : ''; ?>>OK</option>
                                                                                <option value="N" <?= strtolower($r['product_checklist_out']) == 'n' ? 'selected' : ''; ?>>NOK</option>
                                                                            </select>
                                                                            <br />
                                                                            <small><cite title="Source Title">Quantity EPM Pallet</cite></small>
                                                                            <input type="text" name="qep[]" class="form-control qep" placeholder="Quantity EPM Pallet (number)" required="" value="<?= $r['qty_epm_pallet']; ?>">
                                                                            <br />
                                                                            <input type="hidden" id="data_rd_id" class="form-control" name="data_rd_id[]" placeholder="id" value="<?= $r['id']; ?>">
                                                                        </td>
                                                                        <!-- <td class="text-center">
                                                                            <a class="btn btn-sm btn-danger" data-toggle="tooltip" title="Hapus Baris" id="HapusBaris"><i class="bx bxs-x-square"></i></a>
                                                                        </td> -->

                                                                        <!-- </tr> -->
                                                                    </tr>
                                                                    <?php $i++; ?>
                                                                <?php endforeach; ?>
                                                            </tbody>

                                                        </table>
                                                    </div>
                                                </div>
                                            </div>


                                            <input type="hidden" id="id" class="form-control" name="id" placeholder="id" value="<?= $receive_data['id']; ?>">


                                            <hr>
                                            <!-- <div class="row"> -->
                                            <div class="col-md-4">
                                                <label>Suhu setelah muat (Celcius)</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" id="suhu_setelah" class="form-control" name="suhu_setelah" placeholder="Suhu setelah" value="<?= $receive_data['suhu_setelah']; ?>">
                                                <?= form_error('suhu_setelah', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Jam setelah muat</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="time" id="jam_setelah" class="form-control" name="jam_setelah" placeholder="Jam setelah" value="<?= $receive_data['jam_setelah']; ?>">
                                                <?= form_error('jam_setelah', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <!-- </div> -->

                                            <div class="col-sm-12 d-flex justify-content-end mt-2">
                                                <button type="submit" class="btn btn-success mr-1">Kirim Ulang</button>
                                                <button type="reset" class="btn btn-light-secondary">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- Basic Horizontal form layout section end -->
            </div>
        </div>
    </div>

    <!-- ======================================================================= ELSE/REJECT =============================================================================== -->
<?php
} else { ?>





    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <div class="row">
                    <a href="<?= base_url('worker/delivery')  ?>" class="btn btn-icon btn-outline-primary ml-1 mb-2 mr-1"><i class="bx bx-arrow-back"></i></a>
                    <h1 class="h3 mb-2 text-gray-800"><?= $title; ?></h1>
                </div>
                <?php if ($receive_data['status_form_rd_id'] != 6) {
                    null;
                } else { ?>
                    <button type="button" class="btn btn-success glow mr-1 mb-1"><i class="bx bxs-download"></i>
                        <a href="<?= base_url('test/export/' . $receive_data['id']) ?>" target="_blank">
                            <span class="align-middle ml-25" style="color: white;">Export PDF</span>
                        </a>
                    </button>
                <?php } ?>
                <section id="basic-horizontal-layouts">
                    <div class="row match-height">
                        <div class="col-md-12 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <!-- <h4 class="card-title"><?= $title; ?></h4> -->
                                </div>
                                <div class="card-body">
                                    <?= form_open_multipart('worker/deliveryAdd'); ?>

                                    <div class="form-body">
                                        <!-- <?php if (validation_errors()) : ?>
                                            <div class="alert alert-danger" role="alert">
                                                <?= validation_errors(); ?>
                                            </div>
                                        <?php endif; ?> -->
                                        <?= $this->session->flashdata('message'); ?>
                                        <div class="row">
                                            <?php if ($receive_data['status_form_rd_id'] == 2) { ?>
                                                <div class="col-md-12">
                                                    <div class="alert alert-warning" role="alert">Return oleh <?= $receive_data['names'] ?>: <?= $receive_data['alasan_return'] ?> </div>
                                                </div>
                                            <?php } elseif ($receive_data['status_form_rd_id'] == 3) {
                                            ?>
                                                <div class="col-md-12">
                                                    <div class="alert alert-danger" role="alert">Reject oleh <?= $receive_data['names'] ?>: <?= $receive_data['alasan_reject'] ?> </div>
                                                </div>
                                            <?php
                                            } ?>
                                            <div class="col-md-4">
                                                <label>Tanggal</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <fieldset class="form-group position-relative has-icon-left">
                                                    <!-- <input type="date" name="dataPicker" class="form-control pickadate" placeholder="Select Date" value="<?= set_value('dataPicker'); ?>"> -->
                                                    <input type="date" name="dataPicker" class="form-control" placeholder="Select Date" value="<?= $receive_data['tanggal']; ?>" disabled>
                                                    <div class="form-control-position">
                                                        <i class='bx bx-calendar'></i>
                                                    </div>
                                                </fieldset>
                                                <?= form_error('dataPicker', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Distributor/Customer</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" id="dist_cust" class="form-control" name="dist_cust" placeholder="Distributor or customer" value="<?= $receive_data['distributor_customer']; ?>" disabled>
                                                <?= form_error('dist_cust', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Ekspedisi</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" id="ekspedisi" class="form-control" name="ekspedisi" placeholder="Ekspedisi" value="<?= $receive_data['ekspedisi']; ?>" disabled>
                                                <?= form_error('ekspedisi', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <div class="col-md-4">
                                                <label>No Polisi</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" id="no_polisi" class="form-control" name="no_polisi" placeholder="No Plat Polisi" value="<?= $receive_data['no_polisi']; ?>" disabled>
                                                <?= form_error('no_polisi', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Suhu awal muat (Celcius)</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" id="suhu_awal" class="form-control" name="suhu_awal" placeholder="Suhu awal" value="<?= $receive_data['suhu_awal']; ?>" disabled>
                                                <?= form_error('suhu_awal', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <!-- <div class="col-md-4">
                                                <label>Suhu setelah muat (Celcius)</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" id="suhu_setelah" class="form-control" name="suhu_setelah" placeholder="Suhu setelah" value="<?= $receive_data['suhu_setelah']; ?>" disabled>
                                                <?= form_error('suhu_setelah', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div> -->
                                            <div class="col-md-4">
                                                <label>Jam awal muat</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="time" id="jam_awal" class="form-control" name="jam_awal" placeholder="Jam awal" value="<?= $receive_data['jam_awal']; ?>" disabled>
                                                <?= form_error('jam_awal', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <!-- <div class="col-md-4">
                                                <label>Jam setelah muat</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="time" id="jam_setelah" class="form-control" name="jam_setelah" placeholder="Jam setelah" value="<?= $receive_data['jam_setelah']; ?>" disabled>
                                                <?= form_error('jam_setelah', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div> -->






                                            <div class="col-md-12 mt-2 mb-2">
                                                <h4 class="card-title">Kondisi kendaraan <small class="text-muted">*</small></h4>
                                            </div>



                                            <div class="col-md-4">
                                                <label>Kebersihan</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <ul class="list-unstyled mb-0">
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="kebersihan" id="kebersihan1" value="Bersih" <?= strtolower($receive_data['kebersihan_kendaraan']) == 'bersih' ? 'checked' : ''; ?> disabled>
                                                                <label for="kebersihan1">Bersih</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="kebersihan" id="kebersihan2" value="Tidak bersih" <?= strtolower($receive_data['kebersihan_kendaraan']) == 'tidak bersih' ? 'checked' : ''; ?> disabled>
                                                                <label for="kebersihan2">Tidak bersih</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                </ul>
                                            </div>
                                            <?= form_error('kebersihan', '<small class="text-danger pl-3">', '</small>'); ?>


                                            <div class="col-md-4">
                                                <label>Bunyi</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <ul class="list-unstyled mb-0">
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="bunyi" id="bunyi1" value="Berisik" <?= strtolower($receive_data['bunyi_mesin_kendaraan']) == 'berisik' ? 'checked' : ''; ?> disabled>
                                                                <label for="bunyi1">Berisik</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="bunyi" id="bunyi2" value="Tidak berisik" <?= strtolower($receive_data['bunyi_mesin_kendaraan']) == 'tidak berisik' ? 'checked' : ''; ?> disabled>
                                                                <label for="bunyi2">Tidak berisik</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                </ul>
                                            </div>
                                            <?= form_error('bunyi', '<small class="text-danger pl-3">', '</small>'); ?>


                                            <div class="col-md-4">
                                                <label>Asap Kendaraan</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <ul class="list-unstyled mb-0">
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="asap" id="asap1" value="Hitam" <?= strtolower($receive_data['asap_kendaraan']) == 'hitam' ? 'checked' : ''; ?> disabled>
                                                                <label for="asap1">Hitam</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="asap" id="asap2" value="Tidak hitam" <?= strtolower($receive_data['asap_kendaraan']) == 'tidak hitam' ? 'checked' : ''; ?> disabled>
                                                                <label for="asap2">Tidak hitam</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                </ul>
                                            </div>
                                            <?= form_error('asap', '<small class="text-danger pl-3">', '</small>'); ?>


                                            <div class="col-md-4">
                                                <label>Bau</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <ul class="list-unstyled mb-0">
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="bau" id="bau1" value="Bau" <?= strtolower($receive_data['bau_kendaraan']) == 'bau' ? 'checked' : ''; ?> disabled>
                                                                <label for="bau1">Bau</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="bau" id="bau2" value="Tidak bau" <?= strtolower($receive_data['bau_kendaraan']) == 'tidak bau' ? 'checked' : ''; ?> disabled>
                                                                <label for="bau2">Tidak bau</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                </ul>
                                            </div>
                                            <?= form_error('bau', '<small class="text-danger pl-3">', '</small>'); ?>


                                            <div class="col-md-4">
                                                <label>Kebocoran</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <ul class="list-unstyled mb-0">
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="kebocoran" id="kebocoran1" value="Bocor" <?= strtolower($receive_data['kebocoran_kendaraan']) == 'bocor' ? 'checked' : ''; ?> disabled>
                                                                <label for="kebocoran1">Bocor</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="kebocoran" id="kebocoran2" value="Tidak bocor" <?= strtolower($receive_data['kebocoran_kendaraan']) == 'tidak bocor' ? 'checked' : ''; ?> disabled>
                                                                <label for="kebocoran2">Tidak bocor</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                </ul>
                                            </div>
                                            <?= form_error('kebocoran', '<small class="text-danger pl-3">', '</small>'); ?>


                                            <div class="col-md-4">
                                                <label>Uji Emisi/KIR</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <ul class="list-unstyled mb-0">
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="emisi" id="emisi1" value="Ya" <?= strtolower($receive_data['emisi_kendaraan']) == 'ya' ? 'checked' : ''; ?> disabled>
                                                                <label for="emisi1">Ya</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="emisi" id="emisi2" value="Tidak" <?= strtolower($receive_data['emisi_kendaraan']) == 'tidak' ? 'checked' : ''; ?> disabled>
                                                                <label for="emisi2">Tidak</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                </ul>
                                            </div>
                                            <?= form_error('emisi', '<small class="text-danger pl-3">', '</small>'); ?>


                                            <div class="col-md-4">
                                                <label>Tetesan Oli</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <ul class="list-unstyled mb-0">
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="oli" id="oli1" value="Ya" <?= strtolower($receive_data['oli_kendaraan']) == 'ya' ? 'checked' : ''; ?> disabled>
                                                                <label for="oli1">Ya</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="oli" id="oli2" value="Tidak" <?= strtolower($receive_data['oli_kendaraan']) == 'tidak' ? 'checked' : ''; ?> disabled>
                                                                <label for="oli2">Tidak</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                </ul>
                                            </div>
                                            <?= form_error('oli', '<small class="text-danger pl-3">', '</small>'); ?>

                                            <div class="col-md-12 mt-2 mb-2">
                                                <h4 class="card-title">Pengirim <small class="text-muted">*</small></h4>
                                            </div>


                                            <div class="col-md-4">
                                                <label>Nama Ekspedisi (Nama Pengirim)</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" id="nama_ekspedisi" class="form-control" name="nama_ekspedisi" placeholder="Nama" value="<?= $receive_data['nama_ekspedisi']; ?>" disabled>
                                                <?= form_error('nama_ekspedisi', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Foto Pengirim</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <div class="row">
                                                    <div class="col-sm-9">
                                                        <div class="custom-file">
                                                            <input type="file" class="custom-file-input" id="image" name="image" value="<?= $receive_data['foto_ekspedisi']; ?>" disabled>
                                                            <label class="custom-file-label" for="image">Choose file</label>
                                                            <?= form_error('image', '<small class="text-danger pl-3">', '</small>'); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                            </div>
                                            <div class="col-sm-3">
                                                <img src="<?= base_url('assets/img/foto_ekspedisi/') . $receive_data['foto_ekspedisi']; ?>" class="img-thumbnail">
                                            </div>



                                            <hr>
                                            <div class="col-md-12 mt-2 mb-2">
                                                <h4 class="card-title">Catatan <small class="text-muted"></small></h4>
                                                <small class="text-danger">(catat pada kolom ini jika terdapat produk kondisi basah/rusak dalam bentuk apapun atau terjadi
                                                    penyimpangan):*</small>
                                            </div>


                                            <div class="col-md-4">
                                                <label>Catatan</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" id="catatan" class="form-control" name="catatan" placeholder="Catatan" value="<?= $receive_data['catatan']; ?>" disabled>
                                                <?= form_error('catatan', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>

                                            <div class="col-md-4">
                                                <label>No seal/segel</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" id="segel" class="form-control" name="segel" placeholder="No seal/segel" value="<?= $receive_data['segel']; ?>" disabled>
                                                <?= form_error('segel', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>


                                            <hr>

                                            <div class="col-md-12 mt-2 mb-2">
                                                <h4 class="card-title">Data <small class="text-muted">*</small></h4>
                                            </div>


                                            <!-- Step 2 -->
                                            <h6>
                                                <i class="step-icon"></i>
                                            </h6>
                                            <!-- Step 2 end-->
                                            <!-- body content of step 2 -->
                                            <div class="col-md-12" style="margin: 10px;">
                                                <div class="box box-solid">
                                                    <div class="box-body">
                                                        <br>
                                                        <table class="table table-bordered" id="tableLoop">
                                                            <thead>
                                                                <tr>
                                                                    <th class="text-center col-md-1 col-sm-1">#</th>
                                                                    <th>Data receive</th>
                                                                    <!-- <th class="col-md-2"><button class="btn btn-success btn-block" id="BarisBaru"><i class="fa fa-plus"></i> Baris Baru</button></th> -->
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php $i = 1; ?>
                                                                <?php foreach ($rd_data as $r) : ?>
                                                                    <tr>
                                                                        <td class="text-center"> <?= $i ?>
                                                                        <td>
                                                                            <small><cite title="Source Title">Batch number</cite></small>
                                                                            <input type="text" name="bn[]" class="form-control bn" placeholder="Batch number" required="" value="<?= $r['batch_number']; ?>" disabled>
                                                                            <br />
                                                                            <small><cite title="Source Title">LPN number</cite></small>
                                                                            <input type="text" name="lpn[]" class="form-control lpn" placeholder="LPN number" required="" value="<?= $r['lpn_number']; ?>" disabled>
                                                                            <br />
                                                                            <small><cite title="Source Title">Quantity MB</cite></small>
                                                                            <input type="text" name="qty_mb[]" class="form-control qty_mb" placeholder="Quantity MB" required="" value="<?= $r['qty_mb']; ?>" disabled>
                                                                            <br />
                                                                            <small><cite title="Source Title">Quantity per MB</cite></small>
                                                                            <input type="text" name="qty_per_mb[]" class="form-control qty_per_mb" placeholder="Quantity per MB" required="" value="<?= $r['qty_per_mb']; ?>" disabled>
                                                                            <br />
                                                                            <small><cite title="Source Title">Quantity Pick</cite></small>
                                                                            <input type="text" name="qty_pick[]" class="form-control qty_pick" placeholder="Quantity Pick" required="" value="<?= $r['qty_pick']; ?>" disabled>
                                                                            <br />
                                                                            <small><cite title="Source Title">Product Checklist Out</cite></small>
                                                                            <select name="pco[]" id="pco" class="form-control" disabled>
                                                                                <!-- <option value="Y">OK</option>
                                                                                <option value="N">NOK</option> -->
                                                                                <option value="Y" <?= strtolower($r['product_checklist_out']) == 'y' ? 'selected' : ''; ?>>OK</option>
                                                                                <option value="N" <?= strtolower($r['product_checklist_out']) == 'n' ? 'selected' : ''; ?>>NOK</option>
                                                                            </select>
                                                                            <br />
                                                                            <small><cite title="Source Title">Quantity EPM Pallet</cite></small>
                                                                            <input type="text" name="qep[]" class="form-control qep" placeholder="Quantity EPM Pallet (number)" required="" value="<?= $r['qty_epm_pallet']; ?>" disabled>
                                                                            <br />
                                                                        </td>
                                                                        <!-- <td class="text-center">
                                                                            <a class="btn btn-sm btn-danger" data-toggle="tooltip" title="Hapus Baris" id="HapusBaris"><i class="bx bxs-x-square"></i></a>
                                                                        </td> -->

                                                                        <!-- </tr> -->
                                                                    </tr>
                                                                    <?php $i++; ?>
                                                                <?php endforeach; ?>
                                                            </tbody>

                                                        </table>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                        <hr>
                                        <!-- <div class="row"> -->
                                        <div class="col-md-4">
                                            <label>Suhu setelah muat (Celcius)</label>
                                        </div>
                                        <div class="col-md-8 form-group">
                                            <input type="text" id="suhu_setelah" class="form-control" name="suhu_setelah" placeholder="Suhu setelah" value="<?= $receive_data['suhu_setelah']; ?>" disabled>
                                            <?= form_error('suhu_setelah', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Jam setelah muat</label>
                                        </div>
                                        <div class="col-md-8 form-group">
                                            <input type="time" id="jam_setelah" class="form-control" name="jam_setelah" placeholder="Jam setelah" value="<?= $receive_data['jam_setelah']; ?>" disabled>
                                            <?= form_error('jam_setelah', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>
                                        <!-- </div> -->
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>


<?php } ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>

<!-- <script>
    $(document).ready(function() {
        for (B = 1; B <= 1; B++) {
            Barisbaru();
        }
        $('#BarisBaru').click(function(e) {
            e.preventDefault();
            Barisbaru();
        });

        $("tableLoop tbody").find('input[type=text]').filter(':visible:first').focus();
    });

    //     <div class="col-md-2 col-sm-12 form-group">
    //                     <label for="">Product Checklist Out </label>
    //                     <div class="col-md-2 col-sm-12 form-group">
    //                                                         <label for="gender">Gender</label>
    // <select name="pco[]" id="pco" class="form-control">
    //     <option value="Y">OK</option>
    //     <option value="N">NOK</option>
    // </select>

    function Barisbaru() {
        $(document).ready(function() {
            $("[data-toggle='tooltip']").tooltip();
        });
        var Nomor = $("#tableLoop tbody tr").length + 1;
        var Baris = '<tr>';
        Baris += '<td class="text-center">' + Nomor + '</td>';
        Baris += '<td>';
        Baris += '<small><cite title="Source Title">Batch number</cite></small>';
        Baris += '<input type="text" name="bn[]" class="form-control bn" placeholder="Batch number" required="">';
        Baris += '<br/>'
        Baris += '<small><cite title="Source Title">LPN number</cite></small>';
        Baris += '<input type="text" name="lpn[]" class="form-control lpn" placeholder="LPN number" required="">';
        Baris += '<br/>'
        Baris += '<small><cite title="Source Title">Quantity MB</cite></small>';
        Baris += '<input type="text" name="qty_mb[]" class="form-control qty_mb" placeholder="Quantity MB" required="">';
        Baris += '<br/>'
        Baris += '<small><cite title="Source Title">Quantity per MB</cite></small>';
        Baris += '<input type="text" name="qty_per_mb[]" class="form-control qty_per_mb" placeholder="Quantity per MB" required="">';
        Baris += '<br/>'
        Baris += '<small><cite title="Source Title">Quantity Pick</cite></small>';
        Baris += '<input type="text" name="qty_pick[]" class="form-control qty_pick" placeholder="Quantity Pick" required="">';
        Baris += '<br/>'
        Baris += '<small><cite title="Source Title">Product Checklist Out</cite></small>';
        Baris += '<select name="pco[]" id="pco" class="form-control">';
        Baris += '<option value="Y">OK</option>';
        Baris += '<option value="N">NOK</option>';
        Baris += '</select>';
        Baris += '<br/>'
        Baris += '<small><cite title="Source Title">Quantity EPM Pallet</cite></small>';
        Baris += '<input type="text" name="qep[]" class="form-control qep" placeholder="Quantity EPM Pallet" required="">';
        Baris += '<br/>'
        Baris += '</td>';
        Baris += '<td class="text-center">';
        Baris += '<a class="btn btn-sm btn-danger" data-toggle="tooltip" title="Hapus Baris" id="HapusBaris"><i class="bx bxs-x-square"></i></a>';
        Baris += '</td>';

        Baris += '</tr>';

        $("#tableLoop tbody").append(Baris);
        $("#tableLoop tbody tr").each(function() {
            // $(this).find('td:nth-child(2) input').focus();
        });

    }

    $(document).on('click', '#HapusBaris', function(e) {
        e.preventDefault();
        var Nomor = 1;
        $(this).parent().parent().remove();
        $('tableLoop tbody tr').each(function() {
            $(this).find('td:nth-child(1)').html(Nomor);
            Nomor++;
        });
    });

    // $(document).ready(function() {
    //     $('#SimpanData').submit(function(e) {
    //         e.preventDefault();
    //         biodata();
    //     });
    // });

    function biodata() {
        $.ajax({
            url: $("#SimpanData").attr('action'),
            type: 'post',
            cache: false,
            dataType: "json",
            data: $("#SimpanData").serialize(),
            success: function(data) {
                if (data.success == true) {
                    $('.first_name').val('');
                    $('.last_name').val('');
                    $('#notif').fadeIn(800, function() {
                        $("#notif").html(data.notif).fadeOut(5000).delay(800);
                    });
                } else {
                    $('#notif').html('<div class="alert alert-danger">Data Gagal Disimpan</div>')
                }
            },

            error: function(error) {
                alert(error);
            }

        });
    }
</script> -->

<!-- <script type="text/javascript">
    $( "#datepicker" ).datepicker();      
</script> -->