<script type="text/javascript">
    // $('.pickadate').pickadate({
    //     format: 'yyyy-mm-dd'
    // });

    // $("#datepicker").datepicker({
    //     format: 'yyyy-mm-dd'
    // });
</script>
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <div class="row">
                <a href="<?= base_url('erp/listApprovalDocInspectionChecklist')  ?>" class="btn btn-icon btn-outline-primary ml-1 mb-2 mr-1"><i class="bx bx-arrow-back"></i></a>
                <h1 class="h3 mb-2 text-gray-800"><?= $title; ?></h1>
            </div>
            <section id="basic-horizontal-layouts">
                <div class="row match-height">
                    <div class="col-md-12 col-12">
                        <div class="card">
                            <div class="card-header">
                            </div>
                            <div class="card-body">
                                <?= form_open_multipart('erp/approvalDocDetailInspectionChecklist/' . $receive_data['id']); ?>

                                <div class="form-body">
                                    <!-- <?php if (validation_errors()) : ?>
                                            <div class="alert alert-danger" role="alert">
                                                <?= validation_errors(); ?>
                                            </div>
                                        <?php endif; ?> -->
                                    <?= $this->session->flashdata('message'); ?>


                                    <div class="mx-auto text-center mb-4" style="width: 200px;">
                                        <h2 class="card-title">7 CHECKLIST INSPECTION<small class="text-muted"><br /> Pemeriksaan Kendaraan Ekspor</small></h2>
                                    </div>


                                    <div class="col-md-6 mb-2">
                                        <label>No Polisi :</label>
                                        <input type="text" id="no_polisi" class="form-control" name="no_polisi" placeholder="No Polisi" value="<?= $receive_data['no_polisi']; ?>">
                                        <?= form_error('no_polisi', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>



                                    <div class="row">
                                        <div class="col-md-12 mb-2">
                                            <h4 class="card-title">1. Bagian luar<small class="text-muted"> (Outside/Undercarriage)</small></h4>
                                        </div>



                                        <div class="col-md-6">
                                            <label>a. Pada bagian kontainer jika ada perbaikan, rusak , belubang, penyok</label>
                                        </div>

                                        <div class="col-md-6 form-group">
                                            <ul class="list-unstyled mb-0">
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio">
                                                            <input type="radio" name="1a" id="1a" value="Y" <?= strtolower($receive_data['1a']) == 'y' ? 'checked' : ''; ?>>
                                                            <label for="1a">Ya</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio">
                                                            <input type="radio" name="1a" id="1a2" value="N" <?= strtolower($receive_data['1a']) == 'n' ? 'checked' : ''; ?>>
                                                            <label for="1a2">Tidak</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                            </ul>
                                            <?= form_error('1a', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>

                                        <div class="col-md-6">
                                            <label>b. Bagian rangka kontainer terlihat</label>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <ul class="list-unstyled mb-0">
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio">
                                                            <input type="radio" name="1b" id="1b" value="Y" <?= strtolower($receive_data['1b']) == 'y' ? 'checked' : ''; ?>>
                                                            <label for="1b">Ya</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio">
                                                            <input type="radio" name="1b" id="1b2" value="N" <?= strtolower($receive_data['1b']) == 'n' ? 'checked' : ''; ?>>
                                                            <label for="1b2">Tidak</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                            </ul>
                                            <?= form_error('1b', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>

                                        <div class="col-md-6">
                                            <label>c. Pastikan tidak ada benda lain yang menempel pada kontainer</label>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <ul class="list-unstyled mb-0">
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio">
                                                            <input type="radio" name="1c" id="1c" value="Y" <?= strtolower($receive_data['1c']) == 'y' ? 'checked' : ''; ?>>
                                                            <label for="1c">Ya</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio">
                                                            <input type="radio" name="1c" id="1c2" value="N" <?= strtolower($receive_data['1c']) == 'n' ? 'checked' : ''; ?>>
                                                            <label for="1c2">Tidak</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                            </ul>
                                            <?= form_error('1c', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>


                                        <!-- Bagian 2 -->
                                        <!-- 2.Bagian dalam dan luar pintu (Inside/Outside Doors) -->
                                        <div class="col-md-12 mt-4 mb-2">
                                            <h4 class="card-title">2. Bagian dalam dan luar pintu<small class="text-muted"> (Inside/Outside Doors)</small></h4>
                                        </div>

                                        <div class="col-md-6">
                                            <label>a. Pastikan kunci, dan mekanisme penguncian aman dan terjamin</label>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <ul class="list-unstyled mb-0">
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio">
                                                            <input type="radio" name="2a" id="2a" value="Y" <?= strtolower($receive_data['2a']) == 'y' ? 'checked' : ''; ?>>
                                                            <label for="2a">Ya</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio">
                                                            <input type="radio" name="2a" id="2a2" value="N" <?= strtolower($receive_data['2a']) == 'n' ? 'checked' : ''; ?>>
                                                            <label for="2a2">Tidak</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                            </ul>
                                            <?= form_error('2a', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>

                                        <div class="col-md-6">
                                            <label>b. Cek jika ada baut yang hilang</label>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <ul class="list-unstyled mb-0">
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio">
                                                            <input type="radio" name="2b" id="2b" value="Y" <?= strtolower($receive_data['2b']) == 'y' ? 'checked' : ''; ?>>
                                                            <label for="2b">Ya</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio">
                                                            <input type="radio" name="2b" id="2b2" value="N" <?= strtolower($receive_data['2b']) == 'n' ? 'checked' : ''; ?>>
                                                            <label for="2b2">Tidak</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                            </ul>
                                            <?= form_error('2b', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>

                                        <div class="col-md-6">
                                            <label>c. Pastikan engsel pintu aman dan terjamin</label>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <ul class="list-unstyled mb-0">
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio">
                                                            <input type="radio" name="2c" id="2c" value="Y" <?= strtolower($receive_data['2c']) == 'y' ? 'checked' : ''; ?>>
                                                            <label for="2c">Ya</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio">
                                                            <input type="radio" name="2c" id="2c2" value="N" <?= strtolower($receive_data['2c']) == 'n' ? 'checked' : ''; ?>>
                                                            <label for="2c2">Tidak</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                            </ul>
                                            <?= form_error('2c', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>






                                        <!-- Bagian 3 -->
                                        <!-- 3. Dinding Kanan (Right Side) -->
                                        <div class="col-md-12 mt-4 mb-2">
                                            <h4 class="card-title">3. Dinding kanan<small class="text-muted"> (Right Side)</small></h4>
                                        </div>

                                        <div class="col-md-6">
                                            <label>a. Perhatikan perbaikan yang mencurigakan pada bagian kontainer</label>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <ul class="list-unstyled mb-0">
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio">
                                                            <input type="radio" name="3a" id="3a" value="Y" <?= strtolower($receive_data['3a']) == 'y' ? 'checked' : ''; ?>>
                                                            <label for="3a">Ya</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio">
                                                            <input type="radio" name="3a" id="3a2" value="N" <?= strtolower($receive_data['3a']) == 'n' ? 'checked' : ''; ?>>
                                                            <label for="3a2">Tidak</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                            </ul>
                                            <?= form_error('3a', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>

                                        <div class="col-md-6">
                                            <label>b. Perbaikan pada dinding kontainer harus terlihat dikedua sisi dalam dan diluar</label>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <ul class="list-unstyled mb-0">
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio">
                                                            <input type="radio" name="3b" id="3b" value="Y" <?= strtolower($receive_data['3b']) == 'y' ? 'checked' : ''; ?>>
                                                            <label for="3b">Ya</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio">
                                                            <input type="radio" name="3b" id="3b2" value="N" <?= strtolower($receive_data['3b']) == 'n' ? 'checked' : ''; ?>>
                                                            <label for="3b2">Tidak</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                            </ul>
                                            <?= form_error('3b', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>


                                        <!-- Bagian 4 -->
                                        <!-- 4. Dinding Kiri (Left Side) -->
                                        <div class="col-md-12 mt-4 mb-2">
                                            <h4 class="card-title">4. Dinding kiri<small class="text-muted"> (Left Side)</small></h4>
                                        </div>

                                        <div class="col-md-6">
                                            <label>a. Perhatikan perbaikan yang mencurigakan pada bagian kontainer</label>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <ul class="list-unstyled mb-0">
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio">
                                                            <input type="radio" name="4a" id="4a" value="Y" <?= strtolower($receive_data['4a']) == 'y' ? 'checked' : ''; ?>>
                                                            <label for="4a">Ya</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio">
                                                            <input type="radio" name="4a" id="4a2" value="N" <?= strtolower($receive_data['4a']) == 'n' ? 'checked' : ''; ?>>
                                                            <label for="4a2">Tidak</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                            </ul>
                                            <?= form_error('4a', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>

                                        <div class="col-md-6">
                                            <label>b. Perbaikan pada dinding kontainer harus terlihat dikedua sisi dalam dan diluar</label>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <ul class="list-unstyled mb-0">
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio">
                                                            <input type="radio" name="4b" id="4b" value="Y" <?= strtolower($receive_data['4b']) == 'y' ? 'checked' : ''; ?>>
                                                            <label for="4b">Ya</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio">
                                                            <input type="radio" name="4b" id="4b2" value="N" <?= strtolower($receive_data['4b']) == 'n' ? 'checked' : ''; ?>>
                                                            <label for="4b2">Tidak</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                            </ul>
                                            <?= form_error('4b', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>


                                        <!-- Bagian 5 -->
                                        <!-- 5. Dinding depan (Front Wall) -->
                                        <div class="col-md-12 mt-4 mb-2">
                                            <h4 class="card-title">5. Dinding depan<small class="text-muted"> (Front Wall)</small></h4>
                                        </div>

                                        <div class="col-md-6">
                                            <label>a. Bagian dinding terbuat dari bahan yang bergelombang</label>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <ul class="list-unstyled mb-0">
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio">
                                                            <input type="radio" name="5a" id="5a" value="Y" <?= strtolower($receive_data['5a']) == 'y' ? 'checked' : ''; ?>>
                                                            <label for="5a">Ya</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio">
                                                            <input type="radio" name="5a" id="5a2" value="N" <?= strtolower($receive_data['5a']) == 'n' ? 'checked' : ''; ?>>
                                                            <label for="5a2">Tidak</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                            </ul>
                                            <?= form_error('5a', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>

                                        <div class="col-md-6">
                                            <label>b. Tulangan pada pada sudut bagian kiri dan kanan atas harus terlihat , jika hilang
                                                atau salah adalah tidak normal.</label>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <ul class="list-unstyled mb-0">
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio">
                                                            <input type="radio" name="5b" id="5b" value="Y" <?= strtolower($receive_data['5b']) == 'y' ? 'checked' : ''; ?>>
                                                            <label for="5b">Ya</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio">
                                                            <input type="radio" name="5b" id="5b2" value="N" <?= strtolower($receive_data['5b']) == 'n' ? 'checked' : ''; ?>>
                                                            <label for="5b2">Tidak</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                            </ul>
                                            <?= form_error('5b', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>

                                        <div class="col-md-6">
                                            <label>c. Pastikan ventilasi terlihat</label>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <ul class="list-unstyled mb-0">
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio">
                                                            <input type="radio" name="5c" id="5c" value="Y" <?= strtolower($receive_data['5c']) == 'y' ? 'checked' : ''; ?>>
                                                            <label for="5c">Ya</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio">
                                                            <input type="radio" name="5c" id="5c2" value="N" <?= strtolower($receive_data['5c']) == 'n' ? 'checked' : ''; ?>>
                                                            <label for="5c2">Tidak</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                            </ul>
                                            <?= form_error('5c', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>



                                        <!-- Bagian 6 -->
                                        <!-- 6. Bagian atap (Celling/Roof) -->
                                        <div class="col-md-12 mt-4 mb-2">
                                            <h4 class="card-title">6. Bagian atap<small class="text-muted"> (Celling/Roof)</small></h4>
                                        </div>

                                        <div class="col-md-6">
                                            <label>a. Pastikan bagian rangka kontainer terlihat</label>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <ul class="list-unstyled mb-0">
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio">
                                                            <input type="radio" name="6a" id="6a" value="Y" <?= strtolower($receive_data['6a']) == 'y' ? 'checked' : ''; ?>>
                                                            <label for="6a">Ya</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio">
                                                            <input type="radio" name="6a" id="6a2" value="N" <?= strtolower($receive_data['6a']) == 'n' ? 'checked' : ''; ?>>
                                                            <label for="6a2">Tidak</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                            </ul>
                                            <?= form_error('6a', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>

                                        <div class="col-md-6">
                                            <label>b. Pastikan ventilasi terlihat, jika ada maka tidak boleh ditutup atau dihilangkan.</label>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <ul class="list-unstyled mb-0">
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio">
                                                            <input type="radio" name="6b" id="6b" value="Y" <?= strtolower($receive_data['6b']) == 'y' ? 'checked' : ''; ?>>
                                                            <label for="6b">Ya</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio">
                                                            <input type="radio" name="6b" id="6b2" value="N" <?= strtolower($receive_data['6b']) == 'n' ? 'checked' : ''; ?>>
                                                            <label for="6b2">Tidak</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                            </ul>
                                            <?= form_error('6b', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>

                                        <div class="col-md-6">
                                            <label>c. Pastikan tidak ada benda lain yang menempel pada kontainer</label>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <ul class="list-unstyled mb-0">
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio">
                                                            <input type="radio" name="6c" id="6c" value="Y" <?= strtolower($receive_data['6c']) == 'y' ? 'checked' : ''; ?>>
                                                            <label for="6c">Ya</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio">
                                                            <input type="radio" name="6c" id="6c2" value="N" <?= strtolower($receive_data['6c']) == 'n' ? 'checked' : ''; ?>>
                                                            <label for="6c2">Tidak</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                            </ul>
                                            <?= form_error('6c', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>



                                        <!-- Bagian 7 -->
                                        <!-- 7. Bagian lantai (Floor) -->
                                        <div class="col-md-12 mt-4 mb-2">
                                            <h4 class="card-title">7. Bagian lantai<small class="text-muted"> (Floor)</small></h4>
                                        </div>

                                        <div class="col-md-6">
                                            <label>a. Pastikan lantai kontainer dalam keadaan rata</label>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <ul class="list-unstyled mb-0">
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio">
                                                            <input type="radio" name="7a" id="7a" value="Y" <?= strtolower($receive_data['7a']) == 'y' ? 'checked' : ''; ?>>
                                                            <label for="7a">Ya</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio">
                                                            <input type="radio" name="7a" id="7a2" value="N" <?= strtolower($receive_data['7a']) == 'n' ? 'checked' : ''; ?>>
                                                            <label for="7a2">Tidak</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                            </ul>
                                            <?= form_error('7a', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>

                                        <div class="col-md-6">
                                            <label>b. Pastikan lantai dalam keadaan seragam.</label>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <ul class="list-unstyled mb-0">
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio">
                                                            <input type="radio" name="7b" id="7b" value="Y" <?= strtolower($receive_data['7b']) == 'y' ? 'checked' : ''; ?>>
                                                            <label for="7b">Ya</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio">
                                                            <input type="radio" name="7b" id="7b2" value="N" <?= strtolower($receive_data['7b']) == 'n' ? 'checked' : ''; ?>>
                                                            <label for="7b2">Tidak</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                            </ul>
                                            <?= form_error('7b', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>

                                        <div class="col-md-6">
                                            <label>c. Lihat jika ada perbaikan tidak biasa pada lantai</label>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <ul class="list-unstyled mb-0">
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio">
                                                            <input type="radio" name="7c" id="7c" value="Y" <?= strtolower($receive_data['7c']) == 'y' ? 'checked' : ''; ?>>
                                                            <label for="7c">Ya</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                                <li class="d-inline-block mr-2 mb-1">
                                                    <fieldset>
                                                        <div class="radio">
                                                            <input type="radio" name="7c" id="7c2" value="N" <?= strtolower($receive_data['7c']) == 'n' ? 'checked' : ''; ?>>
                                                            <label for="7c2">Tidak</label>
                                                        </div>
                                                    </fieldset>
                                                </li>
                                            </ul>
                                            <?= form_error('7c', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>

                                        <div class="col-md-6 mb-2">
                                            <label>Catatan :</label>
                                            <input type="text" id="catatan" class="form-control" name="catatan" placeholder="catatan" value="<?= $receive_data['catatan']; ?>">
                                            <?= form_error('catatan', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>

                                    </div>
                                </div>


                                <!-- <?= strtolower($receive_data['kebocoran_kendaraan']) == 'bocor' ? 'checked' : ''; ?>  -->



                                <div class="text-center mb-4">
                                    <small class="text-danger"><br /> **Saya telah melakukan pemeriksaan secara visual kondisi kontainer/box yang disebutkan di atas.
                                        Saya yang bertanda tangan mengkonfirmasi bahwa kontainer memiliki struktur yang kuat, kedap
                                        cuaca, tidak memiliki kompartemen palsu, dan mekanisme penguncian berada dalam keadaan
                                        baik dan terjamin dan tidak menunjukkan tanda-tanda yang dapat dirusak**</small>
                                </div>







                                <input type="hidden" id="id" class="form-control" name="id" placeholder="id" value=<?= $receive_data['id']; ?>>

                                <div class="col-sm-12 d-flex justify-content-end mt-2">
                                    <button type="submit" class="btn btn-primary mr-1">Approve Inspection Checklist</button>
                                    <button type="reset" class="btn btn-light-secondary">Reset</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
        </div>
    </div>
    </section>
    <!-- Basic Horizontal form layout section end -->
</div>
</div>
</div>


<!-- <script type="text/javascript">
    $( "#datepicker" ).datepicker();      
</script> -->