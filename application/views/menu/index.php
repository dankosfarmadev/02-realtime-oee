<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">



            <!-- Zero configuration table -->
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title"><?= $title; ?></h4>
                            </div>
                            <div class="card-body card-dashboard">
                                <!-- <p class="card-text">
                                        DataTables has most features enabled by default, so all you need
                                        to do to use it with your own tables is to call the construction
                                        function: $().DataTable();.
                                    </p> -->
                                <a href="" class="btn btn-primary mb-3" data-toggle="modal" data-target="#newMenuModal">Add New Menu</a>
                                <div class="btn-group">
                                    <div class="dropdown">
                                        <button class="btn btn-primary dropdown-toggle mb-3" type="button" id="dropdownMenuButtonIcon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="bx bx-error-circle mr-50"></i> Export File
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButtonIcon">
                                            <a class="dropdown-item" href="<?= base_url('menu/menuXlsx'); ?>"><i class="bx bx-file mr-50"></i> xlsx </a>
                                            <!-- <a class="dropdown-item" href="javascript:void(0);" target="_blank"><i class="bx bxs-file-pdf mr-50"></i> PDF </a> -->
                                            <!-- <a class="dropdown-item" href="javascript:void(0);"><i class="bx bx-time mr-50"></i> Option 3</a> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive">

                                    <?= form_error('menu', '<div class="alert alert-danger" role="alert">', '</div>'); ?>

                                    <?= $this->session->flashdata('message'); ?>

                                    <table class="table zero-configuration">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Menu</th>
                                                <th>Update Date</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i = 1; ?>
                                            <?php foreach ($menu as $m) : ?>
                                                <tr>
                                                    <th scope="row"><?= $i; ?></th>
                                                    <td><?= $m['menu']; ?></td>
                                                    <td><?= date('d-m-Y H:i:s', strtotime($m['update_date'])); ?></td>
                                                    <td>
                                                        <a href="<?= base_url('menu/menuEdit/') . $m['id']; ?>" class="badge badge-success">edit</a>
                                                        <!-- <a href="" class="badge badge-danger">delete</a> -->
                                                        <!-- <a href="" class="badge badge-danger" data-toggle="modal" data-target="#newMenuModalEdit">Edit Menu</a> -->
                                                    </td>
                                                </tr>
                                                <?php $i++; ?>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->


        </div>
    </div>
</div>
<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->

<!-- Modal -->
<div class="modal fade" id="newMenuModal" tabindex="-1" role="dialog" aria-labelledby="newMenuModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newMenuModalLabel">Add New Menu</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('menu'); ?>" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <input type="text" class="form-control" id="menu" name="menu" placeholder="Menu name">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Add</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Edit-->
<!-- <div class="modal fade" id="newMenuModalEdit" tabindex="-1" role="dialog" aria-labelledby="newMenuModalLabelEdit" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newMenuModalLabel">Edit Menu</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('menu'); ?>" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <input type="text" class="form-control" id="menu" name="menu" placeholder="Menu name">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Add</button>
                </div>
            </form>
        </div>
    </div>
</div> -->