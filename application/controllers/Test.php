<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Test extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('cookie');
        $this->load->model('Admin_model');
        $this->load->model('Mailer_model');
        // is_logged_in();
    }

    public function index()
    {
    }

    public function invoice()
    {
        $data['title'] = 'Invoice';
        $data['user'] = $this->db->get_where('user', ['nik' => $this->session->userdata('nik')])->row_array();

        $data['test'] = $this->db->get_where('user')->result_array();


        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        // $this->load->view('templates/topbar', $data);
        // $this->load->view('templates/sidebar', $data);
        $this->load->view('test/testInvoice', $data);
        $this->load->view('templates/footer');
    }

    public function export($id)
    {
        $this->Mailer_model->exportPDF($id);
    }

    public function exportInspection($id)
    {
        $this->Mailer_model->exportPDFInspection($id);
    }
}
