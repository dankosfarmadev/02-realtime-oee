<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;

class Oee extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url', 'download'));
        $this->load->library('form_validation');
        $this->load->helper('cookie');
        $this->load->model('Admin_model');
        $this->load->model('Oee_model');
        $this->load->model('Mailer_model');
        $this->load->model('Feature_model');
        $this->load->model('Oee_model');
        is_logged_in();
    }

    public function index()
    {
    }

    public function format_oee()
    {
        force_download('application/views/file/OEE_Format.xlsx', NULL);
    }

    public function bulk_delete()
    {
        $data = array();

        // If record delete request is submitted
        if ($this->input->post('bulk_delete_submit')) {
            // Get all selected IDs
            $ids = $this->input->post('checked_id');

            // If id array is not empty
            if (!empty($ids)) {
                // Delete records from the database
                $delete = $this->Oee_model->delete($ids);

                // If delete is successful
                if ($delete) {
                    $data['statusMsg'] = 'Selected records have been deleted successfully.';
                    $this->session->set_flashdata('message', '<div class="alert alert-warning">Successfully deleted.</div>');
                } else {
                    $data['statusMsg'] = 'Some problem occurred, please try again.';
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">Some problem occurred, please try again.</div>');
                }
            } else {
                $data['statusMsg'] = 'Select at least 1 record to delete.';
            }
        }

        // Get user data from the database
        $data['users'] = $this->Oee_model->getRows();

        // Pass the data to view
        // $this->load->view('realtime-oee', $data);
        // $this->table();
        // redirect('');
        // redirect($_SERVER['REQUEST_URI'], 'refresh');
        // redirect($this->uri->uri_string());



        $data['user'] = $this->db->get_where('user_app', ['nik' => $this->session->userdata('nik')])->row_array();

        // echo "<pre>";
        // print_r($data['user']['nik']);
        // echo "</pre>";


        // $data['role'] = $this->Oee_model->oee_table($data['user']['nik']);
        redirect('oee/line_0' . $data['user']['nik']);
        // $this->load->view('admin/oee/oee_table_per_line', $data);
    }

    public function tableau()
    {
        $data['title'] = 'OEE Tableau';
        $data['user'] = $this->db->get_where('user_app', ['nik' => $this->session->userdata('nik')])->row_array();

        $this->form_validation->set_rules('role', 'Role', 'required|trim');

        if ($this->form_validation->run() == false) {
            // Ganti disini
            // $data['role'] = $this->Admin_model->oee_table();
            $data['role'] = $this->Oee_model->tableau('oee');


            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('admin/oee/oee_tableau', $data);
            $this->load->view('templates/footer');
        }
    }

    public function line_01()
    {
        $data['title'] = 'Line 01';
        $data['user'] = $this->db->get_where('user_app', ['nik' => $this->session->userdata('nik')])->row_array();

        $this->form_validation->set_rules('role', 'Role', 'required|trim');

        if ($this->form_validation->run() == false) {
            if ($data['user']['name'] == 'Line 01' || $data['user']['name'] == 'MSTD') {
                // Ganti disini
                // $data['role'] = $this->Admin_model->oee_table();
                $data['role'] = $this->Oee_model->oee_table('1');


                $this->load->view('templates/header', $data);
                $this->load->view('templates/sidebar', $data);
                $this->load->view('templates/topbar', $data);
                $this->load->view('admin/oee/oee_line', $data);
                $this->load->view('templates/footer');
            } else {
                $this->load->view('404/myNotAuthorized'); //loading in custom error view
            }
        }
    }

    public function table()
    {

        $judul = $this->uri->segment('3');
        $judul = substr($judul, -2);

        $data['title'] = 'Line ' . $judul;
        $data['user'] = $this->db->get_where('user_app', ['nik' => $this->session->userdata('nik')])->row_array();

        $this->form_validation->set_rules('role', 'Role', 'required|trim');

        if ($this->form_validation->run() == false) {
            // if ($data['user']['name'] == 'Line 01' || $data['user']['name'] == 'MSTD') {
            // Ganti disini
            // $data['role'] = $this->Admin_model->oee_table();
            $data['role'] = $this->Oee_model->oee_table($judul);


            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('admin/oee/oee_table_per_line', $data);
            $this->load->view('templates/footer');
            // } else {
            // $this->load->view('404/myNotAuthorized'); //loading in custom error view
            // }
        }
    }

    public function line_02()
    {
        $data['title'] = 'Line 02';
        $data['user'] = $this->db->get_where('user_app', ['nik' => $this->session->userdata('nik')])->row_array();

        $this->form_validation->set_rules('role', 'Role', 'required|trim');

        if ($this->form_validation->run() == false) {
            if ($data['user']['name'] == 'Line 02' || $data['user']['name'] == 'MSTD') {
                # code...
                $data['role'] = $this->Oee_model->oee_table(2);

                $this->load->view('templates/header', $data);
                $this->load->view('templates/sidebar', $data);
                $this->load->view('templates/topbar', $data);
                $this->load->view('admin/oee/oee_line', $data);
                $this->load->view('templates/footer');
            } else {
                $this->load->view('404/myNotAuthorized'); //loading in custom error view
            }
        }
    }

    public function line_03()
    {
        $data['title'] = 'Line 03';
        $data['user'] = $this->db->get_where('user_app', ['nik' => $this->session->userdata('nik')])->row_array();

        $this->form_validation->set_rules('role', 'Role', 'required|trim');

        if ($this->form_validation->run() == false) {
            if ($data['user']['name'] == 'Line 03' || $data['user']['name'] == 'MSTD') {
                # code...
                $data['role'] = $this->Oee_model->oee_table(3);
                $this->load->view('templates/header', $data);
                $this->load->view('templates/sidebar', $data);
                $this->load->view('templates/topbar', $data);
                $this->load->view('admin/oee/oee_line', $data);
                $this->load->view('templates/footer');
            } else {
                $this->load->view('404/myNotAuthorized'); //loading in custom error view
            }
        }
    }

    public function line_04()
    {
        $data['title'] = 'Line 04';
        $data['user'] = $this->db->get_where('user_app', ['nik' => $this->session->userdata('nik')])->row_array();

        $this->form_validation->set_rules('role', 'Role', 'required|trim');

        if ($this->form_validation->run() == false) {
            if ($data['user']['name'] == 'Line 04' || $data['user']['name'] == 'MSTD') {
                # code...
                $data['role'] = $this->Oee_model->oee_table(4);
                $this->load->view('templates/header', $data);
                $this->load->view('templates/sidebar', $data);
                $this->load->view('templates/topbar', $data);
                $this->load->view('admin/oee/oee_line', $data);
                $this->load->view('templates/footer');
            } else {
                $this->load->view('404/myNotAuthorized'); //loading in custom error view
            }
        }
    }

    public function line_05()
    {
        $data['title'] = 'Line 05';
        $data['user'] = $this->db->get_where('user_app', ['nik' => $this->session->userdata('nik')])->row_array();

        $this->form_validation->set_rules('role', 'Role', 'required|trim');

        if ($this->form_validation->run() == false) {
            if ($data['user']['name'] == 'Line 05' || $data['user']['name'] == 'MSTD') {
                # code...
                $data['role'] = $this->Oee_model->oee_table(5);
                $this->load->view('templates/header', $data);
                $this->load->view('templates/sidebar', $data);
                $this->load->view('templates/topbar', $data);
                $this->load->view('admin/oee/oee_line', $data);
                $this->load->view('templates/footer');
            } else {
                $this->load->view('404/myNotAuthorized'); //loading in custom error view
            }
        }
    }

    public function line_07()
    {
        $data['title'] = 'Line 07';
        $data['user'] = $this->db->get_where('user_app', ['nik' => $this->session->userdata('nik')])->row_array();

        $this->form_validation->set_rules('role', 'Role', 'required|trim');

        if ($this->form_validation->run() == false) {
            if ($data['user']['name'] == 'Line 07' || $data['user']['name'] == 'MSTD') {
                # code...
                $data['role'] = $this->Oee_model->oee_table(7);
                $this->load->view('templates/header', $data);
                $this->load->view('templates/sidebar', $data);
                $this->load->view('templates/topbar', $data);
                $this->load->view('admin/oee/oee_line', $data);
                $this->load->view('templates/footer');
            } else {
                $this->load->view('404/myNotAuthorized'); //loading in custom error view
            }
        }
    }


    public function spreadhseet_format_download()
    {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="hello_world.xlsx"');
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'S.No');
        $sheet->setCellValue('B1', 'Product Name');
        $sheet->setCellValue('C1', 'Quantity');
        $sheet->setCellValue('D1', 'Price');

        $writer = new Xlsx($spreadsheet);
        $writer->save("php://output");
    }
    public function spreadsheet_import()
    {
        $line['data'] = $this->cek_line($this->session->userdata('name'));
        // var_dump($line['data']);
        // die;
        $upload_file = $_FILES['upload_file']['name'];
        $extension = pathinfo($upload_file, PATHINFO_EXTENSION);
        if ($extension == 'csv') {
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
        } else if ($extension == 'xls') {
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        } else {
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        }
        $spreadsheet = $reader->load($_FILES['upload_file']['tmp_name']);
        $sheetdata = $spreadsheet->getActiveSheet()->toArray();
        $sheetcount = count($sheetdata);

        //bener
        if ($sheetcount > 1) {
            $data = array();
            for ($i = 1; $i < $sheetcount; $i++) {
                $jenis_mesin = $sheetdata[$i][0];
                $shift = $sheetdata[$i][1];
                $masuk_libur = $sheetdata[$i][2];
                $tanggal = $sheetdata[$i][3];
                $type = $sheetdata[$i][4];
                $description = $sheetdata[$i][5];
                $value = $sheetdata[$i][6];
                $operator = $sheetdata[$i][7];
                $data[] = array(
                    'jenis_mesin' => strval($jenis_mesin),
                    'shift' => strval($shift),
                    'masuk_libur' => strval($masuk_libur),
                    'tanggal' => $tanggal,
                    'type' => strval($type),
                    'description' => strval($description),
                    'value' => intval($value),
                    'operator' => strval($operator),
                    'line' => intval($line['data'])
                );
            }
            $inserdata = $this->Admin_model->insert_batch($data);

            // intval($line);
            // strval($line);

            //coba
            // try {
            //     //code...
            //     for ($i = 1; $i < $sheetcount; $i++) {
            //         $jenis_mesin = $sheetdata[$i][0];
            //         $shift = $sheetdata[$i][1];
            //         $masuk_libur = $sheetdata[$i][2];
            //         $tanggal = $sheetdata[$i][3];
            //         $type = $sheetdata[$i][4];
            //         $description = $sheetdata[$i][5];
            //         $value = $sheetdata[$i][6];
            //         $operator = $sheetdata[$i][7];
            //         $result = $this->db->insert('oee', [
            //             'jenis_mesin' => $jenis_mesin,
            //             'shift' => $shift,
            //             'masuk_libur' => $masuk_libur,
            //             'tanggal' => $tanggal,
            //             'type' => $type,
            //             'description' => $description,
            //             'value' => $value,
            //             'operator' => $operator,
            //             'line' => $line['data']
            //         ]);
            //     }
            //     $this->session->set_flashdata('message', '<div class="alert alert-success">Successfully Added.</div>');
            // } catch (\Throwable $th) {
            //     throw $th;
            //     $this->session->set_flashdata('message', '<div class="alert alert-danger">Data Not uploaded. Please Try Again.</div>');
            // }





            // bener 

            if ($inserdata) {
                $this->session->set_flashdata('message', '<div class="alert alert-success">Successfully Added.</div>');
                // redirect('oee/line_0' . $line['data']);
                // echo '<pre>';
                // print_r($sheetdata);
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">Data Not uploaded. Please Try Again.</div>');
                // redirect('oee/line_0' . $line['data']);
                // echo '<pre>';
                // print_r($sheetdata);
            }

            if ($line['data'] == 0) {
                redirect('admin/oee');
            } else {
                redirect('oee/line_0' . $line['data']);
            }
        }
    }
    public function spreadsheet_export()
    {
        //fetch my data
        $productlist = $this->Admin_model->product_list();

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="product.xlsx"');
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'S.No');
        $sheet->setCellValue('B1', 'Product Name');
        $sheet->setCellValue('C1', 'Quantity');
        $sheet->setCellValue('D1', 'Price');
        $sheet->setCellValue('E1', 'Subtotal');

        $sn = 2;
        foreach ($productlist as $prod) {
            //echo $prod->product_name;
            $sheet->setCellValue('A' . $sn, $prod->product_id);
            $sheet->setCellValue('B' . $sn, $prod->product_name);
            $sheet->setCellValue('C' . $sn, $prod->product_quantity);
            $sheet->setCellValue('D' . $sn, $prod->product_price);
            $sheet->setCellValue('E' . $sn, '=C' . $sn . '*D' . $sn);
            $sn++;
        }
        //TOTAL
        $sheet->setCellValue('D8', 'Total');
        $sheet->setCellValue('E8', '=SUM(E2:E' . ($sn - 1) . ')');

        $writer = new Xlsx($spreadsheet);
        $writer->save("php://output");
    }

    public function cek_line($line)
    {
        if ($line == 'Line 01') {
            return 1;
        }
        if ($line == 'Line 02') {
            return 2;
        }
        if ($line == 'Line 03') {
            return 3;
        }
        if ($line == 'Line 04') {
            return 4;
        }
        if ($line == 'Line 05') {
            return 5;
        }
        if ($line == 'Line 07') {
            return 7;
        } else {
            return 0;
        }
    }
}
