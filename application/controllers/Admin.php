<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;

class Admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('cookie');
        $this->load->model('Admin_model');
        $this->load->model('Mailer_model');
        $this->load->model('Feature_model');
        is_logged_in();
    }

    public function index()
    {
        $data['title'] = 'Dashboard';
        $data['user'] = $this->db->get_where('user_app', ['nik' => $this->session->userdata('nik')])->row_array();

        $data['test'] = $this->db->get_where('user_app')->result_array();


        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);

        // $this->load->view('admin/index', $data);
        $this->load->view('admin/template_awal', $data);

        $this->load->view('templates/footer');

        // var_dump(get_cookie('remember_me_email'), get_cookie('remember_me_pw'));die;

    }

    public function log()
    {

        $data['title'] = 'Audit Trail';
        $data['user'] = $this->db->get_where('user_app', ['nik' => $this->session->userdata('nik')])->row_array();
        $data['receive'] = $this->Feature_model->pull_log();
        // var_dump($data['receive']);
        // die;

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/audit_trail/table', $data);
        $this->load->view('templates/footer');
    }

    public function role()
    {
        $data['title'] = 'Role';
        $data['user'] = $this->db->get_where('user_app', ['nik' => $this->session->userdata('nik')])->row_array();

        $data['role'] = $this->db->get('user_role')->result_array();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/role', $data);
        $this->load->view('templates/footer');
    }

    public function roleAdd()
    {
        $data['title'] = 'Role';
        $data['user'] = $this->db->get_where('user_app', ['nik' => $this->session->userdata('nik')])->row_array();
        $data['role'] = $this->db->get('user_role')->result_array();

        $this->form_validation->set_rules('role', 'Role', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('admin/role', $data);
            $this->load->view('templates/footer');
        } else {

            $data = [
                'role' => htmlspecialchars($this->input->post('role', true)),
                'create_by' => $this->session->userdata('id'),
                'update_by' => $this->session->userdata('id'),
            ];

            $this->db->insert('user_role', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Congratulation! Role has been created.</div>');
            redirect('admin/role');
        }
    }

    public function roleAccess($role_id)
    {
        $data['title'] = 'Role Access';
        $data['user'] = $this->db->get_where('user_app', ['nik' => $this->session->userdata('nik')])->row_array();

        $data['role'] = $this->db->get_where('user_role', ['id' => $role_id])->row_array();

        $this->db->where('id !=', 1);
        $data['menu'] = $this->db->get('user_menu')->result_array();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/role-access', $data);
        $this->load->view('templates/footer');
    }


    public function changeAccess()
    {
        $menu_id = $this->input->post('menuId');
        $role_id = $this->input->post('roleId');

        $data = [
            'role_id' => $role_id,
            'menu_id' => $menu_id
        ];

        $result = $this->db->get_where('user_access_menu', $data);

        if ($result->num_rows() < 1) {
            $this->db->insert('user_access_menu', $data);
        } else {
            $this->db->delete('user_access_menu', $data);
        }

        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Access Changed!</div>');
    }

    public function roleEdit($role_id)
    {
        $data['title'] = 'Role Edit';
        $data['user'] = $this->db->get_where('user_app', ['nik' => $this->session->userdata('nik')])->row_array();

        $data['role'] = $this->db->get_where('user_role', ['id' => $role_id])->row_array();


        $this->form_validation->set_rules('role', 'Role Name', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('admin/roleEdit', $data);
            $this->load->view('templates/footer');
        } else {
            $role = $this->input->post('role', true);
            $id = $this->input->post('id', true);
            // echo $role;
            // echo $role_id;
            $this->db->set('role', $role);
            $this->db->where('id', $id);
            $this->db->update('user_role');

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Name role has been updated!</div>');
            redirect('admin/role');
        }
    }

    // Submenu
    public function worker()
    {
        $data['title'] = 'Worker';
        // Wajib setiap controller/halaman baru
        $data['user'] = $this->db->get_where('user_app', ['nik' => $this->session->userdata('nik')])->row_array();

        // tampung di var worker, pilih model yg tadi sebagai alias, narik fungsi apa?

        // Admin_model dari halaman model yg sudah define diatas
        $data['list_departemen'] = $this->Admin_model->getAllDept();
        $data['worker_data'] = $this->Admin_model->getWorker();


        // Bisa ditaro disini codingan editnya cok
        if ($this->uri->segment(3) == 'edit') {
            # edit
            if ($this->form_validation->run() == false) {
                $data['worker_data_specific'] = $this->Admin_model->getSpecificWorker($this->uri->segment(4));

                $this->load->view('templates/header', $data);
                $this->load->view('templates/sidebar', $data);
                $this->load->view('templates/topbar', $data);
                $this->load->view('admin/worker/edit', $data);
                $this->load->view('templates/footer');
            }
        }
        // Bisa ditaro disini codingan editnya cok
        if ($this->uri->segment(3) == 'add') {
            # edit
            if ($this->form_validation->run() == false) {
                // $data['worker_data_specific'] = $this->Admin_model->getSpecificWorker($this->uri->segment(4));

                $this->load->view('templates/header', $data);
                $this->load->view('templates/sidebar', $data);
                $this->load->view('templates/topbar', $data);
                $this->load->view('admin/worker/add', $data);
                $this->load->view('templates/footer');
            }
        } else {
            // $data['role'] = $this->db->get_where('user', [
            //     'role_id !=' => 1
            // ])->result_array();

            // cara narik data dari Model #penting

            // param(nama_mode, alias)
            // $this->load->model('Admin_model', 'worker');
            // tampung di var worker, pilih model yg tadi sebagai alias, narik fungsi apa?
            $data['worker_data'] = $this->Admin_model->getWorker();

            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('admin/worker', $data);
            $this->load->view('templates/footer');
        }
    }

    public function workerAdd()
    {
        $data['title'] = 'Worker';
        $data['user'] = $this->db->get_where('user_app', ['nik' => $this->session->userdata('nik')])->row_array();
        $data['list_departemen'] = $this->Admin_model->getAllDept();
        $data['list_role'] = $this->Admin_model->getAllRole();
        $data['worker_data'] = $this->Admin_model->getWorker();

        // Penjagaa field
        // Untuk jagain form input
        $this->form_validation->set_rules('name', 'Name', 'required|trim');
        $this->form_validation->set_rules('nik', 'Nik', 'required|trim|numeric|is_unique[user_app.nik]', [
            'numeric' => 'This NIK field must contain only numbers',
            'is_unique' => 'This NIK has already registered!'
        ]);
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
        $this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[3]|matches[password2]', [
            'matches' => 'Password dont match!',
            'min_length' => 'Password too short!'
        ]);
        $this->form_validation->set_rules('password2', 'Password', 'required|trim|matches[password1]');
        $this->form_validation->set_rules('departement_id', 'Departement', 'required');

        if ($this->form_validation->run() == false) {
            // $data['worker_data_specific'] = $this->Admin_model->getSpecificWorker($this->uri->segment(4));

            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('admin/worker/add', $data);
            $this->load->view('templates/footer');
        } else {
            // do query
            $active = $this->input->post('active');
            $active = $active == null ? 0 : 1;
            $email = $this->input->post('email', true);

            $data = [
                'name' => htmlspecialchars($this->input->post('name', true)),
                'email' => htmlspecialchars($email),
                'nik' => htmlspecialchars($this->input->post('nik', true)),
                'image' => 'default.jpg',
                'password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
                'role_id' => 2,
                'is_active' => $active, //bisa dijadiin 0 karena nanti spv nya yg approve
                // 'date_created' => time(),
                'departement_id' => $this->input->post('departement_id'),

            ];

            $this->db->insert('user_app', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Congratulation! your account has been created. Please activate your account</div>');
            redirect('admin/worker');
        }
    }

    public function workerEdit($id)
    {
        $data['title'] = 'Worker';
        $data['user'] = $this->db->get_where('user_app', ['nik' => $this->session->userdata('nik')])->row_array();
        $data['list_departemen'] = $this->Admin_model->getAllDept();
        $data['list_role'] = $this->Admin_model->getAllRole();
        $data['worker_data'] = $this->Admin_model->getWorker();

        // penjagaan edit
        $this->form_validation->set_rules('name', 'Name', 'required|trim');
        // $this->form_validation->set_rules('nik', 'Nik', 'required|trim|numeric|is_unique[user_app.nik]', [
        //     'numeric' => 'This NIK field must contain only numbers',
        //     'is_unique' => 'This NIK has already registered!'
        // ]);
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
        // $this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[3]|matches[password2]', [
        //     'matches' => 'Password dont match!',
        //     'min_length' => 'Password too short!'
        // ]);
        // $this->form_validation->set_rules('password2', 'Password', 'required|trim|matches[password1]');
        // $this->form_validation->set_rules('departement_id', 'Departement', 'required');


        if ($this->form_validation->run() == false) {
            $data['worker_data_specific'] = $this->Admin_model->getSpecificWorker($this->uri->segment(3));

            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('admin/worker/edit', $data);
            $this->load->view('templates/footer');
        } else {
            $active = $this->input->post('active');
            if ($active == null) {
                $active = 0;
            } else {
                $active = 1;
            }
            $id = $this->input->post('id', true);

            $data = array(
                'name' => $this->input->post('name', true),
                'nik' => $this->input->post('nik', true),
                'email' => $this->input->post('email', true),
                'is_active' => $active,
                'departement_id' => $this->input->post('departement_id', true),
                'role_id' => $this->input->post('role_id', true),
                'update_by' => $this->session->userdata('id'),
            );
            // 'password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),

            // $this->db->where('id', $id);
            // $this->db->update('user', $data);
            $data['query'] = $this->Admin_model->workerUpdate($id, $data);
            $this->Feature_model->do_log(strval($this->db->last_query()));

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Worker has been updated!</div>');
            redirect('admin/worker');
        }
    }


    public function workerResetPass($id)
    {
        $data['title'] = 'Worker';
        $data['user'] = $this->db->get_where('user_app', ['nik' => $this->session->userdata('nik')])->row_array();

        $data = array(
            'password' => password_hash('onekalbe21', PASSWORD_DEFAULT),
            'update_by' => $this->session->userdata('id'),
        );
        $data['query'] = $this->Admin_model->workerUpdate($id, $data);
        $this->Feature_model->do_log(strval($this->db->last_query()));


        $this->session->set_flashdata('message', '<div class="alert alert-warning" role="alert">Worker password has been reset!</div>');
        redirect('admin/worker');
    }

    // soon query editnya
    public function edit()
    {
        // Untuk jagain form input
        $this->form_validation->set_rules('name', 'Name', 'required|trim');
        $this->form_validation->set_rules('nik', 'Nik', 'required|trim|numeric|is_unique[user_app.nik]', [
            'numeric' => 'This NIK field must contain only numbers',
            'is_unique' => 'This NIK has already registered!'
        ]);
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');


        if ($this->form_validation->run() == false) {
            $active = $this->input->post('active');
            if ($active == null) {
                $active = 0;
            } else {
                $active = 1;
            }
            $id = $this->input->post('id', true);

            $data = array(
                'name' => $this->input->post('name', true),
                'nik' => $this->input->post('nik', true),
                'email' => $this->input->post('email', true),
                'is_active' => $active,
                'update_by' => $this->session->userdata('id')
            );

            $this->db->where('id', $id);
            $this->db->update('user', $data);
            $data['query'] = $this->Admin_model->workerUpdate($id, $data);
            $this->Feature_model->do_log(strval($this->db->last_query()));


            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Worker has been updated!</div>');
            redirect('admin/worker');
        } else {



            // $active = $this->input->post('active');
            // if ($active == null) {
            //     $active = 0;
            // } else {
            //     $active = 1;
            // }
            // $id = $this->input->post('id', true);

            // $data = array(
            //     'name' => $this->input->post('name', true),
            //     'nik' => $this->input->post('nik', true),
            //     'email' => $this->input->post('email', true),
            //     'is_active' => $active,
            //     'update_by' => $this->session->userdata('id')
            // );

            // $this->db->where('id', $id);
            // $this->db->update('user', $data);
            // $data['query'] = $this->Admin_model->workerUpdate($id, $data);

            // $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Worker has been updated!</div>');
            // redirect('admin/worker');
        }
    }


    public function testingEmail()
    {
        // var_dump('wlwl');
        // die;
        $type = 'verify';
        $config = [
            'protocol'  => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_user' => 'dankosfarmadev@gmail.com',
            'smtp_pass' => 'Dankosfarma1',
            'smtp_port' => 465,
            'mailtype'  => 'html',
            'charset'   => 'utf-8',
            'newline'   => "\r\n"
        ];

        $this->email->initialize($config);

        $this->email->from('dankosfarmadev@gmail.com', 'Testing Kirim Email');
        $this->email->to('ndusetwn@gmail.com, ndusetwn22@gmail.com');
        $this->email->cc('pandu.setiawan@dankosfarma.com');
        // $this->email->to($this->input->post('email'));

        if ($type == 'verify') {
            $this->email->subject('Account Verification');
            // $this->email->message('Click this link to verify you account : <a href="' . base_url() . 'auth/verify?email=' . $this->input->post('email') . '&token=' . urlencode($token) . '">Activate</a>');
        } else if ($type == 'forgot') {
            $this->email->subject('Reset Password');
            // $this->email->message('Click this link to reset your password : <a href="' . base_url() . 'auth/resetpassword?email=' . $this->input->post('email') . '&token=' . urlencode($token) . '">Reset Password</a>');
        }

        if ($this->email->send()) {
            echo 'success';
            die;
            // return true;
        } else {
            echo 'gagal';
            echo $this->email->print_debugger();
            die;
        }

        // $this->load->view('templates/header');
        // $this->load->view('templates/sidebar');
        // $this->load->view('templates/topbar');
        // $this->load->view('admin/worker/edit');
        // $this->load->view('templates/footer');
    }

    public function test()
    {
        $this->Mailer_model->sendMail('Dankos Farma Dev by MSTD', 'ndusetwn@gmail.com', 'ndusetwn16@gmail.com, pandu.setiawan@dankosfarma.com', 'verify', '[Notification]', 'Message Email', 'admin');
    }





    public function oee()
    {
        $data['title'] = 'OEE';
        $data['user'] = $this->db->get_where('user_app', ['nik' => $this->session->userdata('nik')])->row_array();

        // $data['role'] = $this->db->get('user_role')->result_array();
        $data['role'] = $this->Admin_model->oee_table();


        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/oee/oee_table', $data);
        $this->load->view('templates/footer');
    }


    public function spreadhseet_format_download()
    {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="hello_world.xlsx"');
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'S.No');
        $sheet->setCellValue('B1', 'Product Name');
        $sheet->setCellValue('C1', 'Quantity');
        $sheet->setCellValue('D1', 'Price');

        $writer = new Xlsx($spreadsheet);
        $writer->save("php://output");
    }
    public function spreadsheet_import()
    {
        $upload_file = $_FILES['upload_file']['name'];
        $extension = pathinfo($upload_file, PATHINFO_EXTENSION);
        if ($extension == 'csv') {
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
        } else if ($extension == 'xls') {
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        } else {
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        }
        $spreadsheet = $reader->load($_FILES['upload_file']['tmp_name']);
        $sheetdata = $spreadsheet->getActiveSheet()->toArray();
        $sheetcount = count($sheetdata);
        if ($sheetcount > 1) {
            $data = array();
            for ($i = 1; $i < $sheetcount; $i++) {
                $jenis_mesin = $sheetdata[$i][0];
                $shift = $sheetdata[$i][1];
                $masuk_libur = $sheetdata[$i][2];
                $tanggal = $sheetdata[$i][3];
                $type = $sheetdata[$i][4];
                $description = $sheetdata[$i][5];
                $value = $sheetdata[$i][6];
                $operator = $sheetdata[$i][7];
                $data[] = array(
                    'jenis_mesin' => strval($jenis_mesin),
                    'shift' => strval($shift),
                    'masuk_libur' => strval($masuk_libur),
                    'tanggal' => $tanggal,
                    'type' => strval($type),
                    'description' => strval($description),
                    'value' => intval($value),
                    'operator' => strval($operator),
                );
            }

            // 'jenis_mesin' => strval($jenis_mesin),
            // 'shift' => strval($shift),
            // 'masuk_libur' => strval($masuk_libur),
            // 'tanggal' => $tanggal,
            // 'type' => strval($type),
            // 'description' => strval($description),
            // 'value' => intval($value),
            // 'operator' => strval($operator),
            // 'line' => intval($line['data'])

            $inserdata = $this->Admin_model->insert_batch($data);
            if ($inserdata) {
                $this->session->set_flashdata('message', '<div class="alert alert-success">Successfully Added.</div>');
                redirect('admin/oee');
                // echo '<pre>';
                // print_r($sheetdata);
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">Data Not uploaded. Please Try Again.</div>');
                redirect('admin/oee');
                // echo '<pre>';
                // print_r($sheetdata);
            }
        }
    }
    public function spreadsheet_export()
    {
        //fetch my data
        $productlist = $this->Admin_model->product_list();

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="product.xlsx"');
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'S.No');
        $sheet->setCellValue('B1', 'Product Name');
        $sheet->setCellValue('C1', 'Quantity');
        $sheet->setCellValue('D1', 'Price');
        $sheet->setCellValue('E1', 'Subtotal');

        $sn = 2;
        foreach ($productlist as $prod) {
            //echo $prod->product_name;
            $sheet->setCellValue('A' . $sn, $prod->product_id);
            $sheet->setCellValue('B' . $sn, $prod->product_name);
            $sheet->setCellValue('C' . $sn, $prod->product_quantity);
            $sheet->setCellValue('D' . $sn, $prod->product_price);
            $sheet->setCellValue('E' . $sn, '=C' . $sn . '*D' . $sn);
            $sn++;
        }
        //TOTAL
        $sheet->setCellValue('D8', 'Total');
        $sheet->setCellValue('E8', '=SUM(E2:E' . ($sn - 1) . ')');

        $writer = new Xlsx($spreadsheet);
        $writer->save("php://output");
    }
}
