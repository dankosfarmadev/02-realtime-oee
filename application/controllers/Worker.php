<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Worker extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('image_lib');

        $this->load->helper('cookie');
        $this->load->model('Worker_model');
        $this->load->model('Feature_model');
        $this->load->model('Mailer_model');

        is_logged_in();
    }

    public function index()
    {
    }

    public function receive()
    {
        $data['title'] = 'Receive';
        $data['user'] = $this->db->get_where('user', ['nik' => $this->session->userdata('nik')])->row_array();
        $data['receive'] = $this->Worker_model->getReceive();
        // var_dump($data['receive']);
        // die;

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('role/logistic_worker/receive/table', $data);
        $this->load->view('templates/footer');
    }

    public function receiveAdd()
    {
        $data['title'] = 'Receive';
        $data['user'] = $this->db->get_where('user', ['nik' => $this->session->userdata('nik')])->row_array();

        $this->form_validation->set_rules('dataPicker', 'Tanggal', 'required|trim');
        $this->form_validation->set_rules('dist_cust', 'Distibutor or customer', 'required|trim');
        $this->form_validation->set_rules('ekspedisi', 'Ekspedisi', 'required|trim');
        $this->form_validation->set_rules('no_polisi', 'No polisi', 'required|trim');
        $this->form_validation->set_rules('suhu_awal', 'Suhu awal', 'required|trim|numeric');
        $this->form_validation->set_rules('suhu_setelah', 'Suhu setelah', 'required|trim|numeric');
        $this->form_validation->set_rules('jam_awal', 'Jam awal', 'required|trim');
        $this->form_validation->set_rules('jam_setelah', 'Jam setelah', 'required|trim');

        $this->form_validation->set_rules('kebersihan', 'Kebersihan', 'required|trim');
        $this->form_validation->set_rules('bunyi', 'Bunyi', 'required|trim');
        $this->form_validation->set_rules('asap', 'Asap Kendaraan', 'required|trim');
        $this->form_validation->set_rules('bau', 'Bau', 'required|trim');
        $this->form_validation->set_rules('kebocoran', 'Kebocoran', 'required|trim');
        $this->form_validation->set_rules('emisi', 'Emisi', 'required|trim');
        $this->form_validation->set_rules('oli', 'Oli', 'required|trim');

        $this->form_validation->set_rules('nama_ekspedisi', 'Nama pengirim', 'required|trim');

        $this->form_validation->set_rules('catatan', 'Catatan', 'required|trim');
        $this->form_validation->set_rules('segel', 'Segel', 'required|trim');
        // $this->form_validation->set_rules('image', 'Foto pengirim', 'required|trim');


        if ($this->form_validation->run() == false) {
            // $data['worker_data_specific'] = $this->Admin_model->getSpecificWorker($this->uri->segment(4));

            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('role/logistic_worker/receive/add', $data);
            $this->load->view('templates/footer');
        } else {
            // do query
            // var_dump($this->input->post('dataPicker', true));
            // die;

            // cek jika ada gambar yang akan diupload
            $upload_image = $_FILES['image']['name'];
            // echo $upload_image;

            if ($upload_image) {
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size']      = '2048';
                $config['upload_path'] = './assets/img/foto_ekspedisi/';

                $this->load->library('upload', $config);

                if ($old = $this->upload->do_upload('image')) {
                    $new_image2 = $this->upload->data('file_name');
                    $this->image_lib->initialize(array(
                        'image_library' => 'gd2', //library yang kita gunakan
                        'source_image' => './assets/img/foto_ekspedisi/' . $new_image2,
                        // 'new_image' => './assets/img/thumb/' . $new_image2,
                        'maintain_ratio' => FALSE,
                        'create_thumb' => TRUE,
                        'width' => 300,
                        'height' => 250
                    ));

                    $this->image_lib->resize();

                    $foto_ekspedisi = $new_image2;
                    $file_info = pathinfo($new_image2);
                    // $foto_ekspedisi = $file_info['dirname'] . '/' . $file_info['filename'] . '_thumb.' . $file_info['extension'];
                    $foto_ekspedisi = $file_info['filename'] . '_thumb.' . $file_info['extension'];
                    unlink(FCPATH . 'assets/img/foto_ekspedisi/' . $new_image2);
                } else {
                    echo $this->upload->dispay_errors();
                }
            }

            $jenis_form = 'receive';

            $data = [
                'jenis_form' => $jenis_form,
                'tanggal' => htmlspecialchars($this->input->post('dataPicker', true)),
                'distributor_customer' => htmlspecialchars($this->input->post('dist_cust', true)),
                'ekspedisi' => htmlspecialchars($this->input->post('ekspedisi', true)),
                'no_polisi' => htmlspecialchars($this->input->post('no_polisi', true)),
                'suhu_awal' => htmlspecialchars($this->input->post('suhu_awal', true)),
                'suhu_setelah' => htmlspecialchars($this->input->post('suhu_setelah', true)),
                'jam_awal' => htmlspecialchars($this->input->post('jam_awal', true)),
                'jam_setelah' => htmlspecialchars($this->input->post('jam_setelah', true)),
                'kebersihan_kendaraan' => htmlspecialchars($this->input->post('kebersihan', true)),
                'bunyi_mesin_kendaraan' => htmlspecialchars($this->input->post('bunyi', true)),
                'asap_kendaraan' => htmlspecialchars($this->input->post('asap', true)),
                'bau_kendaraan' => htmlspecialchars($this->input->post('bau', true)),
                'kebocoran_kendaraan' => htmlspecialchars($this->input->post('kebocoran', true)),
                'emisi_kendaraan' => htmlspecialchars($this->input->post('emisi', true)),
                'oli_kendaraan' => htmlspecialchars($this->input->post('oli', true)),

                'catatan' => htmlspecialchars($this->input->post('catatan', true)),
                'segel' => htmlspecialchars($this->input->post('segel', true)),
                'create_by' => $this->session->userdata('id'),
                'update_by' => $this->session->userdata('id'),
                'nama_ekspedisi' => htmlspecialchars($this->input->post('nama_ekspedisi', true)),
                'foto_ekspedisi' => $foto_ekspedisi,
                'status_form_rd_id' => '4'
            ];

            $this->db->insert('receive_delivery', $data);
            // gabisa pake log kalo ada insert id, karena akan menimpa dari last id yg diambil
            // $this->Feature_model->do_log(strval($this->db->last_query()));
            $insert_id = $this->db->insert_id();

            // return  $insert_id;
            // After insert harus returning ID nya dulu, baru insert lagi


            $bn = $this->input->post('bn');
            $lpn = $this->input->post('lpn');
            $qty_mb = $this->input->post('qty_mb');
            $qty_per_mb = $this->input->post('qty_per_mb');
            $qty_pick = $this->input->post('qty_pick');
            $pco = $this->input->post('pco');
            $qep = $this->input->post('qep');

            // echo "<pre>";
            // print_r($_POST);
            // echo "</pre>";
            // die;

            $data = array();
            for ($i = 0; $i < count($this->input->post('bn')); $i++) {
                $data[]  = array(
                    'batch_number' => $bn[$i],
                    'lpn_number' => $lpn[$i],
                    'qty_mb' => $qty_mb[$i],
                    'qty_per_mb' => $qty_per_mb[$i],
                    'qty_pick' => $qty_pick[$i],
                    'product_checklist_out' => $pco[$i],
                    'qty_epm_pallet' => $qep[$i],
                    'rd_id' => $insert_id,
                    'create_by' => $this->session->userdata('id'),
                    'update_by' => $this->session->userdata('id')
                );
            }
            $this->db->insert_batch('data_rd', $data);
            $this->Feature_model->do_log(strval($this->db->last_query()));

            // for ($i = 0; $i < count($bn); $i++) {
            //     $result = $this->db->insert('data_rd', [
            //         'batch_number' => $bn[$i],
            //         'lpn_number' => $lpn[$i],
            //         'qty_mb' => $qty_mb[$i],
            //         'qty_per_mb' => $qty_per_mb[$i],
            //         'qty_pick' => $qty_pick[$i],
            //         'product_checklist_out' => $pco[$i],
            //         'qty_epm_pallet' => $qep[$i],
            //         'rd_id' => 1,
            //         'create_by' => $this->session->userdata('id'),
            //         'update_by' => $this->session->userdata('id')
            //     ]);
            // }

            // public function sendMail($name, $to, $cc, $type, $subject, $message, $redirect)

            $message = "==================================" . "<br>" . "
                        You have new notification. " . date('d F Y') . "<br>" . "
                        Document: Worker create form " . $jenis_form . "<br>" . "
                        Date: " . date('d-m-Y', strtotime($this->input->post('dataPicker')))  . "<br>" . "
                        Distributor/Customer: " . $this->input->post('dist_cust') . "<br>" . "
                        Ekspedisi: " . $this->input->post('ekspedisi') . "<br>" . "
                        No Polisi: " . $this->input->post('no_polisi') . "<br>" . "
                        Check your account!\r\n" . "<br>" . "
                        ==================================" . "<br>" . "
                        ---- Do not reply this message ----";

            $this->Mailer_model->sendMail('Dankos Farma Dev by MSTD', 'dankosfarmadev@gmail.com', CC_EMAIL, 'verify', '[Notification]', $message, '');

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Document receive has been created!</div>');
            redirect('worker/receive');
            // echo $base64;
        }
    }

    public function receiveDetail($id)
    {
        $data['title'] = 'Receive';
        $data['user'] = $this->db->get_where('user', ['nik' => $this->session->userdata('nik')])->row_array();

        if ($this->form_validation->run() == false) {
            $data['receive_data'] = $this->Worker_model->getSpecificReceive($id);
            $data['rd_data'] = $this->Worker_model->getEveryBatchData($id);
            // var_dump($data['rd_data']);
            // die;


            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('role/logistic_worker/receive/detail', $data);
            $this->load->view('templates/footer');
        } else {

            $jenis_form = 'receive';

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Document receive has been created!</div>');
            redirect('worker/receive');
            // echo $base64;
        }
    }


    public function deletePath($path)
    {
        unlink(FCPATH . 'assets/img/foto_ekspedisi/' . $path);
    }

    public function delivery()
    {
        $data['title'] = 'Delivery';
        $data['user'] = $this->db->get_where('user', ['nik' => $this->session->userdata('nik')])->row_array();
        $data['receive'] = $this->Worker_model->getDelivery();
        // var_dump($data['receive']);
        // die;

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('role/logistic_worker/delivery/table', $data);
        $this->load->view('templates/footer');
    }

    public function deliveryAdd()
    {
        $data['title'] = 'Delivery';
        $data['user'] = $this->db->get_where('user', ['nik' => $this->session->userdata('nik')])->row_array();

        $this->form_validation->set_rules('dataPicker', 'Tanggal', 'required|trim');
        $this->form_validation->set_rules('dist_cust', 'Distibutor or customer', 'required|trim');
        $this->form_validation->set_rules('ekspedisi', 'Ekspedisi', 'required|trim');
        $this->form_validation->set_rules('no_polisi', 'No polisi', 'required|trim');
        $this->form_validation->set_rules('suhu_awal', 'Suhu awal', 'required|trim|numeric');
        $this->form_validation->set_rules('suhu_setelah', 'Suhu setelah', 'required|trim|numeric');
        $this->form_validation->set_rules('jam_awal', 'Jam awal', 'required|trim');
        $this->form_validation->set_rules('jam_setelah', 'Jam setelah', 'required|trim');

        $this->form_validation->set_rules('kebersihan', 'Kebersihan', 'required|trim');
        $this->form_validation->set_rules('bunyi', 'Bunyi', 'required|trim');
        $this->form_validation->set_rules('asap', 'Asap Kendaraan', 'required|trim');
        $this->form_validation->set_rules('bau', 'Bau', 'required|trim');
        $this->form_validation->set_rules('kebocoran', 'Kebocoran', 'required|trim');
        $this->form_validation->set_rules('emisi', 'Emisi', 'required|trim');
        $this->form_validation->set_rules('oli', 'Oli', 'required|trim');

        $this->form_validation->set_rules('nama_ekspedisi', 'Nama pengirim', 'required|trim');

        $this->form_validation->set_rules('catatan', 'Catatan', 'required|trim');
        $this->form_validation->set_rules('segel', 'Segel', 'required|trim');
        // $this->form_validation->set_rules('image', 'Foto pengirim', 'required');




        // $this->form_validation->set_rules('nik', 'Nik', 'required|trim|numeric|is_unique[user.nik]', [
        //     'numeric' => 'This NIK field must contain only numbers',
        //     'is_unique' => 'This NIK has already registered!'
        // ]);
        // $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
        // $this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[3]|matches[password2]', [
        //     'matches' => 'Password dont match!',
        //     'min_length' => 'Password too short!'
        // ]);
        // $this->form_validation->set_rules('password2', 'Password', 'required|trim|matches[password1]');
        // $this->form_validation->set_rules('departement_id', 'Departement', 'required');

        if ($this->form_validation->run() == false) {
            // $data['worker_data_specific'] = $this->Admin_model->getSpecificWorker($this->uri->segment(4));

            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('role/logistic_worker/delivery/add', $data);
            $this->load->view('templates/footer');
        } else {
            // do query
            // var_dump($this->input->post('dataPicker', true));
            // die;
            // $this->form_validation->set_rules('image', 'Foto', 'callback_file_check');

            // cek jika ada gambar yang akan diupload
            $upload_image = $_FILES['image']['name'];
            // echo $upload_image;

            if ($upload_image) {
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size']      = '2048';
                $config['upload_path'] = './assets/img/foto_ekspedisi/';

                $this->load->library('upload', $config);

                if ($old = $this->upload->do_upload('image')) {
                    $new_image2 = $this->upload->data('file_name');
                    $this->image_lib->initialize(array(
                        'image_library' => 'gd2', //library yang kita gunakan
                        'source_image' => './assets/img/foto_ekspedisi/' . $new_image2,
                        // 'new_image' => './assets/img/thumb/' . $new_image2,
                        'maintain_ratio' => FALSE,
                        'create_thumb' => TRUE,
                        'width' => 300,
                        'height' => 250
                    ));

                    $this->image_lib->resize();

                    $foto_ekspedisi = $new_image2;
                    $file_info = pathinfo($new_image2);
                    // $foto_ekspedisi = $file_info['dirname'] . '/' . $file_info['filename'] . '_thumb.' . $file_info['extension'];
                    $foto_ekspedisi = $file_info['filename'] . '_thumb.' . $file_info['extension'];
                    unlink(FCPATH . 'assets/img/foto_ekspedisi/' . $new_image2);
                } else {
                    echo $this->upload->dispay_errors();
                }
            }

            $jenis_form = 'delivery';

            $data = [
                'jenis_form' => $jenis_form,
                'tanggal' => htmlspecialchars($this->input->post('dataPicker', true)),
                'distributor_customer' => htmlspecialchars($this->input->post('dist_cust', true)),
                'ekspedisi' => htmlspecialchars($this->input->post('ekspedisi', true)),
                'no_polisi' => htmlspecialchars($this->input->post('no_polisi', true)),
                'suhu_awal' => htmlspecialchars($this->input->post('suhu_awal', true)),
                'suhu_setelah' => htmlspecialchars($this->input->post('suhu_setelah', true)),
                'jam_awal' => htmlspecialchars($this->input->post('jam_awal', true)),
                'jam_setelah' => htmlspecialchars($this->input->post('jam_setelah', true)),
                'kebersihan_kendaraan' => htmlspecialchars($this->input->post('kebersihan', true)),
                'bunyi_mesin_kendaraan' => htmlspecialchars($this->input->post('bunyi', true)),
                'asap_kendaraan' => htmlspecialchars($this->input->post('asap', true)),
                'bau_kendaraan' => htmlspecialchars($this->input->post('bau', true)),
                'kebocoran_kendaraan' => htmlspecialchars($this->input->post('kebocoran', true)),
                'emisi_kendaraan' => htmlspecialchars($this->input->post('emisi', true)),
                'oli_kendaraan' => htmlspecialchars($this->input->post('oli', true)),

                'catatan' => htmlspecialchars($this->input->post('catatan', true)),
                'segel' => htmlspecialchars($this->input->post('segel', true)),
                'create_by' => $this->session->userdata('id'),
                'update_by' => $this->session->userdata('id'),
                'nama_ekspedisi' => htmlspecialchars($this->input->post('nama_ekspedisi', true)),
                'foto_ekspedisi' => $foto_ekspedisi,
                'status_form_rd_id' => '4'
            ];

            $this->db->insert('receive_delivery', $data);
            // gabisa pake log kalo ada insert id, karena akan menimpa dari last id yg diambil
            // $this->Feature_model->do_log(strval($this->db->last_query()));
            $insert_id = $this->db->insert_id();

            // return  $insert_id;
            // After insert harus returning ID nya dulu, baru insert lagi


            $bn = $this->input->post('bn');
            // var_dump($bn); die;
            $lpn = $this->input->post('lpn');
            $qty_mb = $this->input->post('qty_mb');
            $qty_per_mb = $this->input->post('qty_per_mb');
            $qty_pick = $this->input->post('qty_pick');
            $pco = $this->input->post('pco');
            $qep = $this->input->post('qep');

            // echo "<pre>";
            // print_r($_POST);
            // echo "</pre>";
            // die;

            $data = array();
            for ($i = 0; $i < count($this->input->post('bn')); $i++) {
                $data[]  = array(
                    'batch_number' => $bn[$i],
                    'lpn_number' => $lpn[$i],
                    'qty_mb' => $qty_mb[$i],
                    'qty_per_mb' => $qty_per_mb[$i],
                    'qty_pick' => $qty_pick[$i],
                    'product_checklist_out' => $pco[$i],
                    'qty_epm_pallet' => $qep[$i],
                    'rd_id' => $insert_id,
                    'create_by' => $this->session->userdata('id'),
                    'update_by' => $this->session->userdata('id')
                );
            }
            $this->db->insert_batch('data_rd', $data);
            $this->Feature_model->do_log(strval($this->db->last_query()));

            // for ($i = 0; $i < count($bn); $i++) {
            //     $result = $this->db->insert('data_rd', [
            //         'batch_number' => $bn[$i],
            //         'lpn_number' => $lpn[$i],
            //         'qty_mb' => $qty_mb[$i],
            //         'qty_per_mb' => $qty_per_mb[$i],
            //         'qty_pick' => $qty_pick[$i],
            //         'product_checklist_out' => $pco[$i],
            //         'qty_epm_pallet' => $qep[$i],
            //         'rd_id' => 1,
            //         'create_by' => $this->session->userdata('id'),
            //         'update_by' => $this->session->userdata('id')
            //     ]);
            // }

            // public function sendMail($name, $to, $cc, $type, $subject, $message, $redirect)

            $message = "==================================" . "<br>" . "
                        You have new notification. " . date('d F Y') . "<br>" . "
                        Document: Worker create form " . $jenis_form . "<br>" . "
                        Date: " . date('d-m-Y', strtotime($this->input->post('dataPicker')))  . "<br>" . "
                        Distributor/Customer: " . $this->input->post('dist_cust') . "<br>" . "
                        Ekspedisi: " . $this->input->post('ekspedisi') . "<br>" . "
                        No Polisi: " . $this->input->post('no_polisi') . "<br>" . "
                        Check your account!\r\n" . "<br>" . "
                        ==================================" . "<br>" . "
                        ---- Do not reply this message ----";

            $this->Mailer_model->sendMail('Dankos Farma Dev by MSTD', 'dankosfarmadev@gmail.com', CC_EMAIL, 'verify', '[Notification]', $message, '');

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Document delivery has been created!</div>');
            redirect('worker/delivery');
            // echo $base64;
        }
    }

    public function deliveryDetail($id)
    {
        $data['title'] = 'Delivery';
        $data['user'] = $this->db->get_where('user', ['nik' => $this->session->userdata('nik')])->row_array();

        if ($this->form_validation->run() == false) {
            $data['receive_data'] = $this->Worker_model->getSpecificDelivery($id);
            $data['rd_data'] = $this->Worker_model->getEveryBatchData($id);


            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('role/logistic_worker/delivery/detail', $data);
            $this->load->view('templates/footer');
        } else {

            $jenis_form = 'delivery';

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Document delivery has been created!</div>');
            redirect('worker/delivery');
            // echo $base64;
        }
    }



    public function delivery_export()
    {
        $data['title'] = 'Delivery Export';
        $data['user'] = $this->db->get_where('user', ['nik' => $this->session->userdata('nik')])->row_array();
        $data['receive'] = $this->Worker_model->getDeliveryExport();
        // var_dump($data['receive']);
        // die;

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('role/logistic_worker/delivery_export/table', $data);
        $this->load->view('templates/footer');
    }

    public function deliveryAdd_export()
    {
        $data['title'] = 'Delivery Export';
        $data['user'] = $this->db->get_where('user', ['nik' => $this->session->userdata('nik')])->row_array();

        $this->form_validation->set_rules('dataPicker', 'Tanggal', 'required|trim');
        $this->form_validation->set_rules('dist_cust', 'Distibutor or customer', 'required|trim');
        $this->form_validation->set_rules('ekspedisi', 'Ekspedisi', 'required|trim');
        $this->form_validation->set_rules('no_polisi', 'No polisi', 'required|trim');
        $this->form_validation->set_rules('suhu_awal', 'Suhu awal', 'required|trim|numeric');
        $this->form_validation->set_rules('suhu_setelah', 'Suhu setelah', 'required|trim|numeric');
        $this->form_validation->set_rules('jam_awal', 'Jam awal', 'required|trim');
        $this->form_validation->set_rules('jam_setelah', 'Jam setelah', 'required|trim');

        $this->form_validation->set_rules('kebersihan', 'Kebersihan', 'required|trim');
        $this->form_validation->set_rules('bunyi', 'Bunyi', 'required|trim');
        $this->form_validation->set_rules('asap', 'Asap Kendaraan', 'required|trim');
        $this->form_validation->set_rules('bau', 'Bau', 'required|trim');
        $this->form_validation->set_rules('kebocoran', 'Kebocoran', 'required|trim');
        $this->form_validation->set_rules('emisi', 'Emisi', 'required|trim');
        $this->form_validation->set_rules('oli', 'Oli', 'required|trim');

        $this->form_validation->set_rules('nama_ekspedisi', 'Nama pengirim', 'required|trim');

        $this->form_validation->set_rules('catatan', 'Catatan', 'required|trim');
        $this->form_validation->set_rules('segel', 'Segel', 'required|trim');

        $this->form_validation->set_rules('no_polisi2', 'No Polisi', 'required|trim');

        $this->form_validation->set_rules('1a', 'No 1a', 'required|trim');
        $this->form_validation->set_rules('1b', 'No 1b', 'required|trim');
        $this->form_validation->set_rules('1c', 'No 1c', 'required|trim');

        $this->form_validation->set_rules('2a', 'No 2a', 'required|trim');
        $this->form_validation->set_rules('2b', 'No 2b', 'required|trim');
        $this->form_validation->set_rules('2c', 'No 2c', 'required|trim');

        $this->form_validation->set_rules('3a', 'No 3a', 'required|trim');
        $this->form_validation->set_rules('3b', 'No 3b', 'required|trim');

        $this->form_validation->set_rules('4a', 'No 4a', 'required|trim');
        $this->form_validation->set_rules('4b', 'No 4b', 'required|trim');

        $this->form_validation->set_rules('5a', 'No 5a', 'required|trim');
        $this->form_validation->set_rules('5b', 'No 5b', 'required|trim');
        $this->form_validation->set_rules('5c', 'No 5c', 'required|trim');

        $this->form_validation->set_rules('6a', 'No 6a', 'required|trim');
        $this->form_validation->set_rules('6b', 'No 6b', 'required|trim');
        $this->form_validation->set_rules('6c', 'No 6c', 'required|trim');

        $this->form_validation->set_rules('7a', 'No 7a', 'required|trim');
        $this->form_validation->set_rules('7b', 'No 7b', 'required|trim');
        $this->form_validation->set_rules('7c', 'No 7c', 'required|trim');



        // $this->form_validation->set_rules('image', 'Foto pengirim', 'required');




        // $this->form_validation->set_rules('nik', 'Nik', 'required|trim|numeric|is_unique[user.nik]', [
        //     'numeric' => 'This NIK field must contain only numbers',
        //     'is_unique' => 'This NIK has already registered!'
        // ]);
        // $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
        // $this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[3]|matches[password2]', [
        //     'matches' => 'Password dont match!',
        //     'min_length' => 'Password too short!'
        // ]);
        // $this->form_validation->set_rules('password2', 'Password', 'required|trim|matches[password1]');
        // $this->form_validation->set_rules('departement_id', 'Departement', 'required');

        if ($this->form_validation->run() == false) {
            // $data['worker_data_specific'] = $this->Admin_model->getSpecificWorker($this->uri->segment(4));

            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('role/logistic_worker/delivery_export/add', $data);
            $this->load->view('templates/footer');
        } else {
            // do query
            // var_dump($this->input->post('dataPicker', true));
            // die;
            // $this->form_validation->set_rules('image', 'Foto', 'callback_file_check');

            // cek jika ada gambar yang akan diupload
            $upload_image = $_FILES['image']['name'];
            // echo $upload_image;

            if ($upload_image) {
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size']      = '2048';
                $config['upload_path'] = './assets/img/foto_ekspedisi/';

                $this->load->library('upload', $config);

                if ($old = $this->upload->do_upload('image')) {
                    $new_image2 = $this->upload->data('file_name');
                    $this->image_lib->initialize(array(
                        'image_library' => 'gd2', //library yang kita gunakan
                        'source_image' => './assets/img/foto_ekspedisi/' . $new_image2,
                        // 'new_image' => './assets/img/thumb/' . $new_image2,
                        'maintain_ratio' => FALSE,
                        'create_thumb' => TRUE,
                        'width' => 300,
                        'height' => 250
                    ));

                    $this->image_lib->resize();

                    $foto_ekspedisi = $new_image2;
                    $file_info = pathinfo($new_image2);
                    // $foto_ekspedisi = $file_info['dirname'] . '/' . $file_info['filename'] . '_thumb.' . $file_info['extension'];
                    $foto_ekspedisi = $file_info['filename'] . '_thumb.' . $file_info['extension'];
                    unlink(FCPATH . 'assets/img/foto_ekspedisi/' . $new_image2);
                } else {
                    echo $this->upload->dispay_errors();
                }
            }

            $jenis_form = 'delivery_export';

            $data = [
                'jenis_form' => $jenis_form,
                'tanggal' => htmlspecialchars($this->input->post('dataPicker', true)),
                'distributor_customer' => htmlspecialchars($this->input->post('dist_cust', true)),
                'ekspedisi' => htmlspecialchars($this->input->post('ekspedisi', true)),
                'no_polisi' => htmlspecialchars($this->input->post('no_polisi', true)),
                'suhu_awal' => htmlspecialchars($this->input->post('suhu_awal', true)),
                'suhu_setelah' => htmlspecialchars($this->input->post('suhu_setelah', true)),
                'jam_awal' => htmlspecialchars($this->input->post('jam_awal', true)),
                'jam_setelah' => htmlspecialchars($this->input->post('jam_setelah', true)),
                'kebersihan_kendaraan' => htmlspecialchars($this->input->post('kebersihan', true)),
                'bunyi_mesin_kendaraan' => htmlspecialchars($this->input->post('bunyi', true)),
                'asap_kendaraan' => htmlspecialchars($this->input->post('asap', true)),
                'bau_kendaraan' => htmlspecialchars($this->input->post('bau', true)),
                'kebocoran_kendaraan' => htmlspecialchars($this->input->post('kebocoran', true)),
                'emisi_kendaraan' => htmlspecialchars($this->input->post('emisi', true)),
                'oli_kendaraan' => htmlspecialchars($this->input->post('oli', true)),

                'catatan' => htmlspecialchars($this->input->post('catatan', true)),
                'segel' => htmlspecialchars($this->input->post('segel', true)),
                'create_by' => $this->session->userdata('id'),
                'update_by' => $this->session->userdata('id'),
                'nama_ekspedisi' => htmlspecialchars($this->input->post('nama_ekspedisi', true)),
                'foto_ekspedisi' => $foto_ekspedisi,
                'status_form_rd_id' => '4'
            ];

            $this->db->insert('receive_delivery', $data);
            // gabisa pake log kalo ada insert id, karena akan menimpa dari last id yg diambil
            // $this->Feature_model->do_log(strval($this->db->last_query()));
            $insert_id = $this->db->insert_id();

            // return  $insert_id;
            // After insert harus returning ID nya dulu, baru insert lagi


            $bn = $this->input->post('bn');
            // var_dump($bn); die;
            $lpn = $this->input->post('lpn');
            $qty_mb = $this->input->post('qty_mb');
            $qty_per_mb = $this->input->post('qty_per_mb');
            $qty_pick = $this->input->post('qty_pick');
            $pco = $this->input->post('pco');
            $qep = $this->input->post('qep');

            $data = array();
            for ($i = 0; $i < count($this->input->post('bn')); $i++) {
                $data[]  = array(
                    'batch_number' => $bn[$i],
                    'lpn_number' => $lpn[$i],
                    'qty_mb' => $qty_mb[$i],
                    'qty_per_mb' => $qty_per_mb[$i],
                    'qty_pick' => $qty_pick[$i],
                    'product_checklist_out' => $pco[$i],
                    'qty_epm_pallet' => $qep[$i],
                    'rd_id' => $insert_id,
                    'create_by' => $this->session->userdata('id'),
                    'update_by' => $this->session->userdata('id')
                );
            }
            $this->db->insert_batch('data_rd', $data);
            $this->Feature_model->do_log(strval($this->db->last_query()));

            $data = [
                '1a' => htmlspecialchars($this->input->post('1a', true)),
                '1b' => htmlspecialchars($this->input->post('1b', true)),
                '1c' => htmlspecialchars($this->input->post('1c', true)),
                '2a' => htmlspecialchars($this->input->post('2a', true)),
                '2b' => htmlspecialchars($this->input->post('2b', true)),
                '2c' => htmlspecialchars($this->input->post('2c', true)),
                '3a' => htmlspecialchars($this->input->post('3a', true)),
                '3b' => htmlspecialchars($this->input->post('3b', true)),
                '4a' => htmlspecialchars($this->input->post('4a', true)),
                '4b' => htmlspecialchars($this->input->post('4b', true)),
                '5a' => htmlspecialchars($this->input->post('5a', true)),
                '5b' => htmlspecialchars($this->input->post('5b', true)),
                '5c' => htmlspecialchars($this->input->post('5c', true)),
                '6a' => htmlspecialchars($this->input->post('6a', true)),
                '6b' => htmlspecialchars($this->input->post('6b', true)),
                '6c' => htmlspecialchars($this->input->post('6c', true)),
                '7a' => htmlspecialchars($this->input->post('7a', true)),
                '7b' => htmlspecialchars($this->input->post('7b', true)),
                '7c' => htmlspecialchars($this->input->post('7c', true)),
                'no_polisi' => htmlspecialchars($this->input->post('no_polisi2', true)),
                'catatan' => htmlspecialchars($this->input->post('catatan2', true)),
                'create_by' => $this->session->userdata('id'),
                'update_by' => $this->session->userdata('id'),
                'rd_id' => $insert_id
            ];

            $this->db->insert('inspection_checklist', $data);

            $message = "==================================" . "<br>" . "
                        You have new notification. " . date('d F Y') . "<br>" . "
                        Document: Worker create form " . $jenis_form . "<br>" . "
                        Date: " . date('d-m-Y', strtotime($this->input->post('dataPicker')))  . "<br>" . "
                        Distributor/Customer: " . $this->input->post('dist_cust') . "<br>" . "
                        Ekspedisi: " . $this->input->post('ekspedisi') . "<br>" . "
                        No Polisi: " . $this->input->post('no_polisi') . "<br>" . "
                        Check your account!\r\n" . "<br>" . "
                        ==================================" . "<br>" . "
                        ---- Do not reply this message ----";

            $this->Mailer_model->sendMail('Dankos Farma Dev by MSTD', 'dankosfarmadev@gmail.com', CC_EMAIL, 'verify', '[Notification]', $message, '');

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Document delivery export has been created!</div>');
            redirect('worker/delivery_export');
            // echo $base64;
        }
    }

    public function deliveryDetail_export($id)
    {
        $data['title'] = 'Delivery Export';
        $data['user'] = $this->db->get_where('user', ['nik' => $this->session->userdata('nik')])->row_array();

        if ($this->form_validation->run() == false) {
            $data['receive_data'] = $this->Worker_model->getSpecificDeliveryExport($id);
            $data['rd_data'] = $this->Worker_model->getEveryBatchData($id);
            $data['ins'] = $this->Worker_model->getSpecificInspectionExport($id);



            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('role/logistic_worker/delivery_export/detail', $data);
            $this->load->view('templates/footer');
        } else {

            $jenis_form = 'delivery_export';

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Document delivery export has been created!</div>');
            redirect('worker/delivery_export');
            // echo $base64;
        }
    }





    public function inspection()
    {
        $data['title'] = 'Inspection Checklist';
        $data['user'] = $this->db->get_where('user', ['nik' => $this->session->userdata('nik')])->row_array();
        $data['receive'] = $this->Worker_model->getInspection();
        // var_dump($data['receive']);
        // die;

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('role/logistic_worker/inspection_checklist/table', $data);
        $this->load->view('templates/footer');
    }

    public function inspectionAdd()
    {
        $data['title'] = 'Inspection Checklist';
        $data['user'] = $this->db->get_where('user', ['nik' => $this->session->userdata('nik')])->row_array();

        $this->form_validation->set_rules('no_polisi', 'No Polisi', 'required|trim');

        $this->form_validation->set_rules('1a', 'No 1a', 'required|trim');
        $this->form_validation->set_rules('1b', 'No 1b', 'required|trim');
        $this->form_validation->set_rules('1c', 'No 1c', 'required|trim');

        $this->form_validation->set_rules('2a', 'No 2a', 'required|trim');
        $this->form_validation->set_rules('2b', 'No 2b', 'required|trim');
        $this->form_validation->set_rules('2c', 'No 2c', 'required|trim');

        $this->form_validation->set_rules('3a', 'No 3a', 'required|trim');
        $this->form_validation->set_rules('3b', 'No 3b', 'required|trim');

        $this->form_validation->set_rules('4a', 'No 4a', 'required|trim');
        $this->form_validation->set_rules('4b', 'No 4b', 'required|trim');

        $this->form_validation->set_rules('5a', 'No 5a', 'required|trim');
        $this->form_validation->set_rules('5b', 'No 5b', 'required|trim');
        $this->form_validation->set_rules('5c', 'No 5c', 'required|trim');

        $this->form_validation->set_rules('6a', 'No 6a', 'required|trim');
        $this->form_validation->set_rules('6b', 'No 6b', 'required|trim');
        $this->form_validation->set_rules('6c', 'No 6c', 'required|trim');

        $this->form_validation->set_rules('7a', 'No 7a', 'required|trim');
        $this->form_validation->set_rules('7b', 'No 7b', 'required|trim');
        $this->form_validation->set_rules('7c', 'No 7c', 'required|trim');



        if ($this->form_validation->run() == false) {
            // $data['worker_data_specific'] = $this->Admin_model->getSpecificWorker($this->uri->segment(4));

            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('role/logistic_worker/inspection_checklist/add', $data);
            $this->load->view('templates/footer');
        } else {
            // do query
            // var_dump($this->input->post('dataPicker', true));
            // die;

            $data = [
                '1a' => htmlspecialchars($this->input->post('1a', true)),
                '1b' => htmlspecialchars($this->input->post('1b', true)),
                '1c' => htmlspecialchars($this->input->post('1c', true)),
                '2a' => htmlspecialchars($this->input->post('2a', true)),
                '2b' => htmlspecialchars($this->input->post('2b', true)),
                '2c' => htmlspecialchars($this->input->post('2c', true)),
                '3a' => htmlspecialchars($this->input->post('3a', true)),
                '3b' => htmlspecialchars($this->input->post('3b', true)),
                '4a' => htmlspecialchars($this->input->post('4a', true)),
                '4b' => htmlspecialchars($this->input->post('4b', true)),
                '5a' => htmlspecialchars($this->input->post('5a', true)),
                '5b' => htmlspecialchars($this->input->post('5b', true)),
                '5c' => htmlspecialchars($this->input->post('5c', true)),
                '6a' => htmlspecialchars($this->input->post('6a', true)),
                '6b' => htmlspecialchars($this->input->post('6b', true)),
                '6c' => htmlspecialchars($this->input->post('6c', true)),
                '7a' => htmlspecialchars($this->input->post('7a', true)),
                '7b' => htmlspecialchars($this->input->post('7b', true)),
                '7c' => htmlspecialchars($this->input->post('7c', true)),
                'no_polisi' => htmlspecialchars($this->input->post('no_polisi', true)),
                'catatan' => htmlspecialchars($this->input->post('catatan', true)),
                'create_by' => $this->session->userdata('id'),
                'update_by' => $this->session->userdata('id'),
                'status_form_inspection_id' => '4'
            ];

            $this->db->insert('inspection_checklist', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Document inspection checklist has been created!</div>');
            redirect('worker/inspection');
            // echo $base64;
        }
    }

    public function inspectionDetail($id)
    {
        $data['title'] = 'Inspection Checklist';
        $data['user'] = $this->db->get_where('user', ['nik' => $this->session->userdata('nik')])->row_array();

        if ($this->form_validation->run() == false) {
            $data['receive_data'] = $this->Worker_model->getSpecificInspection($id);


            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('role/logistic_worker/inspection_checklist/detail', $data);
            $this->load->view('templates/footer');
        } else {

            $jenis_form = 'delivery';

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Document delivery has been created!</div>');
            redirect('worker/inspection');
            // echo $base64;
        }
    }


    public function kirimUlangReceive($id)
    {
        $data['title'] = 'Receive';
        $data['user'] = $this->db->get_where('user', ['nik' => $this->session->userdata('nik')])->row_array();


        $this->form_validation->set_rules('dataPicker', 'Tanggal', 'required|trim');
        $this->form_validation->set_rules('dist_cust', 'Distibutor or customer', 'required|trim');
        $this->form_validation->set_rules('ekspedisi', 'Ekspedisi', 'required|trim');
        $this->form_validation->set_rules('no_polisi', 'No polisi', 'required|trim');
        $this->form_validation->set_rules('suhu_awal', 'Suhu awal', 'required|trim|numeric');
        $this->form_validation->set_rules('suhu_setelah', 'Suhu setelah', 'required|trim|numeric');
        $this->form_validation->set_rules('jam_awal', 'Jam awal', 'required|trim');
        $this->form_validation->set_rules('jam_setelah', 'Jam setelah', 'required|trim');

        $this->form_validation->set_rules('kebersihan', 'Kebersihan', 'required|trim');
        $this->form_validation->set_rules('bunyi', 'Bunyi', 'required|trim');
        $this->form_validation->set_rules('asap', 'Asap Kendaraan', 'required|trim');
        $this->form_validation->set_rules('bau', 'Bau', 'required|trim');
        $this->form_validation->set_rules('kebocoran', 'Kebocoran', 'required|trim');

        $this->form_validation->set_rules('catatan', 'Catatan', 'required|trim');
        $this->form_validation->set_rules('segel', 'Segel', 'required|trim');

        $this->form_validation->set_rules('emisi', 'Emisi', 'required|trim');
        $this->form_validation->set_rules('oli', 'Oli', 'required|trim');

        // $this->form_validation->set_rules('nama_ekspedisi', 'Nama pengirim', 'required|trim');

        if ($this->form_validation->run() == false) {
            $data['receive_data'] = $this->Worker_model->getSpecificReceive($id);
            $data['rd_data'] = $this->Worker_model->getEveryBatchData($id);


            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('role/logistic_coordinator/approval_doc/detail', $data);
            $this->load->view('templates/footer');
        } else {

            $data = [
                'tanggal' => htmlspecialchars($this->input->post('dataPicker', true)),
                'distributor_customer' => htmlspecialchars($this->input->post('dist_cust', true)),
                'ekspedisi' => htmlspecialchars($this->input->post('ekspedisi', true)),
                'no_polisi' => htmlspecialchars($this->input->post('no_polisi', true)),
                'suhu_awal' => htmlspecialchars($this->input->post('suhu_awal', true)),
                'suhu_setelah' => htmlspecialchars($this->input->post('suhu_setelah', true)),
                'jam_awal' => htmlspecialchars($this->input->post('jam_awal', true)),
                'jam_setelah' => htmlspecialchars($this->input->post('jam_setelah', true)),
                'kebersihan_kendaraan' => htmlspecialchars($this->input->post('kebersihan', true)),
                'bunyi_mesin_kendaraan' => htmlspecialchars($this->input->post('bunyi', true)),
                'asap_kendaraan' => htmlspecialchars($this->input->post('asap', true)),
                'bau_kendaraan' => htmlspecialchars($this->input->post('bau', true)),
                'kebocoran_kendaraan' => htmlspecialchars($this->input->post('kebocoran', true)),
                'emisi_kendaraan' => htmlspecialchars($this->input->post('emisi', true)),
                'oli_kendaraan' => htmlspecialchars($this->input->post('oli', true)),
                'catatan' => htmlspecialchars($this->input->post('catatan', true)),
                'segel' => htmlspecialchars($this->input->post('segel', true)),
                'update_by' => $this->session->userdata('id'),
                'status_form_rd_id' => '4'
            ];

            $this->db->where('id', $id);
            $this->db->update('receive_delivery', $data);
            $this->Feature_model->do_log(strval($this->db->last_query()));



            $bn = $this->input->post('bn');
            $lpn = $this->input->post('lpn');
            $qty_mb = $this->input->post('qty_mb');
            $qty_per_mb = $this->input->post('qty_per_mb');
            $qty_pick = $this->input->post('qty_pick');
            $pco = $this->input->post('pco');
            $qep = $this->input->post('qep');
            $data_rd_id = $this->input->post('data_rd_id');

            // $data = array();
            // for ($i = 0; $i < count($this->input->post('bn')); $i++) {
            //     $data[]  = array(
            //         'batch_number' => $bn[$i],
            //         'lpn_number' => $lpn[$i],
            //         'qty_mb' => $qty_mb[$i],
            //         'qty_per_mb' => $qty_per_mb[$i],
            //         'qty_pick' => $qty_pick[$i],
            //         'product_checklist_out' => $pco[$i],
            //         'qty_epm_pallet' => $qep[$i],
            //         // 'rd_id' => $insert_id,
            //         'update_by' => $this->session->userdata('id')
            //     );
            // }
            // $this->db->update_batch('data_rd', $data, 'id');
            // echo 'wkwk', $bn;

            if ($bn != 0) {
                # code...
                for ($i = 0; $i < count($bn); $i++) {
                    // $this->db->where('id', $data_rd_id[$i]);
                    $result = $this->db->update('data_rd', [
                        'batch_number' => $bn[$i],
                        'lpn_number' => $lpn[$i],
                        'qty_mb' => $qty_mb[$i],
                        'qty_per_mb' => $qty_per_mb[$i],
                        'qty_pick' => $qty_pick[$i],
                        'product_checklist_out' => $pco[$i],
                        'qty_epm_pallet' => $qep[$i],
                        'update_by' => $this->session->userdata('id')
                    ], array('id' => $data_rd_id[$i]));
                }
            }
            $this->Feature_model->do_log(strval($this->db->last_query()));

            // echo "<pre>";
            // print_r($_POST);
            // echo "</pre>";
            // die;


            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Document receive has been resend!</div>');
            redirect('worker/receive');
            // echo $base64;
        }
    }

    public function kirimUlangDelivery($id)
    {
        $data['title'] = 'Delivery';
        $data['user'] = $this->db->get_where('user', ['nik' => $this->session->userdata('nik')])->row_array();


        $this->form_validation->set_rules('dataPicker', 'Tanggal', 'required|trim');
        $this->form_validation->set_rules('dist_cust', 'Distibutor or customer', 'required|trim');
        $this->form_validation->set_rules('ekspedisi', 'Ekspedisi', 'required|trim');
        $this->form_validation->set_rules('no_polisi', 'No polisi', 'required|trim');
        $this->form_validation->set_rules('suhu_awal', 'Suhu awal', 'required|trim|numeric');
        $this->form_validation->set_rules('suhu_setelah', 'Suhu setelah', 'required|trim|numeric');
        $this->form_validation->set_rules('jam_awal', 'Jam awal', 'required|trim');
        $this->form_validation->set_rules('jam_setelah', 'Jam setelah', 'required|trim');

        $this->form_validation->set_rules('kebersihan', 'Kebersihan', 'required|trim');
        $this->form_validation->set_rules('bunyi', 'Bunyi', 'required|trim');
        $this->form_validation->set_rules('asap', 'Asap Kendaraan', 'required|trim');
        $this->form_validation->set_rules('bau', 'Bau', 'required|trim');
        $this->form_validation->set_rules('kebocoran', 'Kebocoran', 'required|trim');

        $this->form_validation->set_rules('catatan', 'Catatan', 'required|trim');
        $this->form_validation->set_rules('segel', 'Segel', 'required|trim');

        $this->form_validation->set_rules('emisi', 'Emisi', 'required|trim');
        $this->form_validation->set_rules('oli', 'Oli', 'required|trim');

        // $this->form_validation->set_rules('nama_ekspedisi', 'Nama pengirim', 'required|trim');

        if ($this->form_validation->run() == false) {
            $data['receive_data'] = $this->Worker_model->getSpecificReceive($id);
            $data['rd_data'] = $this->Worker_model->getEveryBatchData($id);


            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('role/logistic_coordinator/approval_doc/detail', $data);
            $this->load->view('templates/footer');
        } else {

            $data = [
                'tanggal' => htmlspecialchars($this->input->post('dataPicker', true)),
                'distributor_customer' => htmlspecialchars($this->input->post('dist_cust', true)),
                'ekspedisi' => htmlspecialchars($this->input->post('ekspedisi', true)),
                'no_polisi' => htmlspecialchars($this->input->post('no_polisi', true)),
                'suhu_awal' => htmlspecialchars($this->input->post('suhu_awal', true)),
                'suhu_setelah' => htmlspecialchars($this->input->post('suhu_setelah', true)),
                'jam_awal' => htmlspecialchars($this->input->post('jam_awal', true)),
                'jam_setelah' => htmlspecialchars($this->input->post('jam_setelah', true)),
                'kebersihan_kendaraan' => htmlspecialchars($this->input->post('kebersihan', true)),
                'bunyi_mesin_kendaraan' => htmlspecialchars($this->input->post('bunyi', true)),
                'asap_kendaraan' => htmlspecialchars($this->input->post('asap', true)),
                'bau_kendaraan' => htmlspecialchars($this->input->post('bau', true)),
                'kebocoran_kendaraan' => htmlspecialchars($this->input->post('kebocoran', true)),
                'emisi_kendaraan' => htmlspecialchars($this->input->post('emisi', true)),
                'oli_kendaraan' => htmlspecialchars($this->input->post('oli', true)),
                'catatan' => htmlspecialchars($this->input->post('catatan', true)),
                'segel' => htmlspecialchars($this->input->post('segel', true)),
                'update_by' => $this->session->userdata('id'),
                'status_form_rd_id' => '4'
            ];

            $this->db->where('id', $id);
            $this->db->update('receive_delivery', $data);
            $this->Feature_model->do_log(strval($this->db->last_query()));



            $bn = $this->input->post('bn');
            $lpn = $this->input->post('lpn');
            $qty_mb = $this->input->post('qty_mb');
            $qty_per_mb = $this->input->post('qty_per_mb');
            $qty_pick = $this->input->post('qty_pick');
            $pco = $this->input->post('pco');
            $qep = $this->input->post('qep');
            $data_rd_id = $this->input->post('data_rd_id');

            // $data = array();
            // for ($i = 0; $i < count($this->input->post('bn')); $i++) {
            //     $data[]  = array(
            //         'batch_number' => $bn[$i],
            //         'lpn_number' => $lpn[$i],
            //         'qty_mb' => $qty_mb[$i],
            //         'qty_per_mb' => $qty_per_mb[$i],
            //         'qty_pick' => $qty_pick[$i],
            //         'product_checklist_out' => $pco[$i],
            //         'qty_epm_pallet' => $qep[$i],
            //         // 'rd_id' => $insert_id,
            //         'update_by' => $this->session->userdata('id')
            //     );
            // }
            // $this->db->update_batch('data_rd', $data, 'id');
            // echo 'wkwk', $bn;

            if ($bn != 0) {
                # code...
                for ($i = 0; $i < count($bn); $i++) {
                    // $this->db->where('id', $data_rd_id[$i]);
                    $result = $this->db->update('data_rd', [
                        'batch_number' => $bn[$i],
                        'lpn_number' => $lpn[$i],
                        'qty_mb' => $qty_mb[$i],
                        'qty_per_mb' => $qty_per_mb[$i],
                        'qty_pick' => $qty_pick[$i],
                        'product_checklist_out' => $pco[$i],
                        'qty_epm_pallet' => $qep[$i],
                        'update_by' => $this->session->userdata('id')
                    ], array('id' => $data_rd_id[$i]));
                }
            }
            $this->Feature_model->do_log(strval($this->db->last_query()));

            // echo "<pre>";
            // print_r($_POST);
            // echo "</pre>";
            // die;


            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Document delivery has been resend!</div>');
            redirect('worker/delivery');
            // echo $base64;
        }
    }

    public function kirimUlangExport($id)
    {
        $data['title'] = 'Approval Delivery Export';
        $data['user'] = $this->db->get_where('user', ['nik' => $this->session->userdata('nik')])->row_array();


        $this->form_validation->set_rules('dataPicker', 'Tanggal', 'required|trim');
        $this->form_validation->set_rules('dist_cust', 'Distibutor or customer', 'required|trim');
        $this->form_validation->set_rules('ekspedisi', 'Ekspedisi', 'required|trim');
        $this->form_validation->set_rules('no_polisi', 'No polisi', 'required|trim');
        $this->form_validation->set_rules('suhu_awal', 'Suhu awal', 'required|trim|numeric');
        $this->form_validation->set_rules('suhu_setelah', 'Suhu setelah', 'required|trim|numeric');
        $this->form_validation->set_rules('jam_awal', 'Jam awal', 'required|trim');
        $this->form_validation->set_rules('jam_setelah', 'Jam setelah', 'required|trim');

        $this->form_validation->set_rules('kebersihan', 'Kebersihan', 'required|trim');
        $this->form_validation->set_rules('bunyi', 'Bunyi', 'required|trim');
        $this->form_validation->set_rules('asap', 'Asap Kendaraan', 'required|trim');
        $this->form_validation->set_rules('bau', 'Bau', 'required|trim');
        $this->form_validation->set_rules('kebocoran', 'Kebocoran', 'required|trim');

        $this->form_validation->set_rules('emisi', 'Emisi', 'required|trim');
        $this->form_validation->set_rules('oli', 'Oli', 'required|trim');

        $this->form_validation->set_rules('catatan', 'Catatan', 'required|trim');
        $this->form_validation->set_rules('segel', 'Segel', 'required|trim');

        // $this->form_validation->set_rules('nama_ekspedisi', 'Nama pengirim', 'required|trim');

        $this->form_validation->set_rules('no_polisi2', 'No Polisi', 'required|trim');

        $this->form_validation->set_rules('1a', 'No 1a', 'required|trim');
        $this->form_validation->set_rules('1b', 'No 1b', 'required|trim');
        $this->form_validation->set_rules('1c', 'No 1c', 'required|trim');

        $this->form_validation->set_rules('2a', 'No 2a', 'required|trim');
        $this->form_validation->set_rules('2b', 'No 2b', 'required|trim');
        $this->form_validation->set_rules('2c', 'No 2c', 'required|trim');

        $this->form_validation->set_rules('3a', 'No 3a', 'required|trim');
        $this->form_validation->set_rules('3b', 'No 3b', 'required|trim');

        $this->form_validation->set_rules('4a', 'No 4a', 'required|trim');
        $this->form_validation->set_rules('4b', 'No 4b', 'required|trim');

        $this->form_validation->set_rules('5a', 'No 5a', 'required|trim');
        $this->form_validation->set_rules('5b', 'No 5b', 'required|trim');
        $this->form_validation->set_rules('5c', 'No 5c', 'required|trim');

        $this->form_validation->set_rules('6a', 'No 6a', 'required|trim');
        $this->form_validation->set_rules('6b', 'No 6b', 'required|trim');
        $this->form_validation->set_rules('6c', 'No 6c', 'required|trim');

        $this->form_validation->set_rules('7a', 'No 7a', 'required|trim');
        $this->form_validation->set_rules('7b', 'No 7b', 'required|trim');
        $this->form_validation->set_rules('7c', 'No 7c', 'required|trim');


        if ($this->form_validation->run() == false) {
            $data['receive_data'] = $this->Worker_model->getSpecificDeliveryExport($id);
            $data['rd_data'] = $this->Worker_model->getEveryBatchData($id);
            $data['ins'] = $this->Worker_model->getSpecificInspectionExport($id);


            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('role/logistic_coordinator/approval_doc/detail', $data);
            $this->load->view('templates/footer');
        } else {

            $data = [
                'tanggal' => htmlspecialchars($this->input->post('dataPicker', true)),
                'distributor_customer' => htmlspecialchars($this->input->post('dist_cust', true)),
                'ekspedisi' => htmlspecialchars($this->input->post('ekspedisi', true)),
                'no_polisi' => htmlspecialchars($this->input->post('no_polisi', true)),
                'suhu_awal' => htmlspecialchars($this->input->post('suhu_awal', true)),
                'suhu_setelah' => htmlspecialchars($this->input->post('suhu_setelah', true)),
                'jam_awal' => htmlspecialchars($this->input->post('jam_awal', true)),
                'jam_setelah' => htmlspecialchars($this->input->post('jam_setelah', true)),
                'kebersihan_kendaraan' => htmlspecialchars($this->input->post('kebersihan', true)),
                'bunyi_mesin_kendaraan' => htmlspecialchars($this->input->post('bunyi', true)),
                'asap_kendaraan' => htmlspecialchars($this->input->post('asap', true)),
                'bau_kendaraan' => htmlspecialchars($this->input->post('bau', true)),
                'kebocoran_kendaraan' => htmlspecialchars($this->input->post('kebocoran', true)),
                'emisi_kendaraan' => htmlspecialchars($this->input->post('emisi', true)),
                'oli_kendaraan' => htmlspecialchars($this->input->post('oli', true)),
                'catatan' => htmlspecialchars($this->input->post('catatan', true)),
                'segel' => htmlspecialchars($this->input->post('segel', true)),
                'update_by' => $this->session->userdata('id'),
                'status_form_rd_id' => '4'
            ];

            // $data['query'] = $this->Admin_model->workerUpdate($id, $data);
            $this->db->where('id', $id);
            $this->db->update('receive_delivery', $data);
            $this->Feature_model->do_log(strval($this->db->last_query()));


            $bn = $this->input->post('bn');
            $lpn = $this->input->post('lpn');
            $qty_mb = $this->input->post('qty_mb');
            $qty_per_mb = $this->input->post('qty_per_mb');
            $qty_pick = $this->input->post('qty_pick');
            $pco = $this->input->post('pco');
            $qep = $this->input->post('qep');
            $data_rd_id = $this->input->post('data_rd_id');


            if ($bn != 0) {
                # code...
                for ($i = 0; $i < count($bn); $i++) {
                    // $this->db->where('id', $data_rd_id[$i]);
                    $result = $this->db->update('data_rd', [
                        'batch_number' => $bn[$i],
                        'lpn_number' => $lpn[$i],
                        'qty_mb' => $qty_mb[$i],
                        'qty_per_mb' => $qty_per_mb[$i],
                        'qty_pick' => $qty_pick[$i],
                        'product_checklist_out' => $pco[$i],
                        'qty_epm_pallet' => $qep[$i],
                        'update_by' => $this->session->userdata('id')
                    ], array('id' => $data_rd_id[$i]));
                }
            }
            $this->Feature_model->do_log(strval($this->db->last_query()));

            // echo "<pre>";
            // print_r($_POST);
            // echo "</pre>";
            // die;

            $data = [
                '1a' => htmlspecialchars($this->input->post('1a', true)),
                '1b' => htmlspecialchars($this->input->post('1b', true)),
                '1c' => htmlspecialchars($this->input->post('1c', true)),
                '2a' => htmlspecialchars($this->input->post('2a', true)),
                '2b' => htmlspecialchars($this->input->post('2b', true)),
                '2c' => htmlspecialchars($this->input->post('2c', true)),
                '3a' => htmlspecialchars($this->input->post('3a', true)),
                '3b' => htmlspecialchars($this->input->post('3b', true)),
                '4a' => htmlspecialchars($this->input->post('4a', true)),
                '4b' => htmlspecialchars($this->input->post('4b', true)),
                '5a' => htmlspecialchars($this->input->post('5a', true)),
                '5b' => htmlspecialchars($this->input->post('5b', true)),
                '5c' => htmlspecialchars($this->input->post('5c', true)),
                '6a' => htmlspecialchars($this->input->post('6a', true)),
                '6b' => htmlspecialchars($this->input->post('6b', true)),
                '6c' => htmlspecialchars($this->input->post('6c', true)),
                '7a' => htmlspecialchars($this->input->post('7a', true)),
                '7b' => htmlspecialchars($this->input->post('7b', true)),
                '7c' => htmlspecialchars($this->input->post('7c', true)),
                'no_polisi' => htmlspecialchars($this->input->post('no_polisi2', true)),
                'catatan' => htmlspecialchars($this->input->post('catatan2', true)),
                'update_by' => $this->session->userdata('id')
            ];

            $this->db->where('rd_id', $id);
            $this->db->update('inspection_checklist', $data);

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Document delivery export has been resend!</div>');
            redirect('worker/delivery_export');
            // echo $base64;
        }
    }

























    public function file_check($str)
    {
        $allowed_mime_type_arr = array('image/jpeg', 'image/jpg', 'image/png');
        $mime = get_mime_by_extension($_FILES['image']['name']);
        if (isset($_FILES['image']['name']) && $_FILES['image']['name'] != "") {
            if (in_array($mime, $allowed_mime_type_arr)) {
                return true;
            } else {
                $this->form_validation->set_message('file_check', 'Please select only jpg/png file.');
                return false;
            }
        } else {
            $this->form_validation->set_message('file_check', 'Please choose a file to upload.');
            return false;
        }
    }
}
