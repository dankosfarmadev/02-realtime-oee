<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;

// require FCPATH . 'vendor/autoload.php';
// require(APPPATH . 'vendor/autoload.php');

class Menu extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        is_logged_in();

        // Load semua model disini
        $this->load->model('Menu_model');
        $this->load->model('Feature_model');
    }

    // tampilan beserta aturan segment
    public function index()
    {
        $data['title'] = 'Menu Management';
        $data['user'] = $this->db->get_where('user_app', ['nik' => $this->session->userdata('nik')])->row_array();

        // Tarik list table menu
        $data['menu'] = $this->db->get('user_menu')->result_array();
        $this->form_validation->set_rules('menu', 'Menu', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('menu/index', $data);
            $this->load->view('templates/footer');
        } else {
            $this->db->insert('user_menu', ['menu' => $this->input->post('menu')]);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">New menu added!</div>');
            redirect('menu');
        }
    }

    // halaman menuEdit
    public function menuEdit($id)
    {
        $data['title'] = 'Menu Management';
        $data['user'] = $this->db->get_where('user_app', ['nik' => $this->session->userdata('nik')])->row_array();
        $data['menuView'] = $this->db->get_where('user_menu', ['id' => $id])->row_array();

        // penjagaan
        $this->form_validation->set_rules("menu", "Menu", "required");

        if ($this->form_validation->run() == false) {
            // load dulu modelnya
            $this->load->model('Menu_model', 'menu');
            // $data['menuView'] = $this->menu->menuView($this->uri->segment(3));

            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('menu/menu/menuEdit', $data);
            $this->load->view('templates/footer');
        } else {
            $id = $this->input->post('id', true);
            $menu = $this->input->post('menu', true);
            $this->db->set('menu', $menu);
            $this->db->where('id', $id);
            $this->db->update('user_menu');

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Menu has been updated!</div>');
            redirect('menu');
        }
    }

    // halaman menuUpdate
    public function menuUpdate()
    {
        $menu = $this->input->post('menu', true);
        $id = $this->input->post('id', true);
        // var_dump($active);
        // die;
        $this->db->set('menu', $menu);
        $this->db->where('id', $id);
        $this->db->update('user_menu');
        $this->Feature_model->do_log(strval($this->db->last_query()));

        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Menu has been updated!</div>');
        redirect('menu');
    }


    public function submenu()
    {
        $data['title'] = 'Submenu Management';
        $data['user'] = $this->db->get_where('user_app', ['nik' => $this->session->userdata('nik')])->row_array();

        // cara pertama
        // $this->load->model('Menu_model', 'menu');
        // $data['subMenu'] = $this->menu->getSubMenu();

        // cara kedua
        $data['subMenu'] = $this->Menu_model->getSubMenu();
        $data['menu'] = $this->db->get('user_menu')->result_array();

        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('menu_id', 'Menu', 'required');
        $this->form_validation->set_rules('url', 'URL', 'required');
        $this->form_validation->set_rules('icon', 'icon', 'required');

        if ($this->form_validation->run() ==  false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('menu/submenu', $data);
            $this->load->view('templates/footer');
        } else {
            $data = [
                'title' => $this->input->post('title'),
                'menu_id' => $this->input->post('menu_id'),
                'url' => $this->input->post('url'),
                'icon' => $this->input->post('icon'),
                'is_active' => $this->input->post('is_active')
            ];
            $this->db->insert('user_sub_menu', $data);
            $this->Feature_model->do_log(strval($this->db->last_query()));
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">New sub menu added!</div>');
            redirect('menu/submenu');
        }
    }

    public function submenuEdit($id)
    {
        // echo "wkwk";
        $data['title'] = 'Submenu Management';
        $data['user'] = $this->db->get_where('user_app', ['nik' => $this->session->userdata('nik')])->row_array();
        $data['submenuView'] = $this->db->get_where('user_sub_menu', ['id' => $id])->row_array();


        // $this->form_validation->set_rules("title", "Title", "required");
        // $this->form_validation->set_rules("url", "Menu", "required");
        // $this->form_validation->set_rules("icon", "URL", "required");

        $this->form_validation->set_rules("title", "Title", "required");
        $this->form_validation->set_rules("url", "Menu", "required");
        $this->form_validation->set_rules("icon", "URL", "required");
        if ($this->form_validation->run() == false) {
            // load dulu modelnya
            $this->load->model('Menu_model', 'menu');
            // $data['submenuView'] = $this->menu->submenuView($this->uri->segment(3));

            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('menu/submenu/submenuEdit', $data);
            $this->load->view('templates/footer');
        } else {
            $id = $this->input->post('id', true);
            $title = $this->input->post('title', true);
            $icon = $this->input->post('icon', true);
            $url = $this->input->post('url', true);
            $active = $this->input->post('active');
            // $active == 'on' ? '1' : '0';
            if ($active == null) {
                $active = 0;
            } else {
                $active = 1;
            }

            // $this->db->set('title', $title);
            // $this->db->set('icon', $icon);
            // $this->db->set('url', $url);
            // $this->db->set('is_active', $active);
            // $this->db->where('id', $id);
            // $this->db->update('user_sub_menu');

            $query = array(
                'title' => $title,
                'icon' => $icon,
                'url' => $url,
                'is_active' => $active
            );

            $this->db->where('id', $id);
            $this->db->update('user_sub_menu', $query);
            $this->Feature_model->do_log(strval($this->db->last_query()));

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Submenu has been updated!</div>');
            redirect('menu/submenu');
        }
    }



    public function submenuXlsx()
    {

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="submenu.xlsx"');
        header('Cache-Control: max-age=0');
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Title');
        $sheet->setCellValue('B1', 'Menu');
        $sheet->setCellValue('C1', 'URL');
        $sheet->setCellValue('D1', 'Icon');
        $sheet->setCellValue('E1', 'Active');
        $sheet->setCellValue('F1', 'Update Date');

        // // Fetch data
        $row = 2;
        $this->load->model('Menu_model', 'menu');
        $wkwk['test'] = $this->menu->getSubMenu();


        foreach ($wkwk['test'] as $key) {
            $key['is_active'] = $key['is_active'] == 1 ? 'Active' : 'Not Active';
            $sheet->setCellValue('A' . $row, $key['title']);
            $sheet->setCellValue('B' . $row, $key['menu']);
            $sheet->setCellValue('C' . $row, $key['url']);
            $sheet->setCellValue('D' . $row, $key['icon']);
            $sheet->setCellValue('E' . $row, $key['is_active']);
            $sheet->setCellValue('F' . $row, date('d-m-Y H:i:s', strtotime($key['update_date'])));
            // echo $key['title'];
            // echo $key['menu'];
            $row++;
            # code...
        }
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer = new Xlsx($spreadsheet);
        ob_end_clean();
        $writer->save('php://output');
        die;
        exit;
    }

    public function menuXlsx()
    {

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="menu.xlsx"');
        header('Cache-Control: max-age=0');
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Menu');
        $sheet->setCellValue('B1', 'Update Date');

        // // Fetch data
        $row = 2;
        $this->load->model('Menu_model', 'menu');
        $fetch['test'] = $this->menu->getMenu();


        foreach ($fetch['test'] as $key) {
            $sheet->setCellValue('A' . $row, $key['menu']);
            $sheet->setCellValue('B' . $row, date('d-m-Y H:i:s', strtotime($key['update_date'])));
            $row++;
            # code...
        }
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer = new Xlsx($spreadsheet);
        ob_end_clean();
        $writer->save('php://output');
        die;
        exit;
    }

    public function pdfGen()
    {
        // panggil library yang kita buat sebelumnya yang bernama pdfgenerator
        $this->load->library('Pdfgenerator');

        // title dari pdf
        $this->data['title_pdf'] = 'Laporan Penjualan Toko Kita';

        // filename dari pdf ketika didownload
        $file_pdf = 'laporan_penjualan_toko_kita';
        // setting paper
        $paper = 'A4';
        //orientasi paper potrait / landscape
        $orientation = "portrait";

        $html = $this->load->view('menu/submenu', $this->data, true);

        // run dompdf
        $this->pdfgenerator->generate($html, $file_pdf, $paper, $orientation);
    }

    public function laporan_pdf()
    {

        $data = array(
            "dataku" => array(
                "nama" => "Petani Kode",
                "url" => "http://petanikode.com"
            )
        );

        $this->load->library('pdf');

        $this->pdf->setPaper('A4', 'potrait');
        $this->pdf->filename = "laporan-petanikode.pdf";
        $this->pdf->load_view('menu/submenu', $data);
    }

























    // GAJADI DIPAKE
    public function submenuUpdate()
    {
        $data['title'] = 'Submenu Managements';
        $this->load->library('form_validation');

        $this->form_validation->set_rules("title", "Title", "required");
        $this->form_validation->set_rules("url", "Menu", "required");
        $this->form_validation->set_rules("icon", "URL", "required");
        // $this->form_validation->set_rules('active[]', 'Active', 'required');
        // $this->form_validation->run();
        // if ($this->form_validation->run() ==  false) {
        //     redirect('menu/submenu');
        // }


        if ($this->form_validation->run() == false) {
            // $id = $this->input->post('id', true);
            // redirect('menu/submenuEdit/' . $id);
            $this->load->model('Menu_model', 'menu');
            $data['user'] = $this->db->get_where('user', ['nik' => $this->session->userdata('nik')])->row_array();
            // $data['submenuView'] = $this->menu->submenuView($this->uri->segment(3));

            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('menu/submenu/submenuEdit', $data);
            $this->load->view('templates/footer');
            // var_dump("KENAPA ERROR WEH");
        } else {
            // query
            $id = $this->input->post('id', true);
            $title = $this->input->post('title', true);
            $icon = $this->input->post('icon', true);
            $url = $this->input->post('url', true);


            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Submenu has been updated!</div>');
            redirect('menu/submenu');
        }

        // $title = $this->input->post('title', true);
        // if ($this->form_validation->run() == false) {
        //     // echo "wkwk";
        // }
    }
}
