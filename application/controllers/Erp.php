<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Erp extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('image_lib');

        $this->load->helper('cookie');
        $this->load->model('Worker_model');
        $this->load->model('Erp_model');
        $this->load->model('Feature_model');
        is_logged_in();
    }

    public function index()
    {
    }


    public function listApprovalDoc()
    {
        $data['title'] = 'Approval Receive Delivery';
        $data['user'] = $this->db->get_where('user', ['nik' => $this->session->userdata('nik')])->row_array();
        $data['receive'] = $this->Erp_model->getAllWhere();
        // $data['receive'] = $this->Erp_model->getAllWhereDoc();
        // var_dump($data['receive']);
        // die;

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('role/erp_support/approval_doc/table', $data);
        $this->load->view('templates/footer');
    }

    public function approvalDocDetail($id)
    {
        $data['title'] = 'Approval Receive Delivery';
        $data['user'] = $this->db->get_where('user', ['nik' => $this->session->userdata('nik')])->row_array();


        $this->form_validation->set_rules('dataPicker', 'Tanggal', 'required|trim');
        $this->form_validation->set_rules('dist_cust', 'Distibutor or customer', 'required|trim');
        $this->form_validation->set_rules('ekspedisi', 'Ekspedisi', 'required|trim');
        $this->form_validation->set_rules('no_polisi', 'No polisi', 'required|trim');
        $this->form_validation->set_rules('suhu_awal', 'Suhu awal', 'required|trim|numeric');
        $this->form_validation->set_rules('suhu_setelah', 'Suhu setelah', 'required|trim|numeric');
        $this->form_validation->set_rules('jam_awal', 'Jam awal', 'required|trim');
        $this->form_validation->set_rules('jam_setelah', 'Jam setelah', 'required|trim');

        $this->form_validation->set_rules('kebersihan', 'Kebersihan', 'required|trim');
        $this->form_validation->set_rules('bunyi', 'Bunyi', 'required|trim');
        $this->form_validation->set_rules('asap', 'Asap Kendaraan', 'required|trim');
        $this->form_validation->set_rules('bau', 'Bau', 'required|trim');
        $this->form_validation->set_rules('kebocoran', 'Kebocoran', 'required|trim');

        // $this->form_validation->set_rules('nama_ekspedisi', 'Nama pengirim', 'required|trim');

        if ($this->form_validation->run() == false) {
            $data['receive_data'] = $this->Worker_model->getSpecificReceive($id);
            $data['rd_data'] = $this->Worker_model->getEveryBatchData($id);


            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('role/erp_support/approval_doc/detail', $data);
            $this->load->view('templates/footer');
        } else {

            $data = [
                'tanggal' => htmlspecialchars($this->input->post('dataPicker', true)),
                'distributor_customer' => htmlspecialchars($this->input->post('dist_cust', true)),
                'ekspedisi' => htmlspecialchars($this->input->post('ekspedisi', true)),
                'no_polisi' => htmlspecialchars($this->input->post('no_polisi', true)),
                'suhu_awal' => htmlspecialchars($this->input->post('suhu_awal', true)),
                'suhu_setelah' => htmlspecialchars($this->input->post('suhu_setelah', true)),
                'jam_awal' => htmlspecialchars($this->input->post('jam_awal', true)),
                'jam_setelah' => htmlspecialchars($this->input->post('jam_setelah', true)),
                'kebersihan_kendaraan' => htmlspecialchars($this->input->post('kebersihan', true)),
                'bunyi_mesin_kendaraan' => htmlspecialchars($this->input->post('bunyi', true)),
                'asap_kendaraan' => htmlspecialchars($this->input->post('asap', true)),
                'bau_kendaraan' => htmlspecialchars($this->input->post('bau', true)),
                'kebocoran_kendaraan' => htmlspecialchars($this->input->post('kebocoran', true)),
                'emisi_kendaraan' => htmlspecialchars($this->input->post('emisi', true)),
                'oli_kendaraan' => htmlspecialchars($this->input->post('oli', true)),
                'catatan' => htmlspecialchars($this->input->post('catatan', true)),
                'segel' => htmlspecialchars($this->input->post('segel', true)),
                'update_by' => $this->session->userdata('id'),
                'approve_by' => $this->session->userdata('id'),
                'status_form_rd_id' => '5'
            ];

            $this->db->where('id', $id);
            $this->db->update('receive_delivery', $data);
            $this->Feature_model->do_log(strval($this->db->last_query()));


            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Document has been approve!</div>');
            redirect('erp/listApprovalDoc');
            // echo $base64;
        }
    }

    public function approvalDocDetailz($id)
    {
        $data['title'] = 'Approval Receive Delivery';
        $data['user'] = $this->db->get_where('user', ['nik' => $this->session->userdata('nik')])->row_array();


        $this->form_validation->set_rules('dataPicker', 'Tanggal', 'required|trim');
        $this->form_validation->set_rules('dist_cust', 'Distibutor or customer', 'required|trim');
        $this->form_validation->set_rules('ekspedisi', 'Ekspedisi', 'required|trim');
        $this->form_validation->set_rules('no_polisi', 'No polisi', 'required|trim');
        $this->form_validation->set_rules('suhu_awal', 'Suhu awal', 'required|trim|numeric');
        $this->form_validation->set_rules('suhu_setelah', 'Suhu setelah', 'required|trim|numeric');
        $this->form_validation->set_rules('jam_awal', 'Jam awal', 'required|trim');
        $this->form_validation->set_rules('jam_setelah', 'Jam setelah', 'required|trim');

        $this->form_validation->set_rules('kebersihan', 'Kebersihan', 'required|trim');
        $this->form_validation->set_rules('bunyi', 'Bunyi', 'required|trim');
        $this->form_validation->set_rules('asap', 'Asap Kendaraan', 'required|trim');
        $this->form_validation->set_rules('bau', 'Bau', 'required|trim');
        $this->form_validation->set_rules('kebocoran', 'Kebocoran', 'required|trim');

        $this->form_validation->set_rules('emisi', 'Emisi', 'required|trim');
        $this->form_validation->set_rules('oli', 'Oli', 'required|trim');

        // $this->form_validation->set_rules('nama_ekspedisi', 'Nama pengirim', 'required|trim');

        if ($this->form_validation->run() == false) {
            $data['receive_data'] = $this->Worker_model->getSpecificReceive($id);
            $data['rd_data'] = $this->Worker_model->getEveryBatchData($id);


            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            // $this->load->view('role/logistic_coordinator/approval_doc/detail', $data);
            $this->load->view('role/erp_support/approval_doc/detail', $data);
            $this->load->view('templates/footer');
        } else {

            $data = [
                'tanggal' => htmlspecialchars($this->input->post('dataPicker', true)),
                'distributor_customer' => htmlspecialchars($this->input->post('dist_cust', true)),
                'ekspedisi' => htmlspecialchars($this->input->post('ekspedisi', true)),
                'no_polisi' => htmlspecialchars($this->input->post('no_polisi', true)),
                'suhu_awal' => htmlspecialchars($this->input->post('suhu_awal', true)),
                'suhu_setelah' => htmlspecialchars($this->input->post('suhu_setelah', true)),
                'jam_awal' => htmlspecialchars($this->input->post('jam_awal', true)),
                'jam_setelah' => htmlspecialchars($this->input->post('jam_setelah', true)),
                'kebersihan_kendaraan' => htmlspecialchars($this->input->post('kebersihan', true)),
                'bunyi_mesin_kendaraan' => htmlspecialchars($this->input->post('bunyi', true)),
                'asap_kendaraan' => htmlspecialchars($this->input->post('asap', true)),
                'bau_kendaraan' => htmlspecialchars($this->input->post('bau', true)),
                'kebocoran_kendaraan' => htmlspecialchars($this->input->post('kebocoran', true)),
                'emisi_kendaraan' => htmlspecialchars($this->input->post('emisi', true)),
                'oli_kendaraan' => htmlspecialchars($this->input->post('oli', true)),
                'catatan' => htmlspecialchars($this->input->post('catatan', true)),
                'segel' => htmlspecialchars($this->input->post('segel', true)),
                'update_by' => $this->session->userdata('id'),
                'approve_by' => $this->session->userdata('id'),
                'status_form_rd_id' => '5'
            ];

            // $data['query'] = $this->Admin_model->workerUpdate($id, $data);
            $this->db->where('id', $id);
            $this->db->update('receive_delivery', $data);
            $this->Feature_model->do_log(strval($this->db->last_query()));



            $bn = $this->input->post('bn');
            $lpn = $this->input->post('lpn');
            $qty_mb = $this->input->post('qty_mb');
            $qty_per_mb = $this->input->post('qty_per_mb');
            $qty_pick = $this->input->post('qty_pick');
            $pco = $this->input->post('pco');
            $qep = $this->input->post('qep');
            $data_rd_id = $this->input->post('data_rd_id');

            if ($bn != 0) {
                # code...
                for ($i = 0; $i < count($bn); $i++) {
                    // $this->db->where('id', $data_rd_id[$i]);
                    $result = $this->db->update('data_rd', [
                        'batch_number' => $bn[$i],
                        'lpn_number' => $lpn[$i],
                        'qty_mb' => $qty_mb[$i],
                        'qty_per_mb' => $qty_per_mb[$i],
                        'qty_pick' => $qty_pick[$i],
                        'product_checklist_out' => $pco[$i],
                        'qty_epm_pallet' => $qep[$i],
                        'update_by' => $this->session->userdata('id')
                    ], array('id' => $data_rd_id[$i]));
                }
            }

            $this->Feature_model->do_log(strval($this->db->last_query()));

            // echo "<pre>";
            // print_r(strval($this->db->last_query()));
            // echo "</pre>";
            // die;

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Document has been approve!</div>');
            redirect('erp/listApprovalDoc');
        }
    }



    public function listApprovalDocExport()
    {
        $data['title'] = 'Approval Delivery Export';
        $data['user'] = $this->db->get_where('user', ['nik' => $this->session->userdata('nik')])->row_array();
        $data['receive'] = $this->Erp_model->getAllWhereExport();
        // $data['receive'] = $this->Erp_model->getAllWhereDoc();
        // var_dump($data['receive']);
        // die;

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('role/erp_support/approval_doc_export/table', $data);
        $this->load->view('templates/footer');
    }

    public function approvalDocDetailExport($id)
    {
        $data['title'] = 'Approval Delivery Export';
        $data['user'] = $this->db->get_where('user', ['nik' => $this->session->userdata('nik')])->row_array();


        $this->form_validation->set_rules('dataPicker', 'Tanggal', 'required|trim');
        $this->form_validation->set_rules('dist_cust', 'Distibutor or customer', 'required|trim');
        $this->form_validation->set_rules('ekspedisi', 'Ekspedisi', 'required|trim');
        $this->form_validation->set_rules('no_polisi', 'No polisi', 'required|trim');
        $this->form_validation->set_rules('suhu_awal', 'Suhu awal', 'required|trim|numeric');
        $this->form_validation->set_rules('suhu_setelah', 'Suhu setelah', 'required|trim|numeric');
        $this->form_validation->set_rules('jam_awal', 'Jam awal', 'required|trim');
        $this->form_validation->set_rules('jam_setelah', 'Jam setelah', 'required|trim');

        $this->form_validation->set_rules('kebersihan', 'Kebersihan', 'required|trim');
        $this->form_validation->set_rules('bunyi', 'Bunyi', 'required|trim');
        $this->form_validation->set_rules('asap', 'Asap Kendaraan', 'required|trim');
        $this->form_validation->set_rules('bau', 'Bau', 'required|trim');
        $this->form_validation->set_rules('kebocoran', 'Kebocoran', 'required|trim');

        $this->form_validation->set_rules('emisi', 'Emisi', 'required|trim');
        $this->form_validation->set_rules('oli', 'Oli', 'required|trim');

        // $this->form_validation->set_rules('nama_ekspedisi', 'Nama pengirim', 'required|trim');


        $this->form_validation->set_rules('no_polisi2', 'No Polisi', 'required|trim');

        $this->form_validation->set_rules('1a', 'No 1a', 'required|trim');
        $this->form_validation->set_rules('1b', 'No 1b', 'required|trim');
        $this->form_validation->set_rules('1c', 'No 1c', 'required|trim');

        $this->form_validation->set_rules('2a', 'No 2a', 'required|trim');
        $this->form_validation->set_rules('2b', 'No 2b', 'required|trim');
        $this->form_validation->set_rules('2c', 'No 2c', 'required|trim');

        $this->form_validation->set_rules('3a', 'No 3a', 'required|trim');
        $this->form_validation->set_rules('3b', 'No 3b', 'required|trim');

        $this->form_validation->set_rules('4a', 'No 4a', 'required|trim');
        $this->form_validation->set_rules('4b', 'No 4b', 'required|trim');

        $this->form_validation->set_rules('5a', 'No 5a', 'required|trim');
        $this->form_validation->set_rules('5b', 'No 5b', 'required|trim');
        $this->form_validation->set_rules('5c', 'No 5c', 'required|trim');

        $this->form_validation->set_rules('6a', 'No 6a', 'required|trim');
        $this->form_validation->set_rules('6b', 'No 6b', 'required|trim');
        $this->form_validation->set_rules('6c', 'No 6c', 'required|trim');

        $this->form_validation->set_rules('7a', 'No 7a', 'required|trim');
        $this->form_validation->set_rules('7b', 'No 7b', 'required|trim');
        $this->form_validation->set_rules('7c', 'No 7c', 'required|trim');

        // $this->form_validation->set_rules('nama_ekspedisi', 'Nama pengirim', 'required|trim');

        if ($this->form_validation->run() == false) {
            $data['receive_data'] = $this->Worker_model->getSpecificDeliveryExport($id);
            $data['rd_data'] = $this->Worker_model->getEveryBatchData($id);
            $data['ins'] = $this->Worker_model->getSpecificInspectionExport($id);



            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            // $this->load->view('role/logistic_coordinator/approval_doc/detail', $data);
            $this->load->view('role/erp_support/approval_doc_export/detail', $data);
            $this->load->view('templates/footer');
        } else {

            $data = [
                'tanggal' => htmlspecialchars($this->input->post('dataPicker', true)),
                'distributor_customer' => htmlspecialchars($this->input->post('dist_cust', true)),
                'ekspedisi' => htmlspecialchars($this->input->post('ekspedisi', true)),
                'no_polisi' => htmlspecialchars($this->input->post('no_polisi', true)),
                'suhu_awal' => htmlspecialchars($this->input->post('suhu_awal', true)),
                'suhu_setelah' => htmlspecialchars($this->input->post('suhu_setelah', true)),
                'jam_awal' => htmlspecialchars($this->input->post('jam_awal', true)),
                'jam_setelah' => htmlspecialchars($this->input->post('jam_setelah', true)),
                'kebersihan_kendaraan' => htmlspecialchars($this->input->post('kebersihan', true)),
                'bunyi_mesin_kendaraan' => htmlspecialchars($this->input->post('bunyi', true)),
                'asap_kendaraan' => htmlspecialchars($this->input->post('asap', true)),
                'bau_kendaraan' => htmlspecialchars($this->input->post('bau', true)),
                'kebocoran_kendaraan' => htmlspecialchars($this->input->post('kebocoran', true)),
                'emisi_kendaraan' => htmlspecialchars($this->input->post('emisi', true)),
                'oli_kendaraan' => htmlspecialchars($this->input->post('oli', true)),
                'catatan' => htmlspecialchars($this->input->post('catatan', true)),
                'segel' => htmlspecialchars($this->input->post('segel', true)),
                'update_by' => $this->session->userdata('id'),
                'approve_by' => $this->session->userdata('id'),
                'status_form_rd_id' => '5'
            ];

            // $data['query'] = $this->Admin_model->workerUpdate($id, $data);
            $this->db->where('id', $id);
            $this->db->update('receive_delivery', $data);
            $this->Feature_model->do_log(strval($this->db->last_query()));



            $bn = $this->input->post('bn');
            $lpn = $this->input->post('lpn');
            $qty_mb = $this->input->post('qty_mb');
            $qty_per_mb = $this->input->post('qty_per_mb');
            $qty_pick = $this->input->post('qty_pick');
            $pco = $this->input->post('pco');
            $qep = $this->input->post('qep');
            $data_rd_id = $this->input->post('data_rd_id');

            if ($bn != 0) {
                # code...
                for ($i = 0; $i < count($bn); $i++) {
                    // $this->db->where('id', $data_rd_id[$i]);
                    $result = $this->db->update('data_rd', [
                        'batch_number' => $bn[$i],
                        'lpn_number' => $lpn[$i],
                        'qty_mb' => $qty_mb[$i],
                        'qty_per_mb' => $qty_per_mb[$i],
                        'qty_pick' => $qty_pick[$i],
                        'product_checklist_out' => $pco[$i],
                        'qty_epm_pallet' => $qep[$i],
                        'update_by' => $this->session->userdata('id')
                    ], array('id' => $data_rd_id[$i]));
                }
            }

            $this->Feature_model->do_log(strval($this->db->last_query()));

            $data = [
                '1a' => htmlspecialchars($this->input->post('1a', true)),
                '1b' => htmlspecialchars($this->input->post('1b', true)),
                '1c' => htmlspecialchars($this->input->post('1c', true)),
                '2a' => htmlspecialchars($this->input->post('2a', true)),
                '2b' => htmlspecialchars($this->input->post('2b', true)),
                '2c' => htmlspecialchars($this->input->post('2c', true)),
                '3a' => htmlspecialchars($this->input->post('3a', true)),
                '3b' => htmlspecialchars($this->input->post('3b', true)),
                '4a' => htmlspecialchars($this->input->post('4a', true)),
                '4b' => htmlspecialchars($this->input->post('4b', true)),
                '5a' => htmlspecialchars($this->input->post('5a', true)),
                '5b' => htmlspecialchars($this->input->post('5b', true)),
                '5c' => htmlspecialchars($this->input->post('5c', true)),
                '6a' => htmlspecialchars($this->input->post('6a', true)),
                '6b' => htmlspecialchars($this->input->post('6b', true)),
                '6c' => htmlspecialchars($this->input->post('6c', true)),
                '7a' => htmlspecialchars($this->input->post('7a', true)),
                '7b' => htmlspecialchars($this->input->post('7b', true)),
                '7c' => htmlspecialchars($this->input->post('7c', true)),
                'no_polisi' => htmlspecialchars($this->input->post('no_polisi2', true)),
                'catatan' => htmlspecialchars($this->input->post('catatan2', true)),
                'update_by' => $this->session->userdata('id')
            ];

            $this->db->where('rd_id', $id);
            $this->db->update('inspection_checklist', $data);

            // echo "<pre>";
            // print_r(strval($this->db->last_query()));
            // echo "</pre>";
            // die;

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Document delivery export has been approve!</div>');
            redirect('erp/listApprovalDocExport');
        }
    }



    public function receiveDeliveryReject()
    {
        $data = [
            'alasan_reject' => htmlspecialchars($this->input->post('alasan_reject', true)),
            'update_by' => $this->session->userdata('id'),
            'status_form_rd_id' => '3'
        ];

        // $data['query'] = $this->Admin_model->workerUpdate($id, $data);
        $this->db->where('id', $this->input->post('id'));
        $this->db->update('receive_delivery', $data);
        $this->Feature_model->do_log(strval($this->db->last_query()));

        $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Document has been rejected!</div>');
        redirect('erp/listApprovalDoc');
    }

    public function receiveDeliveryReturn()
    {
        // echo $this->input->post('alasan_return');
        // $this->form_validation->set_rules('alasan_return', 'Alasan', 'required|trim');

        $data = [
            'alasan_return' => htmlspecialchars($this->input->post('alasan_return', true)),
            'update_by' => $this->session->userdata('id'),
            'status_form_rd_id' => '2'
        ];

        // $data['query'] = $this->Admin_model->workerUpdate($id, $data);
        $this->db->where('id', $this->input->post('id'));
        $this->db->update('receive_delivery', $data);
        $this->Feature_model->do_log(strval($this->db->last_query()));

        $this->session->set_flashdata('message', '<div class="alert alert-primary" role="alert">Document has been returned!</div>');
        redirect('erp/listApprovalDoc');
    }


    public function listApprovalDocInspectionChecklist()
    {
        $data['title'] = 'Approval Inspection Checklist';
        $data['user'] = $this->db->get_where('user', ['nik' => $this->session->userdata('nik')])->row_array();
        $data['receive'] = $this->Erp_model->getAllWhereDoc();
        // var_dump($data['receive']);
        // die;

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('role/erp_support/approval_doc_inspection_checklist/table', $data);
        $this->load->view('templates/footer');
    }


    public function approvalDocDetailInspectionChecklist($id)
    {
        $data['title'] = 'Approval Inspection Checklist';
        $data['user'] = $this->db->get_where('user', ['nik' => $this->session->userdata('nik')])->row_array();

        $this->form_validation->set_rules('no_polisi', 'No Polisi', 'required|trim');

        $this->form_validation->set_rules('1a', 'No 1a', 'required|trim');
        $this->form_validation->set_rules('1b', 'No 1b', 'required|trim');
        $this->form_validation->set_rules('1c', 'No 1c', 'required|trim');

        $this->form_validation->set_rules('2a', 'No 2a', 'required|trim');
        $this->form_validation->set_rules('2b', 'No 2b', 'required|trim');
        $this->form_validation->set_rules('2c', 'No 2c', 'required|trim');

        $this->form_validation->set_rules('3a', 'No 3a', 'required|trim');
        $this->form_validation->set_rules('3b', 'No 3b', 'required|trim');

        $this->form_validation->set_rules('4a', 'No 4a', 'required|trim');
        $this->form_validation->set_rules('4b', 'No 4b', 'required|trim');

        $this->form_validation->set_rules('5a', 'No 5a', 'required|trim');
        $this->form_validation->set_rules('5b', 'No 5b', 'required|trim');
        $this->form_validation->set_rules('5c', 'No 5c', 'required|trim');

        $this->form_validation->set_rules('6a', 'No 6a', 'required|trim');
        $this->form_validation->set_rules('6b', 'No 6b', 'required|trim');
        $this->form_validation->set_rules('6c', 'No 6c', 'required|trim');

        $this->form_validation->set_rules('7a', 'No 7a', 'required|trim');
        $this->form_validation->set_rules('7b', 'No 7b', 'required|trim');
        $this->form_validation->set_rules('7c', 'No 7c', 'required|trim');


        if ($this->form_validation->run() == false) {
            $data['receive_data'] = $this->Erp_model->getSpecificInspectionChecklist($id);


            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('role/erp_support/approval_doc_inspection_checklist/detail', $data);
            $this->load->view('templates/footer');
        } else {

            $data = [
                '1a' => htmlspecialchars($this->input->post('1a', true)),
                '1b' => htmlspecialchars($this->input->post('1b', true)),
                '1c' => htmlspecialchars($this->input->post('1c', true)),
                '2a' => htmlspecialchars($this->input->post('2a', true)),
                '2b' => htmlspecialchars($this->input->post('2b', true)),
                '2c' => htmlspecialchars($this->input->post('2c', true)),
                '3a' => htmlspecialchars($this->input->post('3a', true)),
                '3b' => htmlspecialchars($this->input->post('3b', true)),
                '4a' => htmlspecialchars($this->input->post('4a', true)),
                '4b' => htmlspecialchars($this->input->post('4b', true)),
                '5a' => htmlspecialchars($this->input->post('5a', true)),
                '5b' => htmlspecialchars($this->input->post('5b', true)),
                '5c' => htmlspecialchars($this->input->post('5c', true)),
                '6a' => htmlspecialchars($this->input->post('6a', true)),
                '6b' => htmlspecialchars($this->input->post('6b', true)),
                '6c' => htmlspecialchars($this->input->post('6c', true)),
                '7a' => htmlspecialchars($this->input->post('7a', true)),
                '7b' => htmlspecialchars($this->input->post('7b', true)),
                '7c' => htmlspecialchars($this->input->post('7c', true)),
                'no_polisi' => htmlspecialchars($this->input->post('no_polisi', true)),
                'catatan' => htmlspecialchars($this->input->post('catatan', true)),
                'update_by' => $this->session->userdata('id'),
                'approve_by' => $this->session->userdata('id'),
                'status_form_inspection_id' => '5'
            ];

            $this->db->where('id', $id);
            $this->db->update('inspection_checklist', $data);

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Document has been approve!</div>');
            redirect('erp/listApprovalDocInspectionChecklist');
            // echo $base64;
        }
    }
}
