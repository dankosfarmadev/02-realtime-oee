<?php
defined('BASEPATH') or exit('No direct script access allowed');

namespace Dompdf;

// require_once 'dompdf-master/autoload.inc.php';

// require_once '/dompdf/lib/html5lib/Parser.php';
// require_once '/php-font-lib/src/FontLib/Autoloader.php';
// require_once '/php-svg-lib/src/autoload.php';
// require_once '/dompdf/src/Autoloader.php';



use Dompdf\Dompdf;
use Dompdf\Options;

class Pdfgenerator
{
    public function generate($html, $filename = '', $paper = '', $orientation = '', $stream = TRUE)
    {
        $options = new Option();
        $options->set('isRemoteEnabled', TRUE);
        $dompdf = new Dompdf($options);
        $dompdf->loadHtml($html);
        $dompdf->setPaper($paper, $orientation);
        $dompdf->render();
        if ($stream) {
            $dompdf->stream($filename . ".pdf", array("Attachment" => 0));
        } else {
            return $dompdf->output();
        }
    }
}
